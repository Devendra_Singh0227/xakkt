//
//  PriceLabel.swift
//  Xakkt-Grocery
//
//  Created by Developer on 07/06/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class PriceLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func clearStrikeOnLabel() {
        let attributedText : NSMutableAttributedString =  NSMutableAttributedString(string: self.text ?? "")
        attributedText.addAttributes([
                        NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue,
                        NSAttributedString.Key.strikethroughColor: UIColor.clear
                        ], range: NSMakeRange(0, attributedText.length))
        self.attributedText = attributedText
    }
    func strikeOnLabel() {
        let attributedText : NSMutableAttributedString =  NSMutableAttributedString(string: self.text ?? "")
        attributedText.addAttributes([
                        NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue,
                        NSAttributedString.Key.strikethroughColor: UIColor.lightGray
                        ], range: NSMakeRange(0, attributedText.length))
        self.attributedText = attributedText
    }

}
