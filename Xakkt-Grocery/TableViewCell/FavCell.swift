//
//  FavCell.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 08/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    @IBOutlet weak var textLbl: UILabel!
}
class FavCell: UITableViewCell {
    
    
    @IBOutlet weak var priceLbl: PriceLabel!
    @IBOutlet weak var dealPriceLbl: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productWgtLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var stepper:StepperView!
    
    @IBOutlet weak var cartButton:UIButton!
    @IBOutlet weak var removeButton:UIButton!
    @IBOutlet weak var rightButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configureData(product:Product) -> Void {
        self.productNameLbl.text = product.name?.english ?? ""
        //self.priceLbl.text = "$\(product.price ?? 0.0)"
        

        if let is_fav = product.is_favourite, is_fav == 1{
            self.rightButton.isSelected = true
        }else{
            self.rightButton.isSelected = false
        }
        
        if let in_cart = product.in_cart?.integerValue, in_cart > 0{
            self.stepper.value = in_cart
            self.cartButton.isSelected = true
        }else{
            self.stepper.value = 1
            self.cartButton.isSelected = false
        }
        if let urlString = product.image, let url = URL(string: urlString){
            self.productImageView?.sd_setImage(with:url, placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        }
        
        if let weight =  product.weight?.stringValue, let unit = product.unit{
            self.productWgtLbl.text = weight + unit
        }
        if let regularPrice = product.price?.doubleValue, let dealPrice = product.deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0) {
            self.priceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            self.dealPriceLbl.isHidden = true
            self.priceLbl.textColor = .black
            self.priceLbl.clearStrikeOnLabel()
        }else{
            self.priceLbl.textColor = .lightGray
            self.dealPriceLbl.isHidden = false
            self.dealPriceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.deal_price?.stringValue ?? "0.00")"
            self.priceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            self.priceLbl.strikeOnLabel()
        }
    }
    
    
    func configureDataForFav(product:Product) -> Void {
        self.productNameLbl.text = product.name?.english ?? ""
        //self.priceLbl.text = "$\(product.price ?? 0.0)"
        

        if let in_shoppinglist = product.in_shoppinglist, in_shoppinglist == 1{
            self.rightButton.isSelected = true
        }else{
            self.rightButton.isSelected = false
        }
        
        if let in_cart = product.in_cart?.integerValue, in_cart > 0{
            self.stepper.value = in_cart
            self.cartButton.isSelected = true
        }else{
            self.stepper.value = 1
            self.cartButton.isSelected = false
        }
        if let urlString = product.image, let url = URL(string: urlString){
            self.productImageView?.sd_setImage(with:url, placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        }
        
        if let weight =  product.weight?.stringValue, let unit = product.unit{
            self.productWgtLbl.text = weight + unit
        }
        if let regularPrice = product.price?.doubleValue, let dealPrice = product.deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0) {
            self.priceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            self.dealPriceLbl.isHidden = true
            self.priceLbl.textColor = .black
            self.priceLbl.clearStrikeOnLabel()
        }else{
            self.priceLbl.textColor = .lightGray
            self.dealPriceLbl.isHidden = false
            self.dealPriceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.deal_price?.stringValue ?? "0.00")"
            self.priceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            self.priceLbl.strikeOnLabel()
        }
    }
    
    
    
    
    
    
    
    func configureDataForCart(product:Product) -> Void {
        self.productNameLbl.text = product.name?.english ?? ""
    
        if let product_in_cart = product.in_cart?.integerValue {
            self.stepper.value = product_in_cart
        }
        
        if let urlString = product.image, let url = URL(string: urlString){
            self.productImageView?.sd_setImage(with:url, placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        }
        
        if let weight =  product.weight?.stringValue, let unit = product.unit{
            self.productWgtLbl.text = weight + unit
        }
        if let regularPrice = product.price?.doubleValue, let dealPrice = product.deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0) {
            self.priceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            self.dealPriceLbl.isHidden = true
            self.priceLbl.textColor = .black
            self.priceLbl.clearStrikeOnLabel()
        }else{
            self.priceLbl.textColor = .lightGray
            self.dealPriceLbl.isHidden = false
            self.dealPriceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.deal_price?.stringValue ?? "0.00")"
            self.priceLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            self.priceLbl.strikeOnLabel()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



