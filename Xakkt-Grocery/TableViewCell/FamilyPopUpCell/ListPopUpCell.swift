//
//  ListPopUpCell.swift
//  Health-Speed
//
//  Created by Ankur Nautiyal on 07/04/20.
//  Copyright © 2020 ankur nautiyal. All rights reserved.
//

import UIKit

class ListPopUpCell: UITableViewCell {

    @IBOutlet weak var lblFmailyName: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
