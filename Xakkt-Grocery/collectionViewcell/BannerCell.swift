//
//  BannerCell.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 03/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class BannerCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!

}
