//
//  HomeTableCell.swift
//  Xakkt-Grocery
//
//  Created by Developer on 24/10/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout


protocol HomeTableCellDelegate:class {
    func returnCartValue(cart:Cart?) -> Void
    func didselectProductCollection(indexPath: IndexPath, collectionview: UICollectionView)
}



class HomeTableCell: UITableViewCell, LoginScreenDelegate, popDidSelectDelegate {
  
    weak var updateProductDelegate:HomeTableCellDelegate?
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var productArray : [Product]?
    fileprivate var type : type?
    fileprivate var path : String?
    var navigation : UINavigationController?
    var listArray : [ListData]?
    var DealId :((IndexPath) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupLayout()
    }
    
    fileprivate func setupLayout() {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        layout.scrollDirection = .horizontal
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 110)
    }
    
    func configureCell(items:[Product]?, type:type, path: String?, listArray: [ListData]?) -> Void {
        self.productArray = items
        self.type = type
        self.path = path
        self.listArray = listArray
        self.collectionView.reloadData()
        let indexPath = IndexPath(item: 1 , section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: true)
    }
    func isSucusssFull(isLogin: Bool) {
//       let vc =  CartVC.storyboardViewController()
//       self.navigation?.pushViewController(vc, animated: true)
    }
    
    // MARK:- selected list
    func addProductInTheList(_ selectedList: ListData?, selectedIndex: Int) {
        guard let list_id = selectedList?.id else {
            return
        }
        guard let _product = self.productArray?[selectedIndex].id else {
            return
        }
        
        let indexPath = IndexPath(row: selectedIndex, section: 0)
        let cell = collectionView.cellForItem(at: indexPath) as! CarouselCollectionViewCell
        cell.listButton.isSelected = true
        
        let param = ["_shoppinglist": list_id ,"_product": _product, "quantity":1 ] as [String : Any]
        Webservices.instance.postMethod(API.addProduct, param: param, header: false, completion: {(error :String?, data: ProductAddList?) -> Void in
            if data?.status?.integerValue == 1 {
               //self.listArray = data?.data ?? []
            }
        })
      }
}

extension HomeTableCell:UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CarouselCollectionViewCell", for: indexPath) as! CarouselCollectionViewCell
            guard let product =  self.productArray?[indexPath.row] else {
                return cell
            }
            if let in_shoppinglist = product.in_shoppinglist, in_shoppinglist == 1{
                cell.listButton.isSelected = true
            }else
            {
                cell.listButton.isSelected = false
            }
            if let is_fav = product.is_favourite, is_fav == 1{
                cell.likeButton.isSelected = true
            }else{
                cell.likeButton.isSelected = false
            }
        
            cell.stepper.tag = indexPath.item
            cell.stepper.delegate = self
            if let in_cart = product.in_cart?.integerValue, in_cart > 0{
                cell.stepper.value = in_cart
                cell.cartButton.isSelected = true
            }else{
                cell.stepper.value = 1
                cell.cartButton.isSelected = false
            }
            if let image_url = self.productArray?[indexPath.item].image{
                cell.productImage?.sd_setImage(with: URL(string: image_url), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
            }
             
            if let weight =  product.weight?.stringValue, let unit = product.unit{
                cell.weightLbl.text = weight + unit
            }
            
            cell.productName.text = product.name?.english ?? ""
        
        if let regularPrice = self.productArray?[indexPath.row].price?.doubleValue, let dealPrice = self.productArray?[indexPath.row].deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0){
                cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.productArray?[indexPath.row].price?.stringValue ?? "0.00")"
                cell.productDealPrice.isHidden = true
                cell.productPrice.textColor = .black
                cell.productPrice.clearStrikeOnLabel()
            }else{
                cell.productPrice.textColor = .lightGray
                cell.productDealPrice.isHidden = false
                cell.productDealPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.productArray?[indexPath.row].deal_price?.stringValue ?? "0.00")"
                cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.productArray?[indexPath.row].price?.stringValue ?? "0.00")"
                cell.productPrice.strikeOnLabel()
            }
            
            
            cell.likeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(self.addFavAction), for: .touchUpInside)
        
            cell.cartButton.tag = indexPath.row
            cell.cartButton.addTarget(self, action: #selector(self.addProductInCart), for: .touchUpInside)
            
            cell.listButton.tag = indexPath.row
            cell.listButton.addTarget(self, action: #selector(self.ShoppingListButton(_:)), for: .touchUpInside)
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //DealId?(indexPath)
        self.updateProductDelegate?.didselectProductCollection(indexPath: indexPath, collectionview: collectionView)
    }
    

    @objc func addProductInCart(sender: UIButton){
        if Utilities.checkUserLogin() {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = collectionView.cellForItem(at: indexPath) as! CarouselCollectionViewCell
            
            sender.isSelected = true
            let product = self.productArray![sender.tag]
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!, "quantity":cell.stepper.value]

            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                if data?.status?.integerValue == 1 {
                    DispatchQueue.main.async {
                        GlobalObject.totalProductInCart = data?.total_products?.integerValue
                        NotificationCenter.default.post(name: Notification.Name("RefreshTotalCount"), object: nil)
                    }
                }
            })
        }else{
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigation?.present(navC, animated: true, completion: nil)
        }
    }
    
    @objc func addFavAction(sender: UIButton){
        if Utilities.checkUserLogin() {
            let product = self.productArray![sender.tag]
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!]

                Webservices.instance.postMethod(API.addWishList, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        DispatchQueue.main.async {
                            var dict = self.productArray![sender.tag]
                            dict.is_favourite = 1
                            sender.isSelected = true
                        }
                    }
                })
            }else{
                let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!]
                Webservices.instance.deleteMethod(API.removeProductWishList, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        DispatchQueue.main.async {
                            var dict = self.productArray![sender.tag]
                            dict.is_favourite = 0
                            sender.isSelected = false
                        }
                    }
                })
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigation?.present(navC, animated: true, completion: nil)
        }
        
        
        
    }
    
    @objc func ShoppingListButton(_ sender: UIButton) {
        if Utilities.checkUserLogin() {
            if let array = listArray, array.count > 0 {
                let controller =  PatientListVC.storyboardViewController()
                controller.shoppingObj = self.listArray
                controller.delegate = self
                controller.selectedIndex = sender.tag
                print(sender.tag)
                if self.listArray?.count ?? 0 <= 6 {
                    controller.preferredContentSize = CGSize(width: 160, height: 35*(self.listArray?.count ?? 0) + 40)
                }
                else {
                    controller.preferredContentSize = CGSize(width: 160, height: 250)
                }
                showPopup(controller, sourceView: sender)
            }else{
                self.navigation?.tabBarController?.selectedIndex = 1
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigation?.present(navC, animated: true, completion: nil)
        }
    }
    
    
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
            let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
            presentationController.sourceView = sourceView
            presentationController.sourceRect = sourceView.bounds
            presentationController.permittedArrowDirections = [.down, .up]
            self.navigation?.viewControllers.first?.present(controller, animated: true)
        }
}



extension HomeTableCell:StepperViewDelegate{
    func setValue(value: Int, tag: Int, isIncrease: Bool) {
        let product = self.productArray![tag]
        if let in_cart = product.in_cart?.integerValue, in_cart > 0{
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"quantity":value]

            Webservices.instance.putMethod(API.updateProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
                if data?.status.integerValue == 1 {
                    self.updateProductDelegate?.returnCartValue(cart: data)
                }
            })
        }
    }
    
    
}
