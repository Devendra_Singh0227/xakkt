//
//  CarouselCollectionViewCell.swift
//  Xakkt-Grocery
//
//  Created by Developer on 24/10/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

//protocol CarouselCollectionViewCellDelegate:class {
//    func setValue(value:Int, tag:Int, isIncrease:Bool) -> Void
//}

class CarouselCollectionViewCell: UICollectionViewCell {
    
    //weak var updateProductDelegate:CarouselCollectionViewCellDelegate?
    
    @IBOutlet weak var stepper: StepperView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var productPrice: PriceLabel!
    @IBOutlet weak var productDealPrice: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


//extension CarouselCollectionViewCell:StepperViewDelegate{
//    func setValue(value: Int, tag: Int, isIncrease: Bool) {
//        self.updateProductDelegate?.setValue(value: value, tag: tag, isIncrease: isIncrease)
//    }
//}
