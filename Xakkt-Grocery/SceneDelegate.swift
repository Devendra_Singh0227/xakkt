//
//  SceneDelegate.swift
//  Xakkt-Grocery
//
//  Created by Developer on 31/07/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    static weak var shared: SceneDelegate?
    
    static func sharedInstance() -> SceneDelegate{
        var appDelegate:SceneDelegate?
        if Thread.isMainThread {
            appDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        }else{
            DispatchQueue.main.sync {
                appDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
            }
        }
        return appDelegate!
    }
    

    func setRootViewController() {
        //window = UIWindow(frame: UIScreen.main.bounds)
        
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
            else {return}
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navVC = storyboard.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController

        
        let rootController = navVC
        if let userDAta : Login = Utilities.getModelUserDefault(keys: keys.userData) as Login?
        {
            GlobalObject.user = userDAta
            rootController.viewControllers = [StoreSelectionVC.storyboardViewController()]
            sceneDelegate.window?.rootViewController = rootController
        }
        else {
            rootController.viewControllers = [LoginViewController.storyboardViewController()]
            sceneDelegate.window?.rootViewController = rootController
        }
        sceneDelegate.window?.makeKeyAndVisible()
        
    }
    
    public func createMenuView() {
        
        // create viewController code...
        let mainViewController = CustomTabBarViewController.storyboardTabBarController()
        let leftViewController = LeftMenuViewController.storyboardViewController()
        
        let navigationViewController = UINavigationController(rootViewController: mainViewController)
        navigationViewController.navigationBar.isHidden = true
        
        let slideMenuController = SlideMenuController(mainViewController: navigationViewController, leftMenuViewController: leftViewController)
        SlideMenuOptions.contentViewScale = 1.0
        SlideMenuOptions.panFromBezel = false
        SlideMenuOptions.hideStatusBar = false
        slideMenuController.changeLeftViewWidth(UIScreen.main.bounds.width*0.80)
        self.window?.rootViewController = slideMenuController
    }
    

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        Self.shared = self
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
//        guard let windowScene = (scene as? UIWindowScene) else { return }
//        self.window = UIWindow(windowScene: windowScene)
//        self.window?.makeKeyAndVisible()
        //self.createMenuView()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

