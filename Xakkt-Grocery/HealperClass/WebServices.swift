//
//  WebServices.swift
//  Health-Speed
//
//  Created by ankur nautiyal on 17/12/19.
//  Copyright © 2019 ankur nautiyal. All rights reserved.
//

import Foundation
import Alamofire
import Reachability
import NVActivityIndicatorView
import SwiftyJSON
 import SVProgressHUD

typealias ServerSuccessCallBack = (_ json:JSON)->Void
typealias ServerFailureCallBack=(_ error:Error?)->Void
typealias ServerProgressCallBack = (_ progress:Double?) -> Void
typealias ServerNetworkConnectionCallBck = (_ reachable:Bool) -> Void
typealias SuccessHandler = (_ obj: String) -> Void


final class Webservices
{
    static let instance = Webservices()
    var completion : SuccessHandler!

    private init() {}
    
    
    
    
    //New method
    func deleteMethod<T: Codable>(_ urlString:String ,param: [String:Any]?, header: Bool, completion: @escaping (_ Error :String?, _ response: T?) -> Void) {

        if self.isReachable {
            guard let serviceUrl = URL(string: urlString) else { return }
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "DELETE"
            request.timeoutInterval = 60
            request.cachePolicy = .reloadIgnoringLocalCacheData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "accept")
            if let user_token = GlobalObject.user?.data?.token {
                request.setValue(user_token, forHTTPHeaderField: "Authorization")
            }

            guard let param = param, let httpBody = try? JSONSerialization.data(withJSONObject: param, options: []) else {
                return
            }
            request.httpBody = httpBody
            let session = URLSession.shared
            // Utilities.showProgress()
            let activityData = ActivityData(size: CGSize(width: 45, height: 45), type: .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            
            session.dataTask(with: request) { (data, response, error) in
                // Check Data
                DispatchQueue.main.asyncAfter(deadline: .now() +  0.4) {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                
                if let data = data {
                    // Json Response
                    
                    let  dfd = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(dfd as Any)
                    // response.
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            completion(nil,model)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(jsonErr.localizedDescription,nil)
                        }
                    }
                    else {
                        completion(error?.localizedDescription,nil)
                    }
                } else {
                    completion(error?.localizedDescription, nil)
                }
            }.resume()
      
        } else {
            DispatchQueue.main.async {
                completion("Please check your internet connection!", nil)
                
            }
        }
    }
    
    
    // MA\\\\\\\\\\\\\\\\\\\\K: - URLSession methods
    func getMethod<T: Codable>(_ url:String, completion: @escaping (T?, _ error:String?) -> ()) {
      
        if self.isReachable
        {
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "GET"
            request.timeoutInterval = 240
            request.cachePolicy = .reloadIgnoringLocalCacheData
                
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            if let user_token = GlobalObject.user?.data?.token {
                request.setValue(user_token, forHTTPHeaderField: "Authorization")
            }
            
            // Create url session
            let session = URLSession(configuration: URLSessionConfiguration.default)
            // Call session data task.
                    
            let activityData = ActivityData(size: CGSize(width: 45, height: 45), type: .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            
            session.dataTask(with: request) { (data, response, error) -> Void in
                // Check Data
                DispatchQueue.main.asyncAfter(deadline: .now() +  0.4) {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                if let data = data {
                    // Json Response
                    let re = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(re as Any)
                    // response.
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            completion(model,nil)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(nil,error?.localizedDescription)
                        }
                    } else {
                        completion(nil,error?.localizedDescription)
                    }
                } else {
                    completion(nil,error?.localizedDescription)
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                completion(nil,"Please check your internet connection!")
            }
        }
    }
    
    
    // MARK: - URLSession methods
    
    func deleteMethod<T: Codable>(_ urlString:String ,param: [String:Any]?, completion: @escaping (T?, _ error:String?) -> Void) {
        
        if self.isReachable
        {
            var request = URLRequest(url: URL(string: urlString)!)
            request.httpMethod = "DELETE"
            request.timeoutInterval = 240
            request.cachePolicy = .reloadIgnoringLocalCacheData
          
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            
            if param != nil {
                guard let httpBody = try? JSONSerialization.data(withJSONObject: param!, options: []) else {
                    return
                }
                request.httpBody = httpBody
            }

            if let user_token = GlobalObject.user?.data?.token {
                request.setValue(user_token, forHTTPHeaderField: "Authorization")
            }
            // Create url session
            let session = URLSession(configuration: URLSessionConfiguration.default)
            // Call session data task.
            let activityData = ActivityData(size: CGSize(width: 45, height: 45), type: .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            
            session.dataTask(with: request) { (data, response, error) -> Void in
                // Check Data
                DispatchQueue.main.asyncAfter(deadline: .now() +  0.4) {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                if let data = data {
                    // Json Response
                    let re = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(re as Any)
                    // response.
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            completion(model,nil)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(nil,error?.localizedDescription)
                        }
                    } else {
                        completion(nil,error?.localizedDescription)
                    }
                } else {
                    completion(nil,error?.localizedDescription)
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                completion(nil,"Please check your internet connection!")
            }
        }
    }
    
    

    // MARK: - URLSession methods
    
    func patchMethod<T: Codable>(_ url:String,param: [String:Any]?, completion: @escaping (T?, _ error:String?) -> ()) {
        
        if self.isReachable
        {
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "PATCH"
            request.timeoutInterval = 240
            request.cachePolicy = .reloadIgnoringLocalCacheData
            
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            //            request.setValue("Bearer \(String(describing: userdata.data!))", forHTTPHeaderField: "Authorization")
            
            if param != nil {
                guard let httpBody = try? JSONSerialization.data(withJSONObject: param!, options: []) else {
                    return
                }
                request.httpBody = httpBody
            }
            
            if let user_token = GlobalObject.user?.data?.token {
                request.setValue(user_token, forHTTPHeaderField: "Authorization")
            }
            // Create url session
            let session = URLSession(configuration: URLSessionConfiguration.default)
            // Call session data task.
            let activityData = ActivityData(size: CGSize(width: 45, height: 45), type: .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            
            
            session.dataTask(with: request) { (data, response, error) -> Void in
                // Check Data
                DispatchQueue.main.asyncAfter(deadline: .now() +  0.4) {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                if let data = data {
                    // Json Response
                    let re = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(re as Any)
                    // response.
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            completion(model,nil)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(nil,error?.localizedDescription)
                        }
                    } else {
                        completion(nil,error?.localizedDescription)
                    }
                } else {
                    completion(nil,error?.localizedDescription)
                }
            }.resume()
        } else {
            DispatchQueue.main.async {
                completion(nil,"Please check your internet connection!")
            }
        }
    }
    
    
    func putMethod<T: Codable>(_ urlString:String ,param: [String:Any], header: Bool, completion: @escaping (_ Error :String?, _ response: T?) -> Void) {

        print("PARAM==> \(param)")
        if self.isReachable {
            guard let serviceUrl = URL(string: urlString) else { return }
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "PUT"
            request.timeoutInterval = 60
            request.cachePolicy = .reloadIgnoringLocalCacheData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "accept")
            if let user_token = GlobalObject.user?.data?.token {
                request.setValue(user_token, forHTTPHeaderField: "Authorization")
            }

            guard let httpBody = try? JSONSerialization.data(withJSONObject: param, options: []) else {
                return
            }
            request.httpBody = httpBody
            let session = URLSession.shared
            // Utilities.showProgress()
            let activityData = ActivityData(size: CGSize(width: 45, height: 45), type: .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            
            session.dataTask(with: request) { (data, response, error) in
                // Check Data
                DispatchQueue.main.asyncAfter(deadline: .now() +  0.4) {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                
                if let data = data {
                    // Json Response
                    
                    let  dfd = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(dfd as Any)
                    // response.
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            completion(nil,model)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(jsonErr.localizedDescription,nil)
                        }
                    }
                    else {
                        completion(error?.localizedDescription,nil)
                    }
                } else {
                    completion(error?.localizedDescription, nil)
                }
            }.resume()
      
        } else {
            DispatchQueue.main.async {
                completion("Please check your internet connection!", nil)
                
            }
        }
    }
    
    
    func postMethod<T: Codable>(_ urlString:String ,param: [String:Any], header: Bool, completion: @escaping (_ Error :String?, _ response: T?) -> Void) {

        print("PARAM==> \(param)")
        if self.isReachable {
            guard let serviceUrl = URL(string: urlString) else { return }
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.timeoutInterval = 60
            request.cachePolicy = .reloadIgnoringLocalCacheData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "accept")
            if let user_token = GlobalObject.user?.data?.token {
                request.setValue(user_token, forHTTPHeaderField: "Authorization")
            }

            guard let httpBody = try? JSONSerialization.data(withJSONObject: param, options: []) else {
                return
            }
            request.httpBody = httpBody
            let session = URLSession.shared
            // Utilities.showProgress()
            let activityData = ActivityData(size: CGSize(width: 45, height: 45), type: .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            
            session.dataTask(with: request) { (data, response, error) in
                // Check Data
                DispatchQueue.main.asyncAfter(deadline: .now() +  0.4) {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                
                if let data = data {
                    // Json Response
                    
                    let  dfd = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(dfd as Any)
                    // response.
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            completion(nil,model)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(jsonErr.localizedDescription,nil)
                        }
                    }else if let dict = dfd as? NSDictionary, let errorMessage = dict.value(forKey: "message") as? String{
                        completion(errorMessage,nil)
                    }
                    else {
                        completion(error?.localizedDescription,nil)
                    }
                } else {
                    completion(error?.localizedDescription, nil)
                }
            }.resume()
      
        } else {
            DispatchQueue.main.async {
                completion("Please check your internet connection!", nil)
                
            }
        }
    }

    
    
    func Login(_ urlString:String, param:[String:Any], completion: @escaping (_ isSuccess :Bool, _ response: Any) -> Void) {
        if isReachable {
            
            AF.request(URL.init(string: urlString)!, method: .get, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                print(response.result)
                
                switch response.result {
                    
                case .success(_):
                    
                    if (response.data != nil) {
                        do {
                            _ = JSONDecoder()
                            
                            completion(false, response)
                            
                        } catch let err {
                            print("Err", err)
                            completion(false, AnyClass.self)
                        }
                    }
                    break
                case .failure(let error):
                    completion(false, AnyClass.self)
                    break
                }
            }
            
            
            
        }
        else {
            DispatchQueue.main.async {
                completion(false, AnyClass.self)
            }
        }
    }
    
    // MARK:- Check for network connection availability
    var isReachable: Bool {
        let reachabity: Reachability = initalReahability()
        return reachabity.connection != .unavailable
    }
    
    func initalReahability() -> Reachability {
        let rachability: Reachability!
        
        do{
            try! rachability = Reachability()
        }
        catch let (error){
            print(error)
        }
        return rachability
    }
    
    // MARK:- Generic method GET
    func genericGet<T: Codable>(_ url:String, param:[String:Any]?, completion: @escaping (T?, _ error:String?) -> ()) {
        if isReachable == false {
            return
        }
        
        var headers: HTTPHeaders = [
                           "Content-Type": "application/json",
                           "Accept": "application/json"
                       ]
        
        if let user_token = GlobalObject.user?.data?.token {
            headers["Authorization"] = user_token
        }
        
        AF.request("http://3.131.128.9:4800/api/v1/user/addresslist", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON { response in
                switch response.result {
                case .success(_):
                    if let val = response.data {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: val)
                            completion(model,nil)
                        } catch let jsonErr {
                            print("failed to decode, \(jsonErr)")
                            completion(nil,jsonErr.localizedDescription)
                        }
                    }
                    break
                case .failure(let error):
                    completion(nil,error.localizedDescription)
                    break
                }
        }
    }
    
    
    // MARK:- Generic methods POST
    func genericPost<T: Codable>(_ url: String, param: [String:Any]?, completion: @escaping (T?, _ error: String?) -> ()) {
        if isReachable == false {          
            return
        }
        
        var headers: HTTPHeaders = [
                           "Content-Type": "application/json",
                           "Accept": "application/json"
                       ]
        
        if let user_token = GlobalObject.user?.data?.token {
            headers["Authorization"] = user_token
        }
        SVProgressHUD.show()
        AF.request(URL.init(string: url)!, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                  SVProgressHUD.dismiss()
                  print(response.result)
                  switch response.result {
                  case .success(_):
                      if let val = response.data {
                          do {
                              let model = try JSONDecoder().decode(T.self, from: val)
                              completion(model,nil)
                          } catch let jsonErr {
                              print("failed to decode, \(jsonErr)")
                              completion(nil,jsonErr.localizedDescription)
                          }
                      }
                      break
                  case .failure(let error):
                      completion(nil,error.localizedDescription)
                      break
                  }
              }
    }
    // MARK:- Generic methods PUT
    func genericPut<T: Codable>(_ url: String, param: [String:Any]?, completion: @escaping (T?, _ error: String?) -> ()) {
        if isReachable == false {
            return
        }
        var headers: HTTPHeaders = [
                           "Content-Type": "application/json",
                           "Accept": "application/json"
                       ]
        
        if let user_token = GlobalObject.user?.data?.token {
            headers["Authorization"] = user_token
        }
        
        
        AF.request(URL.init(string: url)!, method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                  print(response.result)
                  switch response.result {
                  case .success(_):
                      if let val = response.data {
                          do {
                              let model = try JSONDecoder().decode(T.self, from: val)
                              completion(model,nil)
                          } catch let jsonErr {
                              print("failed to decode, \(jsonErr)")
                              completion(nil,jsonErr.localizedDescription)
                          }
                      }
                      break
                  case .failure(let error):
                      completion(nil,error.localizedDescription)
                      break
                  }
              }
    }
}


