//
//  Storyboardable.swift
//
//  Created by Ankur Nautiyal
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol Storyboardable: class {
    static var defaultStoryboardName: String { get }
}

extension Storyboardable where Self: UIViewController {
    static var defaultStoryboardName: String {
        return String(describing: self)
    }


    static func storyboardViewController() -> Self {
        
        let storyboard = Utilities.getStoryBoard(storyBoardName: StoryboardID.main)

        guard let vc = storyboard.instantiateViewController(identifier: defaultStoryboardName) as? Self else {
            fatalError("Could not instantiate initial storyboard with name: \(defaultStoryboardName)")
        }

        return vc
    }

    static func storyboardNavigationController() -> UINavigationController {
        let storyboard = Utilities.getStoryBoard(storyBoardName: StoryboardID.main)

        guard let nc = storyboard.instantiateViewController(identifier: defaultStoryboardName) as? UINavigationController else {
            fatalError("Could not instantiate initial storyboard with name: \(defaultStoryboardName)")
        }

        return nc
    }

    static func storyboardTabBarController() -> UITabBarController {
        let storyboard = Utilities.getStoryBoard(storyBoardName: StoryboardID.main)

        guard let tc = storyboard.instantiateViewController(identifier: defaultStoryboardName) as? UITabBarController else {
            fatalError("Could not instantiate initial storyboard with name: \(defaultStoryboardName)")
        }
        
        return tc
    }
}

