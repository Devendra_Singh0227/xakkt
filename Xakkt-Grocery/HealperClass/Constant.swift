//
//  Constant.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 29/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation
import KeychainSwift



func isCheckSpecialCharecter(testStr:String) -> Bool {
    let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ")
    if testStr.rangeOfCharacter(from: characterset.inverted) != nil {
        return false
    }
    return true
}
let googleApiKey = "AIzaSyBWQzfg1cM3ZHvr-tPQ8zXlKOvXPSsSl98"



struct keys {
    static let userData = "LoginUserData"
    static let username = "username"
    static let password = "password"
    
}

struct Chain {
   static var keychainObject = KeychainSwift()
}

struct API {
   // http://5a3edad796d4.ngrok.io/api/v1/store/list
    static let baseurl     =  "http://3.131.128.9:4800/api/v1/"
    
    static let getProductListByCategory = baseurl + "category/"
    static let getCategoryList = baseurl + "category/list"
   
    static let authenticate =  baseurl + "user/authenticate"
    static let dashboard =  baseurl + "app/dashboard/"
    static let zipcode =  baseurl + "store/zipcode"
    static let login = baseurl + "user/authenticate"
    static let createUser = baseurl + "user/create"
    static let create_shoppinglist = baseurl + "shoppinglist/create"
    static let getShoppingList = baseurl + "shoppinglist/"
    static let wishList = baseurl + "wishlist/"
    static let cart = baseurl + "cart/"
    static let forgotPwd = baseurl + "user/forgetpassword/"
    static let updatePwd = baseurl + "user/changepassword/"

    static let deviceUdid = UIDevice.current.identifierForVendor!.uuidString
    
    // store
    
    static let listAllStore =  baseurl + "store/list"
    static let nearByStore =  baseurl + "store/nearby/stores"
    static let store_detail =  baseurl + "store/"
    static let departments = baseurl + "departments/"
    
    //Adress
    
    static let setDefaultAddress =  baseurl + "user/set_default_address/"
    static let getAddressList =  baseurl + "user/addresslist/"
    static let getdefaultAddress =  baseurl + "user/addresslist?default=1"
    static let addNewUser =  baseurl + "user/address/"
    static let deleteNewUser =  baseurl + "user/delete/"
    static let EditAddress =  baseurl + "user/edit/"
    static let updateProfile = baseurl + "user/update/"
    
    // Setting
    
     static let add_Setting = baseurl + "setting/add"
    
    // product
    
    static let search_product = baseurl + "product/search"
    static let list_product = baseurl + "product/list"
    static let addProduct = getShoppingList + "add_product"
    static let remove_product = getShoppingList + "remove_product/"
    static let product_detail = baseurl + "product"
    
    static let empty_cart = baseurl + "cart/empty/"
    static let get_cart = baseurl + "cart/size/"
    //Rating
    
     static let rate_product = baseurl + "rating"
    
    // Payment
    
    static let payment = baseurl + "payment"
    
    // Catogery
    
    static let categoryList = baseurl + "category/list"
    
    static let bannerProduct = baseurl + "bannerproduct"
    
    // wishlist
    
    static let wishProduct = wishList + "products"
    static let removeProductWishList = wishList + "remove/product"
    static let addWishList = wishList + "add_product"
    
    static let removeWishList = wishList + "remove"
    static let quantity = getShoppingList + "product/quantity"
    
    static let addProductInCart = cart + "add_product"
    static let getCartDetail = cart + "products"
    static let removeProductFromCart = cart + "remove_product"
    static let updateProductFromCart = cart + "update_quantity"
    
    // Coupon
    static let coupon_List = baseurl + "coupon-list"
    static let apply_Coupon = baseurl + "applycoupon"

    // Order
    
     static let createOrder = baseurl + "order/create"
     static let myOrder = baseurl + "order/myorder"
     static let rateOrder = baseurl + "order/feedback"
     static let changeStatus = baseurl + "order/update_status"
     static let removeShoppingList = getShoppingList + "remove/"
    
   // http://3.131.128.9:4800/api/v1/shoppinglist/product/quantity
//    http://3.131.128.9:4800/api/v1/shoppinglist/5f658f230d58eb4a626f8f79/remove
   /// http://3.131.128.9:4800/api/v1/wishlist/remove/5f5dd5a7de45edfc6c7bed44
  //  static let remove_List = getShoppingList + "remove_product/"

    
    
//    /api/v1/
//http://3.131.128.9:4800/api/v1/wishlist/products
    // http://3.131.128.9:4800/api/v1/app/dashboard
}

struct token {
    static var key = ""
}


struct GlobalObject {
    static var selectedStore: Store?
    static var user : Login?
    static var totalProductInCart : Int?
}
