//
//  Utilities.swift
//  health-speed
//
//  Created by Baljit on 12/01/20.
//  Copyright © 2019 impinge. All rights reserved.
//

import Foundation
import UIKit
import KeychainSwift

class Utilities {
    
    //save value in userdefautls
    class func saveUserDefault(key: String, value : Any) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    //save value in userdefautls
    class func saveUserDefaultBoolValue(key: String, value: Bool) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    // get value form userdefaults
    class func getValueDefaults(key:String) -> Any {
        let value = UserDefaults.standard.value(forKey: key)
        return value as Any
    }
    
    // get value form userdefaults
    class func getValueDefaultsBoolValue(key:String) -> Bool {
        let value = UserDefaults.standard.bool(forKey: key)
        return value
    }
    
    class func setViewControllerWithiOS() {
        if #available(iOS 13.0, *) {
            let scene = UIApplication.shared.connectedScenes.first
            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                //sd.setRootViewController()
            }
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //            appDelegate.setRootViewController()
            // Fallback on earlier versions
        }
    }
    
    
    
    
    // show alert view controller
    
    static var mainStoryBoard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    class func getStoryBoard(storyBoardName: String) -> UIStoryboard {
        return UIStoryboard(name: storyBoardName, bundle: nil)
    }
    
    
    // check if email is valid
    class func isValidEmail(_ email:String) -> Bool {
        return NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: email)
    }
    
   class func formatDate(date: String) -> String {
       let dateFormatterGet = DateFormatter()
       dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

       let dateFormatter = DateFormatter()
       dateFormatter.dateStyle = .medium
       dateFormatter.timeStyle = .none
       //    dateFormatter.locale = Locale(identifier: "en_US") //uncomment if you don't want to get the system default format.

       let dateObj: Date? = dateFormatterGet.date(from: date)

       return dateFormatter.string(from: dateObj!)
    }
    
    class func getFormatedStringFromDate(currentdate : Date )->String {
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat="MM/dd/yyyy"
        let dateInStringFormated=dateformatter.string(from: currentdate)
        return dateInStringFormated
    }
    class func getFormatedStringFromYear(currentdate : Date )->String {
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat="yyyy"
        let dateInStringFormated=dateformatter.string(from: currentdate)
        return dateInStringFormated
    }
    
    class func getDob(currentdate : Date )->String {
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat="yyyy/MM/dd"
        let dateInStringFormated=dateformatter.string(from: currentdate)
        return dateInStringFormated
    }

    class func showAlert(_ title:String, _ msg: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title:title , message: msg, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            rootController.present(alert, animated: true, completion: nil)
        }
    }
    
    //    class func pushToVC(identifier: Identifier, storyBoard: StoryBoards, navigation: UINavigationController)  {
    //        let vc = Utilities.getStoryBoard(storyBoardName: storyBoard).instantiateViewController(withIdentifier: identifier.rawValue)
    //        navigation.pushViewController(vc, animated: true)
    //    }
    
    //    class func presentToPopUP(identifier: Identifier, storyBoard: StoryBoards, viewController: UIViewController)  {
    //            let vc = Utilities.getStoryBoard(storyBoardName: storyBoard).instantiateViewController(withIdentifier: identifier.rawValue)
    //             vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    //             viewController.present(vc, animated: true, completion: nil)
    //       }
    
    // MARK: Get RootController
    class var rootController:UIViewController {
        return (UIApplication.shared.keyWindow?.rootViewController)!
    }
    
    // get application window
    class var window : UIWindow? {
        return (UIApplication.shared.delegate?.window)!
    }
    
    // Check Valid UserName
    class func isValidUsername(_ username : String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", "\\A\\w{2,30}\\z").evaluate(with: username)
    }
    
    
    
    class func getMonthorDate (date : String)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let date = dateFormatter.date(from:date)!
        
        let dateformateed = DateFormatter()
        dateformateed.dateFormat = "MM/dd"
        let DateMMdd = dateformateed.string(from: date)
        return DateMMdd
    }
    
    class func  getJson() -> Data  {
        if let path = Bundle.main.path(forResource: "visit", ofType: "json") {
            let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return data
            //            let jsonResult = try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            //            if let jsonResult = jsonResult as? Dictionary<String, Any> {
            //                return jsonResult
            //            }
        }
        return Data()
    }
    
    class func getDayOfWeek(today:String)->Int? {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            return weekDay
        } else {
            return nil
        }
    }
    
    class func saveModel<T: Codable>(ModelObj: T?, key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(ModelObj) {
            UserDefaults.standard.set(encoded, forKey: key)
        }
    }
    
    class func saveModelInKeychan<T: Codable>(ModelObj: T?, key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(ModelObj) {
            Chain.keychainObject.set(encoded, forKey: key)
        }
    }
    
    class func getModelKeyChange <T:Codable>(keys:String) -> T? {
        
        if let savedPerson: Data = Chain.keychainObject.getData(keys) {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(T.self, from: savedPerson) {
                return loadedPerson
            } else {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    
    class func checkUserLogin() -> Bool {
        if let userDAta : Login = Utilities.getModelKeyChange(keys: keys.userData) as Login?
        {
            GlobalObject.user = userDAta
            return true
        }
        else {
            return false
        }
        
    }
    
    
    class func getModelUserDefault <T:Codable>(keys:String) -> T? {
        
        if let savedPerson = UserDefaults.standard.object(forKey: keys) as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(T.self, from: savedPerson) {
                return loadedPerson
            } else {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    static var currentTimestamp: Int {
        return Int(Date().timeIntervalSince1970)
    }
    
    //    class func addTwoString(firstSting: String, SecondString: String) -> NSMutableAttributedString {
    //
    //
    //        let attrs1 = [NSAttributedString.Key.foregroundColor : UIColor.appYellocolor()]
    //
    ////        [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.white]
    //         let attrs2 =  [NSAttributedString.Key.foregroundColor : UIColor.white]
    //
    //         let attributedString1 = NSMutableAttributedString(string:firstSting, attributes:attrs1)
    //
    //         let attributedString2 = NSMutableAttributedString(string:SecondString, attributes:attrs2)
    //
    //         attributedString1.append(attributedString2)
    //         return attributedString1
    //    }
    
    // MARK: Global navigation bar
    //    class func setAppNavigationBar(_ color: UIColor, titleColor: UIColor, font: UIFont) {
    //        UINavigationBar.appearance().barTintColor = color
    //        UINavigationBar.appearance().tintColor = .hsAppWhiteColor()
    //        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: titleColor, NSAttributedString.Key.font: font]
    //        UINavigationBar.appearance().isTranslucent = false
    //    }
    
    // MARK: Particular screen navigation bar
    //    class func setScreenNavigationBar(_ controller: UIViewController, title: NavigationTitles, leftImage: UIImage? , rightImage: UIImage?) {
    //        controller.navigationController?.isNavigationBarHidden = false
    //        controller.navigationItem.title = title.rawValue
    //        if leftImage != nil {
    //            let leftBar = UIBarButtonItem(image: leftImage, style: .plain, target: controller, action: "leftBarPressed:")
    //            controller.navigationItem.leftBarButtonItem = leftBar
    //        }
    //        if rightImage != nil {
    //            let rightBar = UIBarButtonItem(image: rightImage, style: .plain, target: controller, action: "rightBarPressed:")
    //            controller.navigationItem.rightBarButtonItem = rightBar
    //        }
    //    }
    
    class func printAllFonts() {
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
    }
    
    
    class func openPhoneDial(phoneNo: String) {
        
        
        if let phoneCallURL = URL(string: "telprompt://\(phoneNo)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
        
    }
    
    
    //    class func addErrorLabels(txtField: UITextField, strError: String, viewSub: UIView) {
    //
    //         DispatchQueue.main.async {
    //        if let viewWithTag = viewSub.viewWithTag(1001) {
    //            viewWithTag.removeFromSuperview()
    //        }
    //
    //        let lblName = UILabel(frame: CGRect(x: txtField.frame.origin.x + 12, y: txtField.frame.origin.y + txtField.frame.size.height + 2, width: txtField.frame.size.width, height: 25))
    //        lblName.font = UIFont.systemFont(ofSize: 13)
    //        lblName.textColor = .hsAppYellowColor()
    //        lblName.text = strError
    //        lblName.tag = 1001
    //        lblName.numberOfLines = 2
    //        txtField.borderColor = .hsYellowColor()
    //        txtField.layer.borderWidth = 1.5
    //        txtField.superview?.addSubview(lblName)
    //        }
    //    }
    
    //    class func addErrorLabelsForBtn(btnField: UIButton, strError: String, viewSub: UIView) {
    //        if let viewWithTag = viewSub.viewWithTag(1001) {
    //            viewWithTag.removeFromSuperview()
    //        }
    //
    //        let lblName = UILabel(frame: CGRect(x: 25, y: btnField.frame.origin.y + btnField.frame.size.height + 2, width: viewSub.frame.size.width - 50, height: 30))
    //        lblName.font = UIFont.systemFont(ofSize: 13)
    //        lblName.textColor = .hsAppYellowColor()
    //        lblName.text = strError
    //        lblName.numberOfLines = 0
    //        lblName.tag = 1001
    //
    //        btnField.superview?.addSubview(lblName)
    //    }
    
    //    class func addErrorLabelsForlbl(btnField: UILabel, strError: String, viewSub: UIView) {
    //        if let viewWithTag = viewSub.viewWithTag(1001) {
    //            viewWithTag.removeFromSuperview()
    //        }
    //
    //        let lblName = UILabel(frame: CGRect(x: 25, y: btnField.frame.origin.y + btnField.frame.size.height + 2, width: viewSub.frame.size.width - 50, height: 30))
    //        lblName.font = UIFont.systemFont(ofSize: 13)
    //        lblName.textColor = .hsAppYellowColor()
    //        lblName.text = strError
    //        lblName.numberOfLines = 0
    //        lblName.tag = 1001
    //
    //        btnField.superview?.addSubview(lblName)
    //    }
}


public protocol NibInstantiatable {
    
    static func nibName() -> String
}

extension NibInstantiatable {
    
    static func nibName() -> String {
        return String(describing: self)
    }
    
}

extension NibInstantiatable where Self: UIView {
    
    static func fromNib() -> Self {
        
        let bundle = Bundle(for: self)
        let nib = bundle.loadNibNamed(nibName(), owner: self, options: nil)
        
        return nib!.first as! Self
        
    }
    
}
// Only prints items in debug mode
func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS "
    //comment or no for non-private functions for print
    Swift.print(/*formatter.string(from: Date()),*/ "-- \n", items[0], separator:separator, terminator: terminator)
    #else
    //Swift.print(items[0], separator:separator, terminator: terminator)
    //Swift.print("Release mode")
    #endif
}
