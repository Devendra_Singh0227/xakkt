// Copyright 2018, Ralf Ebert
// License   https://opensource.org/licenses/MIT
// License   https://creativecommons.org/publicdomain/zero/1.0/
// Source    https://www.ralfebert.de/ios-examples/uikit/choicepopover/

import UIKit

class ArrayChoiceTableViewController<Patient> : UITableViewController {
    
    typealias SelectionHandler = (_ Patient: ListData) -> Void
    
    private let values : ShoppingList
    private let onSelect : SelectionHandler?
    
    init(_ values : ShoppingList,selectedIndex: Int, onSelect : SelectionHandler? = nil) {
        self.values = values
        self.onSelect = onSelect
        super.init(style: .plain)
    }
    
    override func viewDidLoad() {
        self.tableView.register(UINib(nibName: "ListPopUpCell", bundle: Bundle.main), forCellReuseIdentifier: "ListPopUpCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.values.data?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListPopUpCell") as! ListPopUpCell
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true)
        onSelect?(self.values.data?[indexPath.row] ?? ListData(id: "", name: ""))
    }
    
}
