//
//  StoreSelectionVC.swift
//  Xakkt-Grocery
//
//  Created by Developer on 16/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import SVProgressHUD
import Alamofire

class StoreCell: UICollectionViewCell {
   
    @IBOutlet weak var lblPhoneber: UILabel!
    @IBOutlet weak var imageViewStore: UIImageView!
    @IBOutlet weak var buttonHeart: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var buttonShowHere: UIButton!
    @IBOutlet weak var backView: UIView!
    static let identifier = "StoreCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 10.0
        backView.layer.borderWidth = 0.5
        backView.layer.borderColor = UIColor.lightGray.cgColor
        
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowOffset = .zero
        backView.layer.shadowRadius = 5
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class StoreSelectionVC: UIViewController, Storyboardable {

    var markers = [GMSMarker]()
    var slectedMarker:GMSMarker?
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    @IBOutlet var locationTable: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var storeCollectionVew: UICollectionView!
    private let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: GMSMapView!
    fileprivate var listData : ListAllStore? = nil {
        didSet {
            self.storeCollectionVew.reloadData()
            self.showCurrentLocationOnMap()
        }
    }
    
    fileprivate var searchData : ListAllStore? = nil {
        didSet {
            self.storeCollectionVew.reloadData()
            self.showCurrentLocationOnMap()
        }
    }
    
    var searchBool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getCurrentLocation()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getListStore()
    }
    
    @IBAction func menuButtonAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func cardButtonAction(_ sender: Any) {
        
        if Utilities.checkUserLogin() {
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    private func getCurrentLocation() -> Void {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        //mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
    }
    
    private func getNearByStore(lat: String, long: String) {
        
     let param = ["lat": lat ,"long": long] as [String : Any]

     Webservices.instance.postMethod(API.nearByStore, param: param, header: false, completion: {(error :String?, data: ListAllStore?) -> Void in
         
         print(data as Any)
         if data != nil {
             DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                     self.listData = data
                 }
             }
         }
         else {
             //FIXME: REMOVE BELOW COEE
             DispatchQueue.main.async {
                 //                    Utilities.pushToVC(identifier: .Tab_VC, storyBoard: .Dash, navigation: self.navigationController!)
             }
         
             
         }
     })
}
    
    
    private func getAddressZipCode(Zipcode: String) {
        Webservices.instance.getMethod(API.zipcode) { (data : ListAllStore?, error) in
            if (data != nil) {
                DispatchQueue.main.async {
                    if data?.status?.integerValue == 1 {
                        self.listData = data
                    }
                }
            }
            else {
            }
        }
    }
    
    private func getListStore() {
        Webservices.instance.getMethod(API.listAllStore) { (data : ListAllStore?, error) in
            if (data != nil) {
               
                DispatchQueue.main.async {
                    if data?.status?.integerValue == 1 {
                        self.listData = data
                    }
                }
            }
            else {
            }
        }
    }
    
    func addFavourite(){
        var parameters:Parameters = [:]
        parameters["_product"] = ""//self.addressType
        parameters["_store"] = ""//addressLabel.text
        parameters["wish_price"] = ""//city
        parameters["max_price"] = ""//city
        
        Webservices.instance.postMethod(API.addWishList, param: parameters, header: false, completion: {(error :String?, data: AddAddress?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    self.navigationController?.popViewController(animated: true)
                    //                    self.showAlert(title: "", msg: data?.message ?? "")
                }else if (data?.message) != nil{
                    //                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}


extension StoreSelectionVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        UIView.animate(withDuration: 5.0, animations: { () -> Void in
            // self.londonView?.tintColor = .blue
        }, completion: {(finished) in
            
        })
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("tap marker")
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let mark = slectedMarker {
            mark.icon = GMSMarker.markerImage(with: UIColor.darkGray)
            slectedMarker = marker
        }else{
            slectedMarker = marker
        }
        marker.icon = GMSMarker.markerImage(with: UIColor.red)
        if let index = markers.firstIndex(of: marker) {
            let indexPath = IndexPath(item: index , section: 0)
            self.storeCollectionVew.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: true)
        }
        return true
    }
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        print("tap")
    }
    
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        var index = 0
        for data in self.listData?.data ?? []{
            let doublet = data.location?.coordinates?.last?.doubleValue ?? 0.0
            let doublong = data.location?.coordinates?.first?.doubleValue ?? 0.0
            if marker.position.latitude == doublet && marker.position.longitude == doublong {
                let indexPath = IndexPath(item: index , section: 0)
                self.storeCollectionVew.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: true)
                break
            }
            index = index + 1
        }
        print(index)
        return nil
    }
}





extension StoreSelectionVC:UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoreCell.identifier, for: indexPath) as! StoreCell
        cell.buttonShowHere.addTarget(self, action: #selector(self.shopHereButtonAction), for: .touchUpInside)
        cell.lblName.text = self.listData?.data?[indexPath.row].name
        cell.lblAddress.text = "\(self.listData?.data?[indexPath.row].address ?? ""),zipcode: \(self.listData?.data?[indexPath.row].zipcode?.stringValue ?? "")"
        cell.lblPhoneber.text = "\(self.listData?.data?[indexPath.row].contactNo ?? "")"
        cell.buttonShowHere.tag = indexPath.item
        cell.buttonHeart.tag = indexPath.row
        cell.buttonHeart.addTarget(self, action: #selector(heart_ButtonAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let VC = storyboard?.instantiateViewController(identifier: "StoreDetailsVC") as? StoreDetailsVC
//        VC?.store_Id = self.listData?.data?[indexPath.row].id
//        navigationController?.pushViewController(VC!, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        print(indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            guard let data = self.listData?.data?[indexPath.row]  else { return }
            guard let doublat = data.location?.coordinates?.last?.doubleValue else { return  }
            guard let doublong = data.location?.coordinates?.first?.doubleValue else { return  }
            let location = CLLocationCoordinate2D(latitude: doublat, longitude: doublong)
            self.mapView.camera = GMSCameraPosition(target: location, zoom: 15.0, bearing: 0, viewingAngle: 0)
            
            if let mark = self.slectedMarker {
                mark.icon = GMSMarker.markerImage(with: UIColor.darkGray)
            }
            let marker = self.markers[indexPath.item]
            marker.icon = GMSMarker.markerImage(with: UIColor.red)
            self.slectedMarker = marker
        }
    }
    
    @objc func shopHereButtonAction(_ sender: UIButton)  {
        
        guard let data = self.listData?.data?[sender.tag] else {
            return
        }
        GlobalObject.selectedStore = data
        SceneDelegate.shared?.createMenuView()
        //SceneDelegate.sharedInstance().createMenuView()
    }
    
    @objc func heart_ButtonAction(_ sender: UIButton)  {
//        addFavourite()
        guard let data = self.listData?.data?[sender.tag] else {
            return
        }
        print("")
//        GlobalObject.selectedStore = data
        
    }
}


extension StoreSelectionVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
}


extension StoreSelectionVC:UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let searchString = searchBar.text, searchString.count > 0{
            self.searchLocation(string: searchBar.text ?? "")
        }else{
            self.autocompleteResults.removeAll()
            self.locationTable.isHidden = true
        }
        //self.getAddress(address: searchBar.text ?? "")
    }
    
    
}

// MARK: - CLLocationManagerDelegate
extension StoreSelectionVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        //self.getNearByStore(lat: String(location.coordinate.latitude), long: String(location.coordinate.longitude))
         //   mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 18.0, bearing: 0, viewingAngle: 0)
        //self.showCurrentLocationOnMap()
        locationManager.stopUpdatingLocation()
        //fetchNearbyPlaces(coordinate: location.coordinate)
    }
    
    func showStoreAccordingToCurrtLocation()  {
        
    }
        
    
    func showCurrentLocationOnMap(){
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        var isFirst = true
        
        
        guard let data = self.listData?.data?.first  else { return }
        guard let doublet = data.location?.coordinates?.last?.doubleValue else { return  }
        guard let doublong = data.location?.coordinates?.first?.doubleValue else { return  }
        let location = CLLocationCoordinate2D(latitude: doublet, longitude: doublong)
        mapView.camera = GMSCameraPosition(target: location, zoom: 15.0, bearing: 0, viewingAngle: 0)
        
        for data in self.listData?.data ?? []{
            guard let doublet = data.location?.coordinates?.last?.doubleValue else { return  }
            guard let doublong = data.location?.coordinates?.first?.doubleValue else { return  }
            let location = CLLocationCoordinate2D(latitude: doublet, longitude: doublong)
            let marker = GMSMarker()
            marker.position = location
            if isFirst == true {
                isFirst = false
                marker.icon = GMSMarker.markerImage(with: UIColor.red)
            }else{
                marker.icon = GMSMarker.markerImage(with: UIColor.darkGray)
            }
            marker.map = mapView
            markers.append(marker)
        }
    }
    
    
    
    func checkUsersLocationServicesAuthorization(){
        /// Check if user has authorized Total Plus to use Location Services
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                
            case .notDetermined:
                // Request when-in-use authorization initially
                // This is the first and the ONLY time you will be able to ask the user for permission
                self.locationManager.delegate = self
                locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
                break
                
            case .restricted, .denied:
                // Disable location features
                
                let alert = UIAlertController(title: "Allow Location Access", message: "MyApp needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                // Button to Open Settings
                alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    }
                }))
                break
                
            case .authorizedWhenInUse, .authorizedAlways:
                
                print("Full Access")
                break
            @unknown default:
                print("Full Access")
                break
            }
        }
    }
    
    
    
    private func searchLocation(string:String){
    
        var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                    if self.autocompleteResults.count > 0{
                        self.locationTable.isHidden = false
                        self.locationTable.reloadData()
                    }else{
                        self.locationTable.isHidden = true
                    }
                }
            } else {
                print(response.error ?? "ERROR")
            }
        }
    }
    
    
    private func getAddress(address:String){
        let url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyB4VJd0V3ySbO7Sogv3U9Jc4WtqaILfSfQ&address=\(address)"
        let escapedString = url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        AF.request(escapedString, method: .get,  parameters: nil, encoding: JSONEncoding.default, headers:nil)
                .responseJSON { response in
                    
            switch response.result {
            case .success(let value):
                
                guard let  responseData =  value as? NSDictionary else{
                    return
                }
                if let results = (responseData.value(forKey: "results") as? NSArray)?.firstObject as? NSDictionary, let geometry =  results.value(forKey: "geometry") as? NSDictionary, let location = geometry.value(forKey: "location") as? NSDictionary{

                    if let latValue = location.value(forKey: "lat") as? Double, let longValue = location.value(forKey: "lng") as? Double {
                        self.mapView.camera = GMSCameraPosition.camera(withLatitude: latValue, longitude: longValue, zoom: 15.0)
                        self.getNearByStore(lat: String(latValue), long: String(longValue))
                    }

                }

            case .failure(let error):
                print(error)
            }
            print(response)
        }
    }
}



extension StoreSelectionVC:UITableViewDelegate,UITableViewDataSource
{
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfSections section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return autocompleteResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = self.locationTable.dequeueReusableCell(withIdentifier: "LocationTableCell", for: indexPath) as! LocationTableCell
        cell.txtLbl.text = autocompleteResults[indexPath.row].formattedAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        
        if self.autocompleteResults.count > 0 {
            self.locationTable.isHidden = true
            //let address = autocompleteResults[indexPath.row].formattedAddress
            var input = GInput()
            input.keyword = autocompleteResults[indexPath.row].placeId
            GoogleApi.shared.callApi(.placeInformation,input: input) { (response) in
                if let place =  response.data as? GApiResponse.PlaceInfo, response.isValidFor(.placeInformation) {
                    DispatchQueue.main.async {
                        if let latValue = place.latitude, let longValue = place.longitude{
                            self.mapView.camera = GMSCameraPosition.camera(withLatitude: latValue, longitude: longValue, zoom: 15.0)
                            self.getNearByStore(lat: String(latValue), long: String(longValue))
                        }
                    }
                } else { print(response.error ?? "ERROR") }
            }
        }

    }
}
