//
//  StoreProductListVC.swift
//  Xakkt-Grocery
//
//  Created by Developer on 23/08/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit
import HMSegmentedControl

class StoreProductListVC: UIViewController {

    @IBOutlet weak var segmentView:UIView!
    @IBOutlet weak var childSegmentView:UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var noProductLbl:UILabel!
    
    let segmentedControl = HMSegmentedControl()
    let childSegmentedControl = HMSegmentedControl()
    
    var parentCategoryIndex = 0
    var categoryListArray = [CategoryData]() {
        didSet {
            DispatchQueue.main.async {
                self.segmentedControl.sectionTitles = self.categoryListArray.map { $0.name ?? "" }
                self.configureHMSegment()
                if let childArray = self.categoryListArray[0].child, childArray.count > 0{
                    self.childSegmentView.isHidden = false
                    if let categoryId = childArray[0].id{
                        self.getProductListByCategory(categoryId)
                    }
                    self.childSegmentedControl.sectionTitles = childArray.map { $0.name ?? "" }
                    self.childSegmentedControl.reloadInputViews()
                }else{
                    if let categoryId = self.categoryListArray[0].id{
                        self.getProductListByCategory(categoryId)
                    }
                    self.childSegmentView.isHidden = true
                }
            }
        }
    }
    
    
    var productArray = [Product]() {
        didSet {
            DispatchQueue.main.async {
                self.noProductLbl.isHidden = self.productArray.count == 0 ? false:true
                self.collectionView.reloadData()
            }
        }
    }
    var listArray : [ListData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.tabBarController?.tabBar.isHidden = false
        self.getCategoryList(GlobalObject.selectedStore?.id ?? "")
        // Do any additional setup after loading the view.
    }
    
//    @IBAction func openMenu(_ sender: Any) {
//        slideMenuController()?.toggleLeft()
//    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func openCartAction(_ sender: Any) {
        if Utilities.checkUserLogin() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    func configureHMSegment() {
        self.segmentView.addSubview(segmentedControl)
        segmentedControl.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.selectionIndicatorLocation = .bottom
        segmentedControl.selectionIndicatorHeight = 4.0
        segmentedControl.selectionIndicatorColor = #colorLiteral(red: 0.1142767668, green: 0.3181744218, blue: 0.4912756383, alpha: 1)
        
        segmentedControl.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)
        ]
        
        segmentedControl.selectedTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.05439098924, green: 0.1344551742, blue: 0.1884709597, alpha: 1),
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)
        ]
        segmentedControl.indexChangeBlock = { index in
            if let childArray = self.categoryListArray[Int(index)].child, childArray.count > 0{
                self.childSegmentView.isHidden = false
                if let categoryId = childArray[0].id{
                    self.parentCategoryIndex = Int(index)
                    self.noProductLbl.isHidden = true
                    self.getProductListByCategory(categoryId)
                }
                self.childSegmentedControl.selectedSegmentIndex = 0
                self.childSegmentedControl.sectionTitles = childArray.map { $0.name ?? "" }
            }else{
                if let categoryId = self.categoryListArray[0].id{
                    self.getProductListByCategory(categoryId)
                }
                self.childSegmentView.isHidden = true
            }
        }
        NSLayoutConstraint.activate(
            [segmentedControl.leftAnchor.constraint(equalTo: segmentView.leftAnchor),
             segmentedControl.heightAnchor.constraint(equalToConstant: 50),
             segmentedControl.rightAnchor.constraint(equalTo: segmentView.rightAnchor),
             segmentedControl.topAnchor.constraint(equalTo: segmentView.topAnchor, constant: 0)]
        )
        
        
        
        
        self.childSegmentView.addSubview(childSegmentedControl)
        childSegmentedControl.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        childSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        childSegmentedControl.selectionIndicatorLocation = .bottom
        childSegmentedControl.selectionIndicatorColor = #colorLiteral(red: 0.1142767668, green: 0.3181744218, blue: 0.4912756383, alpha: 1)
        childSegmentedControl.selectionIndicatorHeight = 4.0
        childSegmentedControl.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)
        ]
        
        childSegmentedControl.selectedTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.05439098924, green: 0.1344551742, blue: 0.1884709597, alpha: 1),
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)
        ]
        childSegmentedControl.indexChangeBlock = { index in
            if let childArray = self.categoryListArray[self.parentCategoryIndex].child{
                if let categoryId = childArray[Int(index)].id{
                    self.noProductLbl.isHidden = true
                    self.getProductListByCategory(categoryId)
                }
            }
        }
        NSLayoutConstraint.activate(
            [childSegmentedControl.leftAnchor.constraint(equalTo: childSegmentView.leftAnchor),
             childSegmentedControl.heightAnchor.constraint(equalToConstant: 50),
             childSegmentedControl.rightAnchor.constraint(equalTo: childSegmentView.rightAnchor),
             childSegmentedControl.topAnchor.constraint(equalTo: childSegmentView.topAnchor, constant: 0)]
        )
    }

    func getProductListByCategory(_ categoryid : String) {
        Webservices.instance.getMethod(API.getProductListByCategory + "\(categoryid)/" + "products?_store=\(GlobalObject.selectedStore?.id ?? "")") { (data : CategoryProductList?, error) in
            if let success = data?.status, success.integerValue == 1 {
                guard let productArray =  data?.data else {
                    return
                }
                self.productArray = productArray
            }else{
                
            }
        }
    }
    
    
    func getCategoryList(_ storeid : String) {
        Webservices.instance.getMethod(API.getCategoryList) { (data : StoreCategory?, error) in
            if let success = data?.status, success.integerValue == 1 {
                guard let categoryArray =  data?.data else {
                    return
                }
                self.categoryListArray = categoryArray
            }else{
                
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
















extension StoreProductListVC:UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CarouselCollectionViewCell", for: indexPath) as! CarouselCollectionViewCell
            let product =  self.productArray[indexPath.row]
        
            if let in_shoppinglist = product.in_shoppinglist, in_shoppinglist == 1{
                cell.listButton.isSelected = true
            }else
            {
                cell.listButton.isSelected = false
            }
            if let is_fav = product.is_favourite, is_fav == 1{
                cell.likeButton.isSelected = true
            }else{
                cell.likeButton.isSelected = false
            }
        
            cell.stepper.tag = indexPath.item
            cell.stepper.delegate = self
            if let in_cart = product.in_cart?.integerValue, in_cart > 0{
                cell.stepper.value = in_cart
                cell.cartButton.isSelected = true
            }else{
                cell.stepper.value = 1
                cell.cartButton.isSelected = false
            }
            if let image_url = self.productArray[indexPath.item].image{
                cell.productImage?.sd_setImage(with: URL(string: image_url), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
            }
             
            if let weight =  product.weight?.stringValue, let unit = product.unit{
                cell.weightLbl.text = weight + unit
            }
            
            cell.productName.text = product.name?.english ?? ""
        
           if let regularPrice = self.productArray[indexPath.row].price?.doubleValue, let dealPrice = self.productArray[indexPath.row].deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0) {
                cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.productArray[indexPath.row].price?.stringValue ?? "0.00")"
                cell.productDealPrice.isHidden = true
                cell.productPrice.textColor = .black
                cell.productPrice.clearStrikeOnLabel()
            }else{
                cell.productPrice.textColor = .lightGray
                cell.productDealPrice.isHidden = false
                cell.productDealPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.productArray[indexPath.row].deal_price?.stringValue ?? "0.00")"
                cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.productArray[indexPath.row].price?.stringValue ?? "0.00")"
                cell.productPrice.strikeOnLabel()
            }
            
            
            cell.likeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(self.addFavAction), for: .touchUpInside)
        
            cell.cartButton.tag = indexPath.row
            cell.cartButton.addTarget(self, action: #selector(self.addProductInCart), for: .touchUpInside)
            
            cell.listButton.tag = indexPath.row
            cell.listButton.addTarget(self, action: #selector(self.ShoppingListButton(_:)), for: .touchUpInside)
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product =  self.productArray[indexPath.row]
        let storyboard = UIStoryboard(name: "Product", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProductDetailTableViewController") as! ProductDetailTableViewController
        vc.product = product
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @objc func addProductInCart(sender: UIButton){
        if Utilities.checkUserLogin() {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = collectionView.cellForItem(at: indexPath) as! CarouselCollectionViewCell

            sender.isSelected = true
            let product = self.productArray[sender.tag]
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!, "quantity":cell.stepper.value]

            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                if data?.status?.integerValue == 1 {

                }
            })
        }else{
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    @objc func addFavAction(sender: UIButton){
        if Utilities.checkUserLogin() {
            let product = self.productArray[sender.tag]
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"wish_price":1.0, "max_price":2.0]

                Webservices.instance.postMethod(API.addWishList, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        var dict = self.productArray[sender.tag]
                        dict.is_favourite = 1
                        sender.isSelected = true
                    }
                })
            }else{
                Webservices.instance.deleteMethod(API.addWishList, param: nil, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        var dict = self.productArray[sender.tag]
                        dict.is_favourite = 1
                        
                     //self.listArray = data?.data ?? []
                    }
                })
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            //loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
        
        
        
    }
    
    @objc func ShoppingListButton(_ sender: UIButton) {
        if Utilities.checkUserLogin() {
            if listArray?.count ?? 0 > 0 {
                let controller =  PatientListVC.storyboardViewController()
                controller.shoppingObj = self.listArray
                //controller.delegate = self
                controller.selectedIndex = sender.tag
                print(sender.tag)
                if self.listArray?.count ?? 0 <= 6 {
                    controller.preferredContentSize = CGSize(width: 160, height: 35*(self.listArray?.count ?? 0) + 40)
                }
                else {
                    controller.preferredContentSize = CGSize(width: 160, height: 250)
                }
                showPopup(controller, sourceView: sender)
            }else{
                self.navigationController?.tabBarController?.selectedIndex = 1
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            //loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
            let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
            presentationController.sourceView = sourceView
            presentationController.sourceRect = sourceView.bounds
            presentationController.permittedArrowDirections = [.down, .up]
            self.navigationController?.viewControllers.first?.present(controller, animated: true)
        }
}
extension StoreProductListVC: UICollectionViewDelegateFlowLayout {
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
            UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: (UIScreen.main.bounds.width-10)/2, height: 320)
        }

}


extension StoreProductListVC:StepperViewDelegate{
    func setValue(value: Int, tag: Int, isIncrease: Bool) {
        let product = self.productArray[tag]
        let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"quantity":value]

        Webservices.instance.putMethod(API.updateProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
            if data?.status.integerValue == 1 {

            }
        })
    }
    
    
}
