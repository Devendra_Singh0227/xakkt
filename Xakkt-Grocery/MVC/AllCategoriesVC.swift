//
//  AllCategoriesVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 31/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class AllCategoriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var allCategoriess_TableView: UITableView!
    var categories_Array: [categories]?
    var isExpand = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        category_List()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menu_tn(_ sender: UIButton) {
         slideMenuController()?.toggleLeft()
    }
    
    @IBAction func profile_Btn(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "MyAccount", bundle: nil)
        let VC = storyBoard.instantiateViewController(identifier: "PersonalDetail_VC") as? PersonalDetail_VC
        navigationController?.pushViewController(VC!, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories_Array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isExpand == false {
            return 0
        } else {
            return 5//categories_Array?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allCategoriess_TableView.dequeueReusableCell(withIdentifier: "Offers_Cell", for: indexPath) as? Offers_Cell
        cell?.textLabel?.text = "hahaha"
//        cell?.categoryName_Lbl.text = categories_Array?[indexPath.row].name ?? ""
//        if let image_url =  self.categories_Array?[indexPath.row].logo {
//            cell?.category_Image?.sd_setImage(with: URL(string: image_url), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
//        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let myColor: UIColor = .random
       
        let Headerview = UIView(frame: CGRect(x: 0, y: 0, width:tableView.frame.size.width , height: 90))
        let categoryView = UIView(frame: CGRect(x: Headerview.frame.origin.x + 10, y: 10, width: Headerview.frame.size.width - 20, height: Headerview.frame.size.height - 16))
        let categoryImage = UIImageView(frame: CGRect(x: categoryView.frame.origin.x + 10, y: 16, width: 90, height: 40))
        categoryImage.image = UIImage(named: "headerLogo")
        let dropImage = UIImageView(frame: CGRect(x: categoryView.frame.size.width - 40, y: 26, width: 14, height: 18))
        dropImage.image = UIImage(named: "arrowright")
        let label = UILabel(frame: CGRect(x: categoryImage.frame.size.width + 40, y: 36, width: categoryView.frame.size.width, height: 20))
        categoryView.layer.cornerRadius = 12
        categoryView.clipsToBounds = true
        label.font = UIFont.boldSystemFont(ofSize: 17)
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: Headerview.frame.size.width, height: Headerview.frame.size.width))
        btn.addTarget(self, action: #selector(clickBtn), for: .touchUpInside)
        label.text = categories_Array?[section].name
        Headerview.addSubview(label)
        Headerview.addSubview(categoryView)
        categoryView.addSubview(categoryImage)
        categoryView.addSubview(dropImage)
        Headerview.addSubview(btn)
        Headerview.backgroundColor = UIColor.white
        categoryView.backgroundColor = myColor
        
        return Headerview
    }
    
    @IBAction func Profile_Btn(_ sender: UIButton) {
           let VC = storyboard?.instantiateViewController(identifier: "PersonalDetail_VC") as? PersonalDetail_VC
           navigationController?.pushViewController(VC!, animated: true)
       }
    
    @objc func clickBtn() {
        if isExpand == false {
            isExpand = true
            allCategoriess_TableView.reloadData()
        } else {
            isExpand = false
            allCategoriess_TableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print(indexPath.section)
    }
    
    func category_List() {
        
        Webservices.instance.getMethod(API.categoryList) { (data : Categories?, error) in
//                print(data)
            if data?.status == "success", let array = data?.data, array.count > 0 {
                self.categories_Array = data?.data
                DispatchQueue.main.async {
                     self.allCategoriess_TableView.reloadData()
                }
//                print(self.categories_Array!)
            }else if data?.status == "success", let array = data?.data, array.count == 0 {
                let noOrderView = self.view.viewWithTag(100)
                noOrderView?.isHidden = false
            }else if data?.status == "true", let array = data?.data, array.count == 0{
                
            }
        }
    }
}
