//
//  ApplyCoupon_VC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 05/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit
import Alamofire

class CouponCell: UITableViewCell {
            
    @IBOutlet weak var name_Lbl: UILabel!
    
    static let identifier = "CouponCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class ApplyCoupon_VC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var coupon_Array: [categories]?
    @IBOutlet weak var coupon_TableView: UITableView!
    @IBOutlet weak var couponTxtField: UITextField!
    var cart_Id : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getCoupon_List()
    }
    
    @IBAction func menu_Btn(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }

    @IBAction func apply_CouponBtn(_ sender: UIButton) {
        apply_Coupon()
    }
    
    @IBAction func cancel_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coupon_Array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = coupon_TableView.dequeueReusableCell(withIdentifier: "CouponCell", for: indexPath) as? CouponCell
        cell?.name_Lbl.text = coupon_Array?[indexPath.row].coupon_code ?? ""
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = coupon_Array?[indexPath.row].coupon_code ?? ""
        couponTxtField.text = name
        cart_Id = coupon_Array?[indexPath.row].id ?? ""
    }
    
    func getCoupon_List() {
        var parameters:Parameters = [:]
        parameters["_store"] = GlobalObject.selectedStore!.id!
        Webservices.instance.postMethod(API.coupon_List, param: parameters, header: false, completion: {(error :String?, data: Categories?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
               if let array = data?.data, array.count > 0 {
                self.coupon_Array = data?.data
                    DispatchQueue.main.async {
                     self.coupon_TableView.reloadData()
                    }
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
    
    func apply_Coupon() {
        
        var parameters:Parameters = [:]
        parameters["_cart"] = cart_Id
        parameters["coupan_name"] = couponTxtField.text!
        
        Webservices.instance.postMethod(API.apply_Coupon, param: parameters, header: false, completion: {(error :String?, data: Categories?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if let array = data?.data, array.count > 0 {
//                    self.categories_Array = data?.data
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}
