//
//  Enums.swift
//  TabbarWithSideMenu
//
//  Created by Sunil Prajapati on 20/04/18.
//  Copyright © 2018 sunil.prajapati. All rights reserved.
//

enum LeftMenuItems: String {
    case tabView = "Tab View"
    case home = "Home"
    case shop = "Shop"
    case myAccount = "My Account"
    case activeOrder = "Orders"
    case offers = "Offers Zone"
    case help = "Help"
    case cart = "Cart"
    case rateUs = "Rate Us"
    case changeStore = "buX"
    case terms = "User Agreement"
    case privacy = "Return Policy"
    case fav = "Favourites"
    case logout = "Logout"
    case category = "Categories"
    case cartList = "Cart List"
    case Departments = "Departments"
    case Setting = "Setting"
    case list = "Shopping Lists"
}

enum TabItem: Int {
    case user = 0
    case home
    case shop
    case myAccount
    case offers
    case categories
    case cart
    
    init() {
        self = .user
    }
}

enum ViewControllerID {
    static let shopViewController = "MyListVC"
    static let homeViewController = "MainViewController"
    static let myAccountVC = "WebVC"
    static let activeOrderVC = "ActiveOrderVC"
    static let recommendedShopVC = "RecommendedShopVC"
    static let help = "HelpVC"
    static let leftMenuViewController = "LeftMenuViewController"
    static let offerVC = "OffersVC"
    static let secondViewController = "SecondViewController"
    static let customTabbarViewController = "CustomTabBarViewController"
    static let settingsViewController = "SettingsViewController"
}

enum ViewID {
    static let leftHeaderView = "LeftHeaderView"
}

enum StoryboardID {
    static let main = "Main"
}
