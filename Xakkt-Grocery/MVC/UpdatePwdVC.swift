//
//  UpdatePwdVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 15/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class UpdatePwdVC: UIViewController {

    @IBOutlet weak var email_TxtField: UITextField!
    @IBOutlet weak var oldPassword_TxtField: UITextField!
    @IBOutlet weak var newPassword_TxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
        
    @IBAction func confirm_Btn(_ sender: UIButton) {
        update_Pwd()
    }
    
    func update_Pwd() {
        let param = ["email": email_TxtField.text!, "oldpassword": oldPassword_TxtField.text!, "password": newPassword_TxtField.text!] as [String : Any]

        Webservices.instance.postMethod(API.updatePwd, param: param, header: false, completion: {(error :String?, data: Login?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if let status = data?.status?.integerValue, status == 1 {
                    self.Aler_WithAction(message: data?.message ?? "", completion: {_ in
                        if true {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}
