//
//  ProductDetailTableViewController.swift
//  Ecommerce
//
//  Created by Eric Rosas on 2/13/17.
//  Copyright © 2017 EmpireAppDesignz. All rights reserved.
//

import UIKit

class ProductDetailTableViewController : UIViewController
{
    var product: Product?
    
    @IBOutlet weak var productDetailTable: UITableView!
    @IBOutlet weak var shoeImagesHeaderView: ShoeImagesHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = product?.name?.english
        if let image_url = product?.image{
            shoeImagesHeaderView.productImg?.sd_setImage(with: URL(string: image_url), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        }
        
        self.productDetailTable.estimatedRowHeight = self.productDetailTable.rowHeight
        self.productDetailTable.rowHeight = UITableView.automaticDimension
        
        self.tabBarController?.tabBar.items![1].badgeValue = "2"
    }
    
    struct Storyboard {
        static let showImagesPageVC = "ShowImagesPageViewController"
        static let shoeDetailCell = "ShoeDetailCell"
        static let productDetailCell = "ProductDetailsCell"
        static let suggestionCell = "SuggestionCell"
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func openCartAction(_ sender: Any) {
        if Utilities.checkUserLogin() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    @objc func addProductInCart(sender: UIButton){
        if Utilities.checkUserLogin() {
            guard let product_id =  product?.id else {
                return
            }
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = self.productDetailTable.cellForRow(at: indexPath) as! ShoeDetailCell
            sender.isSelected = true
            let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!, "quantity":cell.stepper.value]
            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                if data?.status?.integerValue == 1 {
                    
                }
            })
        }else{
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    @objc func addFavAction(sender: UIButton){
        if Utilities.checkUserLogin() {
            guard let product_id =  product?.id else {
                return
            }
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!]

                Webservices.instance.postMethod(API.addWishList, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        var dict = self.product
                        dict?.is_favourite = 1
                        sender.isSelected = true
                    }
                })
            }else{
                Webservices.instance.deleteMethod(API.addWishList, param: nil, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        var dict = self.product
                        dict?.is_favourite = 1
                    }
                })
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
        
        
        
    }
    
    @objc func ShoppingListButton(_ sender: UIButton) {
//        if Utilities.checkUserLogin() {
//            if let array = listArray, array.count > 0 {
//                let controller =  PatientListVC.storyboardViewController()
//                controller.shoppingObj = self.listArray
//                controller.delegate = self
//                controller.selectedIndex = sender.tag
//                print(sender.tag)
//                if self.listArray?.count ?? 0 <= 6 {
//                    controller.preferredContentSize = CGSize(width: 160, height: 35*(self.listArray?.count ?? 0) + 40)
//                }
//                else {
//                    controller.preferredContentSize = CGSize(width: 160, height: 250)
//                }
//                showPopup(controller, sourceView: sender)
//            }else{
//                self.navigation?.tabBarController?.selectedIndex = 1
//            }
//        }
//        else {
//            let loginVC = LoginViewController.storyboardViewController()
//            loginVC.delegate = self
//            let navC = UINavigationController(rootViewController: loginVC)
//            self.navigation?.present(navC, animated: true, completion: nil)
//        }
    }
    
    
    
    
    
    // MARK : - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Storyboard.showImagesPageVC {
            if let imagesPageVC = segue.destination as? ShoeImagesPageViewController {
                imagesPageVC.images = [product?.image ?? ""]
                imagesPageVC.pageViewControllerDelegate = shoeImagesHeaderView
            }
        }
    }
}

extension ProductDetailTableViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.shoeDetailCell, for: indexPath) as! ShoeDetailCell
        cell.product = product
        cell.selectionStyle = .none
        
        
        //Shopping List Button
        if let in_shoppinglist = product?.in_shoppinglist, in_shoppinglist == 1{
            cell.listButton.isSelected = true
        }else
        {
            cell.listButton.isSelected = false
        }
        
        //Fav Button
        if let is_fav = product?.is_favourite, is_fav == 1{
            cell.likeButton.isSelected = true
        }else{
            cell.likeButton.isSelected = false
        }
        
        //Stepper 
        cell.stepper.tag = indexPath.item
        cell.stepper.delegate = self
        if let in_cart = product?.in_cart?.integerValue, in_cart > 0{
            cell.stepper.value = in_cart
            cell.cartButton.isSelected = true
        }else{
            cell.stepper.value = 1
            cell.cartButton.isSelected = false
        }
        
        if let weight =  self.product?.weight?.stringValue, let unit = self.product?.unit{
            cell.weightLbl.text = weight + unit
        }
        
        if let regularPrice = self.product?.price?.doubleValue, let dealPrice = self.product?.deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0) {
            cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.product?.price?.stringValue ?? "0.00")"
            cell.productDealPrice.isHidden = true
            cell.productPrice.textColor = .black
            cell.productPrice.clearStrikeOnLabel()
        }else{
            cell.productPrice.textColor = .lightGray
            cell.productDealPrice.isHidden = false
            cell.productDealPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.product?.deal_price?.stringValue ?? "0.00")"
            cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(self.product?.price?.stringValue ?? "0.00")"
            cell.productPrice.strikeOnLabel()
        }
        
        cell.likeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(self.addFavAction), for: .touchUpInside)
    
        cell.cartButton.tag = indexPath.row
        cell.cartButton.addTarget(self, action: #selector(self.addProductInCart), for: .touchUpInside)
        
        cell.listButton.tag = indexPath.row
        cell.listButton.addTarget(self, action: #selector(self.ShoppingListButton(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ProductDetailTableViewController:LoginScreenDelegate{
    func isSucusssFull(isLogin: Bool) {
        
    }
    
    
}
extension ProductDetailTableViewController:StepperViewDelegate{
    func setValue(value: Int, tag: Int, isIncrease: Bool) {
        guard let product_id =  product?.id else {
            return
        }
        let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!,"quantity":value]
        Webservices.instance.putMethod(API.updateProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
            if data?.status.integerValue == 1 {

            }
        })
    }
}
//// MARK: - UICollectionViewDataSource
//
//extension ProductDetailTableViewController: UICollectionViewDataSource
//{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 4
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//    {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.suggestionCell, for: indexPath) as! SuggestionCollectionViewCell
//        let shoes = Shoe.fetchShoes()
//        cell.image = shoes[indexPath.item].images?.first
//        
//        return cell
//    }
//    
//}
//
//extension ProductDetailTableViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
//{
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        let layout = collectionViewLayout as! UICollectionViewFlowLayout
//        layout.minimumLineSpacing = 5.0
//        layout.minimumInteritemSpacing = 2.5
//        let itemWidth = (collectionView.bounds.width - 5.0) / 2.0
//        return CGSize(width: itemWidth, height: itemWidth)
//    }
//}







