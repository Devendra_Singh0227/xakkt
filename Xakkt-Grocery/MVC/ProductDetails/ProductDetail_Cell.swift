//
//  ProductDetail_Cell.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 31/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class ProductDetail_Cell: UITableViewCell {

    @IBOutlet weak var productCollectionView: UICollectionView!
    var section : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.productCollectionView.delegate = self
        self.productCollectionView.dataSource = self
        
        self.productCollectionView.register(UINib(nibName: "SelectSizeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectSizeCollectionViewCell")
        self.productCollectionView.register(UINib(nibName: "SimilarProduct_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SimilarProduct_CollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ProductDetail_Cell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if section == 0 {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectSizeCollectionViewCell", for: indexPath) as! SelectSizeCollectionViewCell
              return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarProduct_CollectionViewCell", for: indexPath) as! SimilarProduct_CollectionViewCell
            return cell
        }     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if section == 0 {
            return CGSize(width: 120, height: 80)
        } else {
            return CGSize(width: 240, height: 320)
        }
       
//        switch self.section {
//
//        case 0:
//            return CGSize(width: collectionView.frame.width/1.4, height: collectionView.frame.height-10)
//        case 2:
//            return CGSize(width: collectionView.frame.width/2.5, height: collectionView.frame.height-10)
////        case 2:
////            return CGSize(width: collectionView.frame.width/1.1, height: collectionView.frame.height-10)
////        case 3:
////            return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height-10)
////        case 4:
////            return CGSize(width: collectionView.frame.width/1.3, height: collectionView.frame.height-10)
////        case 5:
////            return CGSize(width: collectionView.frame.width/1.3, height: collectionView.frame.height-10)
//        default:
//           return CGSize(width: 0, height: 0)
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.1
    }
    
}
