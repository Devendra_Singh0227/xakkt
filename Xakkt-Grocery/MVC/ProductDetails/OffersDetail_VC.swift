//
//  OffersDetail_VC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 31/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class OffersDetail_VC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var productDetailTableView: UITableView!
    let headerView = GlobalView.OfferHeaderFromNib()
    let HEADER_HEIGHT = 470
    let sections = ["Select Size", "About The Product", "SIMILAR PRODUCTS"]
    var product_id : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

         productDetailTableView.register(UINib(nibName: "ProductDetail_Cell", bundle: nil), forCellReuseIdentifier: "ProductDetail_Cell")
         productDetailTableView.register(UINib(nibName: "Moredetail_TableViewCell", bundle: nil), forCellReuseIdentifier: "Moredetail_TableViewCell")
        
        productDetailTableView.tableHeaderView = headerView
        productDetailTableView.tableHeaderView?.frame.size = CGSize(width: productDetailTableView.frame.width, height: CGFloat(HEADER_HEIGHT))
        
        getProductDetail()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = productDetailTableView.dequeueReusableCell(withIdentifier: "Moredetail_TableViewCell", for: indexPath) as? Moredetail_TableViewCell
             return cell!
        } else {
            let cell = productDetailTableView.dequeueReusableCell(withIdentifier: "ProductDetail_Cell", for: indexPath) as? ProductDetail_Cell
                cell?.section = indexPath.section
                cell?.productCollectionView.tag = indexPath.section
                DispatchQueue.main.async {
                    cell?.productCollectionView.reloadData()
                }
             return cell!
        }
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let sectionView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        sectionView.backgroundColor = UIColor.clear

        let sectionName = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width, height: 40))
        sectionName.text = sections[section]
        sectionName.textColor = UIColor.black
        sectionName.font = UIFont.boldSystemFont(ofSize: 16)
        sectionName.textAlignment = .left

        sectionView.addSubview(sectionName)
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 80
        } else if indexPath.section == 1 {
            return 90//UITableView.automaticDimension
        } else {
            return 340
        }
    }
    
    func getProductDetail() {
        
        let param:[String : Any] = ["storeid": GlobalObject.selectedStore!.id!, "productid": product_id!]
         Webservices.instance.postMethod(API.product_detail, param: param, header: false, completion: {(error :String?, data: Product?) -> Void in
                   
                   print(data as Any)
                   DispatchQueue.main.async {
//                       if let array = data?.data, array.count > 0 {
//                           //                self.coupon_Array = data?.data
//                           //                    DispatchQueue.main.async {
//                           //                     self.coupon_TableView.reloadData()
//                           //                    }
//                       }else if let message = data?.message{
//                           self.showAlert(title: "", msg: message)
//                       }else if let message = error{
//                           self.showAlert(title: "", msg: message)
//                       }
                   }
               })
    }
}
