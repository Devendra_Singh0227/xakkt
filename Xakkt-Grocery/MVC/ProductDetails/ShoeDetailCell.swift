//
//  ShoeDetailCell.swift
//  Ecommerce
//
//  Created by Eric Rosas on 2/13/17.
//  Copyright © 2017 EmpireAppDesignz. All rights reserved.
//

import UIKit

class ShoeDetailCell : UITableViewCell
{
    @IBOutlet weak var shoeNameLabel: UILabel!
    @IBOutlet weak var shoeDescriptionLabel: UILabel!
    
    @IBOutlet weak var stepper: StepperView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    
    
    
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var productPrice: PriceLabel!
    @IBOutlet weak var productDealPrice: UILabel!
    
    var product: Product! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI()
    {
        shoeNameLabel.text = product.name?.english
        shoeDescriptionLabel.text = product.name?.english
    }
}
