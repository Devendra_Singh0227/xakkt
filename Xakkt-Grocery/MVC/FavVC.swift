//
//  FavVC.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 08/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class FavVC: UIViewController,Storyboardable {
    @IBOutlet weak var dataHomeTable:UITableView!
    @IBOutlet weak var noPrductLbl: UILabel!

    var totalNumberofCell = 3 {
        didSet {
            if totalNumberofCell == 0 {
                self.dataHomeTable.isHidden = true
            }
        }
    }
    
    var shoppingProductList : ListProductDetail? {
        didSet {
            DispatchQueue.main.async {
                if let data = self.shoppingProductList?.data, data.count > 0{
                    self.noPrductLbl.isHidden = true
                }else{
                    self.noPrductLbl.isHidden = false
                }
                self.dataHomeTable.reloadData()
            }
        }
    }
     
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataHomeTable.dataSource = self
        dataHomeTable.delegate = self
        setNavigationBarItem()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func openMenu(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    @IBAction func openCartAction(_ sender: Any) {
        if Utilities.checkUserLogin() {
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getAllFavList()
    }
    
    
    func getAllFavList() {

        let param = ["_store": GlobalObject.selectedStore?.id ?? ""] as [String : Any]

        Webservices.instance.postMethod(API.wishProduct, param: param, header: false, completion: {(error :String?, data: ListProductDetail?) -> Void in
            if let success = data?.status, success.integerValue == 1 {
                self.shoppingProductList = data
                 
             }
         })
     }
    
 
    @objc func addProductInShoppingList(_ sender: UIButton) {
        if Utilities.checkUserLogin() {
            if AppDelegate.shared.listArray?.count ?? 0 > 0 {
                let controller =  PatientListVC.storyboardViewController()
                controller.shoppingObj = AppDelegate.shared.listArray
                //controller.delegate = self
                controller.selectedIndex = sender.tag
                print(sender.tag)
                if AppDelegate.shared.listArray?.count ?? 0 <= 6 {
                    controller.preferredContentSize = CGSize(width: 160, height: 35*(AppDelegate.shared.listArray?.count ?? 0) + 40)
                }
                else {
                    controller.preferredContentSize = CGSize(width: 160, height: 250)
                }
                showPopup(controller, sourceView: sender)
            }
        }
    }
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down, .up]
        self.present(controller, animated: true)
    }
    @objc func removeProductFormWishList(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].product?.id {
            let param:[String : Any] = ["_product": id, "_store":GlobalObject.selectedStore!.id!]
            Webservices.instance.deleteMethod(API.removeProductWishList , param: param) { (data : RemoveProduct? , error) in
                if data?.status?.integerValue == 1 {
                    self.getAllFavList()
                }
            }
        }
    }
    
    
    @objc func addProductInCart(sender: UIButton){
        if Utilities.checkUserLogin() {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = dataHomeTable.cellForRow(at: indexPath) as! FavCell
            sender.isSelected = true
            
            guard let product = self.shoppingProductList?.data?[sender.tag].product else {
                return
            }
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!, "quantity":cell.stepper.value]

            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                if data?.status?.integerValue == 1 {

                }
            })
        }else{
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension FavVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shoppingProductList?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FavCell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavCell
        cell.selectionStyle = .none
        
        
        cell.rightButton.addTarget(self, action: #selector(self.addProductInShoppingList), for: .touchUpInside)
        cell.rightButton.tag = indexPath.row

        cell.removeButton.addTarget(self, action: #selector(self.removeProductFormWishList(_:)), for: .touchUpInside)
        cell.removeButton.tag = indexPath.row
        
        
        cell.cartButton.addTarget(self, action: #selector(self.addProductInCart), for: .touchUpInside)
        cell.cartButton.tag = indexPath.row
        if let product =  self.shoppingProductList?.data?[indexPath.row].product{
            cell.configureDataForFav(product:product)
        }
        
//        if let product =  self.shoppingProductList?.data?[indexPath.row].product{
//            cell.productNameLbl.text = product.name?.english ?? ""
//            cell.priceLbl.text = "$ \(product.price ?? 0.0)"
//           
//            if let urlString = product.image, let url = URL(string: urlString){
//                cell.productImageView?.sd_setImage(with:url, placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
//            }
//        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return GlobalView.totalInstanceFromNib()
//    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 140
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let product =  self.shoppingProductList?.data?[indexPath.row].product else {
            return
        }
        let storyboard = UIStoryboard(name: "Product", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProductDetailTableViewController") as! ProductDetailTableViewController
        vc.product = product
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    @objc func favButtonClick(_ sender: UIButton) {
        
    }
    
    @objc func removeButtonIcon(_ sender: UIButton) {
        if self.totalNumberofCell == 0 {
            return
        }
        self.totalNumberofCell = self.totalNumberofCell - 1
        self.dataHomeTable.reloadData()
        
    }
    
    
    @objc func minusButtonIcon(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].id, let quantity = self.shoppingProductList?.data?[sender.tag].quantity {
            self.updateQuatitiy(id, Quatitiy: quantity - 1)
        }
    }
    
    @objc func plusButtonIcon(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].id, let quantity = self.shoppingProductList?.data?[sender.tag].quantity {
            self.updateQuatitiy(id, Quatitiy: quantity + 1)
        }
    }
    
    func updateQuatitiy(_ id : String, Quatitiy: Int )  {
        let param = ["_shoppinglist": id ,"quantity": Quatitiy ] as [String : Any]
        
        Webservices.instance.patchMethod(API.quantity, param: param) { (data : RemoveList? , error) in
            if data != nil {
                if data?.status == "true" {
                    self.getAllFavList()
                    //                    self.getProductFromtheStore(self.selectedList?.id ?? "")
                }
            }
        }
    }
    
    
}
