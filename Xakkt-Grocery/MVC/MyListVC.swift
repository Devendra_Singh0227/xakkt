//
//  MyListVC.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 08/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class MyListVC: UIViewController, Storyboardable     {
    
    @IBOutlet weak var noPrductLbl: UILabel!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var showListTblView: UITableView!
    @IBOutlet weak var dataHomeTable:UITableView!
    var totalNumberofCell = 3 {
        didSet {
            if totalNumberofCell == 0 {
                self.dataHomeTable.isHidden = true
            }
        }
    }
    var listArray : [ListData]? {
        didSet {
            DispatchQueue.main.async {
                self.showListTblView.reloadData()
            }
        }
    }
    
    
    var shoppingProductList : ListProductDetail? {
        didSet {
            DispatchQueue.main.async {
                if let data = self.shoppingProductList?.data, data.count > 0{
                    self.noPrductLbl.isHidden = true
                }else{
                    self.noPrductLbl.isHidden = false
                }
                self.dataHomeTable.reloadData()
            }
        }
    }
    var selectedList: ListData?
    @IBOutlet weak var selectedListLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataHomeTable.dataSource = self
        dataHomeTable.delegate = self
        self.showListTblView.dataSource = self
        self.showListTblView.delegate = self
        self.listView.isHidden = true
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if Utilities.checkUserLogin() == false {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
        else {
            self.getAllShoppingList()
            self.listView.shadow()
        }
    }
    
    
    @IBAction func cardShowButtonAction(_ sender: Any) {
        
        if Utilities.checkUserLogin() {
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    @IBAction func openMenu(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    func createList(_ listName: String)  {
        let param = ["name": listName, "_store": GlobalObject.selectedStore?.id ?? "" ] as [String : Any]
        
        Webservices.instance.postMethod(API.create_shoppinglist, param: param, header: false, completion: {(error :String?, data: CreateShoppingList?) -> Void in
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1, let dict =  data?.data{
                    self.selectedListLbl.text = listName
                    self.listArray?.append(dict)
                }
            }
        })
        
    }
    
    func getAllShoppingList() {
        let param = ["_store": GlobalObject.selectedStore?.id ?? ""] as [String : Any]
        Webservices.instance.postMethod(API.getShoppingList, param: param, header: false, completion: {(error :String?, data: ShoppingList?) -> Void in
            if let success = data?.status?.integerValue, success == 1, let array = data?.data, array.count > 0 {
                self.listArray = array
                DispatchQueue.main.async {
                    self.selectedListLbl.text = self.listArray?[0].name
                    self.getProductFromtheStore(self.listArray?[0].id ?? "")
                    self.selectedList = self.listArray?[0]
                }
            }
        })
    }
    
    func getProductFromtheStore(_ storeid : String) {
        Webservices.instance.getMethod(API.getShoppingList + storeid + "/products") { (data : ListProductDetail?, error) in
            if let success = data?.status, success.integerValue == 1 {
                self.shoppingProductList = data
            }else{
                self.shoppingProductList = nil
            }
        }
    }
    
    func DeleteProductFormList(_ storeid : String) {
        Webservices.instance.deleteMethod(API.remove_product + storeid , param: nil) { (data : RemoveProduct? , error) in
            if data != nil {
                if data?.status?.integerValue == 1 {
                    self.getProductFromtheStore(self.selectedList?.id ?? "")
                }
            }
        }
    }
    
        
    func RemoveList(_ storeid : String) {
        //  http://3.131.128.9:4800/api/v1/shoppinglist/5f658f230d58eb4a626f8f79/remove
        
        Webservices.instance.deleteMethod(API.getShoppingList + storeid + "/remove" , param: nil) { (data : RemoveProduct? , error) in
            if data?.status?.integerValue == 1 {
                self.getProductFromtheStore(self.selectedList?.id ?? "")
            }
        }
        
    }
    
    
    
   @objc func addProductWishList(_ sender : UIButton) {
    
    sender.isSelected = !sender.isSelected
    guard let product_id = self.shoppingProductList?.data?[sender.tag].product?.id else {
        return
    }
    
    
    if sender.isSelected {
        let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!,"wish_price":23.54, "max_price":48.9]

        Webservices.instance.postMethod(API.addWishList, param: param, header: true, completion: {(error :String?, data: ShoppingList?) -> Void in
            if let success = data?.status?.integerValue, success == 1, let array = data?.data, array.count > 0 {
                self.listArray = array
            }
        })
    }else{
        let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!]
        Webservices.instance.deleteMethod(API.removeProductWishList, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
            if data?.status?.integerValue == 1 {
            }
        })
    }
    
    
    
        
        
        

//        let param = ["_store": GlobalObject.selectedStore?.id ?? "" ,"_product": id ?? "", "wish_price": self.shoppingProductList?.data?[sender.tag].product?.price ?? 0.0 , "max_price": self.shoppingProductList?.data?[sender.tag].product?.price ?? 0.0 ] as [String : Any]
//
//
//        Webservices.instance.postMethod(API.addWishList, param: param, header: false, completion: {(error :String?, data: ShoppingList?) -> Void in
//            if let success = data?.status, success == "success", let array = data?.data, array.count > 0 {
//                self.listArray = array
//            }
//        })
        
    }
    
    
    //http://3.131.128.9:4800/api/v1/wishlist/add_product
    
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func dropDownButtonAction(_ sender: UIButton) {
        let controller =  PatientListVC.storyboardViewController()
        controller.shoppingObj = self.listArray
        controller.delegate = self
        
        if let array = self.listArray, array.count <= 6 {
            controller.preferredContentSize = CGSize(width: 160, height: 35*(array.count) + 40)
            showPopup(controller, sourceView: sender.superview ?? UIView())
        }
        else {
            controller.preferredContentSize = CGSize(width: 160, height: 240)
            showPopup(controller, sourceView: sender.superview ?? UIView())
        }
    }
    
    
    @IBAction func addListButtonAction(_ sender: Any) {
        
        if Utilities.checkUserLogin() == false {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
            return
        }
        
        let alertController = UIAlertController(title: "Enter a name for the new list", message: "", preferredStyle: UIAlertController.Style.alert)
        //        alertController.addTextField { (textField : UITextField!) -> Void in
        //            textField.placeholder = "Enter Second Name"
        //        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            
            if firstTextField.text != ""  && firstTextField.text?.count ?? 0 > 0 {
                self.createList(firstTextField.text ?? "")
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = ""
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down, .up]
        self.present(controller, animated: true)
    }
    
}

extension MyListVC: LoginScreenDelegate {
    func isSucusssFull(isLogin: Bool) {
        self.getAllShoppingList()
    }
}

extension MyListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.showListTblView {
            return listArray?.count ?? 0
        }
        else {
            return self.shoppingProductList?.data?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.showListTblView {
            let cell:ListCell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
            cell.textLbl.text = self.listArray?[indexPath.row].name ?? ""
            return cell
        }
        else  {
            let cell:FavCell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavCell
            cell.selectionStyle = .none
            cell.rightButton.addTarget(self, action: #selector(self.addProductWishList(_:)), for: .touchUpInside)
            cell.rightButton.tag = indexPath.row
            
            cell.removeButton.addTarget(self, action: #selector(self.removeButtonIcon(_:)), for: .touchUpInside)
            cell.removeButton.tag = indexPath.row
            
            cell.cartButton.addTarget(self, action: #selector(self.addProductInCart), for: .touchUpInside)
            cell.cartButton.tag = indexPath.row
            
            if let product =  self.shoppingProductList?.data?[indexPath.row].product{
                cell.configureData(product:product)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.showListTblView {
            self.selectedListLbl.text = self.listArray?[indexPath.row].name ?? ""
            self.listView.isHidden = true
            //            self.dropDownButton.setImage(#imageLiteral(resourceName: "arrow_down_black"), for: .normal)
            self.getProductFromtheStore(self.listArray?[indexPath.row].id ?? "")
        }else{
            guard let product =  self.shoppingProductList?.data?[indexPath.row].product else {
                return
            }
            let storyboard = UIStoryboard(name: "Product", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ProductDetailTableViewController") as! ProductDetailTableViewController
            vc.product = product
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == showListTblView {
            return 35
        }
        else {
            return 140
            
        }
    }
    
    @objc func favButtonClick(_ sender: UIButton) {
        
    }
    
    @objc func removeButtonIcon(_ sender: UIButton) {
       if let id = self.shoppingProductList?.data?[sender.tag].id {
         self.DeleteProductFormList(id)
       }
    }
    
    @objc func minusButtonIcon(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].id  , let quantity = self.shoppingProductList?.data?[sender.tag].quantity {
            self.updateQuatitiy(id, Quatitiy: quantity - 1)
        }
    }
    
    @objc func plusButtonIcon(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].id, let quantity = self.shoppingProductList?.data?[sender.tag].quantity {
            self.updateQuatitiy(id, Quatitiy: quantity + 1)
        }
    }
    
    func updateQuatitiy(_ id : String, Quatitiy: Int )  {
        let param = ["_shoppinglist": id ,"quantity": Quatitiy ] as [String : Any]
        
        Webservices.instance.patchMethod(API.quantity, param: param) { (data : RemoveList? , error) in
            if data != nil {
                if data?.status == "true" {
                    self.getAllShoppingList()
                    //                    self.getProductFromtheStore(self.selectedList?.id ?? "")
                }
            }
        }
    }
    
    @objc func addProductInCart(sender: UIButton){
        
        sender.isSelected = true
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell: FavCell = self.dataHomeTable.cellForRow(at: indexPath) as! FavCell
        
        if let product =  self.shoppingProductList?.data?[indexPath.row].product, let product_id = product.id{
            let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!, "quantity":cell.stepper.value]
            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                if data?.status?.integerValue == 1 {
                    
                }
            })
        }
    }
    
}

extension MyListVC : popDidSelectDelegate {
    
    func addProductInTheList(_ selectedList: ListData?, selectedIndex: Int) {
        self.selectedList = selectedList
        self.selectedListLbl.text = selectedList?.name ?? ""
        self.getProductFromtheStore(selectedList?.id ?? "" )
    }
    
    func deleteShoppingList(index:Int) {
        self.listArray?.remove(at: index)
    }
}
