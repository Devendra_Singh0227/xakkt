//
//  SetManullyAddressVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 05/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class AdressCell: UITableViewCell {
    
    static let identifier = "AdressCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class SetManullyAddressVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var addressTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addressTableView.dequeueReusableCell(withIdentifier: "AdressCell", for: indexPath) as? AdressCell
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
