//
//  ChangeStore_VC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 31/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import HCSStarRatingView

class Change_store_Cell: UITableViewCell {
    
    @IBOutlet weak var product_Image: DesignableImageView!
    @IBOutlet weak var address_Lbl: UILabel!
    @IBOutlet weak var itemskm_Lbl: UILabel!
    @IBOutlet weak var payment_Lbl: UILabel!
    @IBOutlet weak var orders_Lbl: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var rating_Lbl: UILabel!
        
    static let identifier = "Change_store_Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class ChangeStore_VC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var stores_TableView: UITableView!
    var addresView = GlobalView.ChangeAdressFromNib()
    var listData : ListAllStore? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getListStore()
    }
    
    @IBAction func menu_Btn(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func change_Address_Btn(_ sender: DesignableButton) {
        addresView = GlobalView.ChangeAdressFromNib()
        addresView.cancel_Btn.addTarget(self, action: #selector(removeAdressView), for: .touchUpInside)
        addresView.manually_Btn.addTarget(self, action: #selector(self.setManuallyAddress_Btn), for: .touchUpInside)
        self.view.addSubview(addresView)
    }
    
   @objc func removeAdressView(){
        addresView.removeFromSuperview()
    }
    
    @objc func setManuallyAddress_Btn(){
        addresView.removeFromSuperview()
        let VC = storyboard?.instantiateViewController(identifier: "SetManullyAddressVC") as? SetManullyAddressVC
        navigationController?.pushViewController(VC!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = stores_TableView.dequeueReusableCell(withIdentifier: "Change_store_Cell", for: indexPath) as? Change_store_Cell
        cell?.address_Lbl.text = self.listData?.data?[indexPath.row].name
        cell?.payment_Lbl.text = "\(self.listData?.data?[indexPath.row].address ?? ""), \(self.listData?.data?[indexPath.row].city ?? ""), \(self.listData?.data?[indexPath.row].state ?? "")"
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data = self.listData?.data?[indexPath.row] else {
            return
        }
        GlobalObject.selectedStore = data
        SceneDelegate.sharedInstance().createMenuView()

    }
    
    func getListStore() {
        Webservices.instance.getMethod(API.listAllStore) { (data : ListAllStore?, error) in
            if (data != nil) {
                print(data)
                DispatchQueue.main.async {
                    if data?.status?.integerValue == 1 {
                        self.listData = data
                        self.stores_TableView.reloadData()
                    } else {
                        self.showToast(message: data?.message ?? "")
                    }
                }
            }
            else {
            }
        }
    }
}
