//
//  HomeVC.swift
//  Xakkt-Grocery
//
//  Created by Developer on 01/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout


class HomeVC: UIViewController, LoginScreenDelegate {
    
    fileprivate var homeData : Home? = nil {
        didSet {
            self.dataHomeTable.reloadData()
            //            self.showCurrentLocationOnMap()
        }
    }
    
   fileprivate var listArray : [ListData]?
    
    @IBOutlet weak var cart_count_lbl:UILabel!
    @IBOutlet weak var dataHomeTable:UITableView!
    var id : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: "iSLogin") == nil{
            Chain.keychainObject.clear()
        }
        
        self.getHomeData()
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("RefreshTotalCount"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.getHomeData()
        if Utilities.checkUserLogin() {
            self.getAllShoppingList()
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.cart_count_lbl.text = "\(GlobalObject.totalProductInCart ?? 0)"
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func openMenu(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func reviews_Btn(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(identifier: "Reviews_ViewController") as? Reviews_ViewController
        self.present(VC!, animated: false, completion: nil)
    }
    
    @IBAction func cardShowButtonAction(_ sender: Any) {
        
        if Utilities.checkUserLogin() {
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    
    func getHomeData() {
        Webservices.instance.getMethod(API.dashboard + (GlobalObject.selectedStore?.id ?? "")) { (data : Home?, error) in
            if (data != nil) {
                DispatchQueue.main.async {
                    if data?.status?.integerValue == 1 {
                        self.homeData = data
                    }
                }
            }
            else {
            }
        }
        
        Webservices.instance.getMethod(API.get_cart + (GlobalObject.selectedStore?.id ?? "")) { (data : CartCount?, error) in
            DispatchQueue.main.async {
                if let total_product_count = data?.data?.total_products?.integerValue {
                    self.cart_count_lbl.text = "\(total_product_count)"
                    GlobalObject.totalProductInCart = data?.data?.total_products?.integerValue
                }
            }
        }
    }
    
    func isSucusssFull(isLogin: Bool) {
//        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getAllShoppingList() {
        let param = ["_store": GlobalObject.selectedStore?.id ?? "" ,"_user": GlobalObject.user?.data?.user?.id ?? "" ] as [String : Any]
        
        Webservices.instance.postMethod(API.getShoppingList, param: param, header: false, completion: {(error :String?, data: ShoppingList?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    AppDelegate.shared.listArray = data?.data ?? []
                    self.listArray = data?.data ?? []
                }
            }
        })
    }
}



extension HomeVC:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        }
        return 20
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 20))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width-10, height: 20)
        if let header =  self.homeData?.data?[section].subType, header == "order_again"{
            label.text = "Order Again?"
        }else if let header =  self.homeData?.data?[section].subType, header.lowercased() == "deals", let store_name = GlobalObject.selectedStore?.name{
            label.text = header + " @ " + store_name
        }else if let header =  self.homeData?.data?[section].subType, header.lowercased() == "trending"{
            label.text = "Trending Nearby"
        }
        label.textColor = .darkGray
        headerView.addSubview(label)
        headerView.backgroundColor = .white
        
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.homeData?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK: - Table View
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if self.homeData?.data?[indexPath.section].type == .banner {
            let cell:BannerTableCell = tableView.dequeueReusableCell(withIdentifier: "BannerTableCell", for: indexPath) as! BannerTableCell
            cell.collectionView.tag = indexPath.section
            cell.delegate = self
            if let array = self.homeData?.data?[indexPath.section].banner, array.count > 0 {
                cell.messageLbl.isHidden = true
                cell.configureCell(banner: array, path: self.homeData?.data?[indexPath.section].path)
            }else{
                cell.messageLbl.isHidden = false
                cell.messageLbl.text = self.homeData?.data?[indexPath.section].message
            }
            return cell
        }
        else {
            let cell:HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "ProductTableCell", for: indexPath) as! HomeTableCell
            cell.updateProductDelegate = self
            cell.collectionView.tag = indexPath.section
            cell.DealId = {(id) in
                    print(id)
                self.didselect_CollectionView(indexPath: id)
            }
            if let array = self.homeData?.data?[indexPath.section].product, array.count > 0 {
                cell.messageLbl.isHidden = true
                cell.configureCell(items: array, type: .product, path : self.homeData?.data?[indexPath.section].path, listArray: self.listArray)
            }else{
                cell.messageLbl.text = self.homeData?.data?[indexPath.section].message
                cell.messageLbl.isHidden = false
            }
            cell.navigation = self.navigationController
            return cell
        }
    }
    
    func didselect_CollectionView(indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Product", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "ProductDetailTableViewController") as! ProductDetailTableViewController
//        vc.shoe = self.homeData?.data?[indexPath.section].product
//        self.navigationController?.pushViewController(vc, animated: true)
//        
//        let vc = storyboard.instantiateViewController(identifier: "ProductListBYBanner") as! ProductListBYBanner
//
//        if let dict = self.homeData?.data?[0].banner?[0] {
//            vc.dealId = dict.deal?.id
//            vc.dealId = dict.deal?.name
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}

extension HomeVC:HomeTableCellDelegate {
    func returnCartValue(cart: Cart?) {
        DispatchQueue.main.async {
        if let total_product_count = cart?.subtotal?.quantity?.integerValue {
            self.cart_count_lbl.text = "\(total_product_count)"
            GlobalObject.totalProductInCart = total_product_count
        }
        }
    }
    
    
    func didselectProductCollection(indexPath: IndexPath, collectionview: UICollectionView) {
        let storyboard = UIStoryboard(name: "Product", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProductDetailTableViewController") as! ProductDetailTableViewController
        vc.product = self.homeData?.data?[collectionview.tag].product?[indexPath.item]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeVC:BannerTableCellDelegate {
    func didselectBannerCollection(indexPath: IndexPath, collectionview: UICollectionView) {
        let storyboard = UIStoryboard(name: "Product", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProductListBYBanner") as! ProductListBYBanner
        
        if let dict = self.homeData?.data?[0].banner?[indexPath.row] {
            vc.dealId = dict.deal?.id
            vc.dealTitle = dict.deal?.name ?? ""
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}





