//
//  LoginViewController.swift
//  ElSuperNew
//
//  Created by SABYASACHI POLLEY on 21/02/18.
//  Copyright © 2018 Sabyasachi Polley. All rights reserved.
//

import UIKit

protocol LoginScreenDelegate{
    func isSucusssFull(isLogin : Bool)
}

class LoginViewController: UIViewController,Storyboardable, RegisterScreenDelegate {
  
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var resgisterButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var topHeoght: NSLayoutConstraint!
    var delegate : LoginScreenDelegate?
    var BundlePath = AppDelegate.shared.bundlePath
    var fromStart = true
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(removeKeyBoard))
        self.view.addGestureRecognizer(tap)
        if UIScreen.main.nativeBounds.height  == 2436{
            self.topHeoght.constant = 84
        }
        self.navigationController?.navigationBar.isHidden = true
       // setUp()
    }

    func setUp(){
        userName.placeholder = AppDelegate.shared.bundlePath?.localizedString(forKey: "Email Address*", value: nil, table: nil)
        password.placeholder = AppDelegate.shared.bundlePath?.localizedString(forKey: "Password", value: nil, table: nil)
        loginButton.setTitle(AppDelegate.shared.bundlePath?.localizedString(forKey: "Login", value: nil, table: nil), for: .normal)
        resgisterButton.setTitle(AppDelegate.shared.bundlePath?.localizedString(forKey: "Register", value: nil, table: nil), for: .normal)
        closeButton.setTitle(AppDelegate.shared.bundlePath?.localizedString(forKey: "Close", value: nil, table: nil), for: .normal)
        //forgotPasswordButton.setTitle(AppDelegate.shared.bundlePath?.localizedString(forKey: "Forgot your password?", value: nil, table: nil), for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func facebookLogin(_ sender: Any) {
        
    }
    
    @IBAction func googleLogin(_ sender: Any) {
        
    }
    
    @IBAction func forgot_Btn(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(identifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.present(VC!, animated: true, completion: nil)
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if userName.text == ""{
            self.showAlert(title: "Alert!", msg: "Please enter your user name.")
            return
        }
        if password.text == ""{
            self.showAlert(title: "Alert!", msg: "Please enter your password.")
            return
        }
        self.view.endEditing(true)

        let param = ["email": userName.text ?? "" ,"password": password.text ?? "", "device_token":AppDelegate.shared.fcmToken, "device_type":"ios"] as [String : Any]

        Webservices.instance.postMethod(API.login, param: param, header: false, completion: {(error :String?, data: Login?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    UserDefaults.standard.set("Save", forKey: "iSLogin")
                    Utilities.saveModelInKeychan(ModelObj: data, key: keys.userData)
                    self.delegate?.isSucusssFull(isLogin: true)
                    self.navigationController?.dismiss(animated: false, completion: nil)
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }

    @IBAction func registerTapped(_ sender: Any) {
        
        let vc = RegisterViewController.storyboardViewController()
        vc.delegate = self
        let navC = UINavigationController(rootViewController: vc)
        self.navigationController?.present(navC, animated: true, completion: nil)
        
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
//
//    if AppDelegate.shared.selectedMallID == ""{
//        let story = UIStoryboard(name: "LoginStory", bundle: Bundle.main)
//        let start = story.instantiateViewController(withIdentifier: "StartScreen") as! StartScreenViewController
//        AppDelegate.shared.window?.rootViewController = start
//        AppDelegate.shared.window?.makeKeyAndVisible()
//        }else{
//
////            AppDelegate.shared.setUpBurgerMenu()
//        }
    }
    
    func isSucusssFull(isSign: Bool) {
        if isSign {
            DispatchQueue.main.asyncAfter(deadline: .now() +  1.0) {
                self.showAlert(title: "Succssful Registered!", msg: "Please Login here")
            }
        }
        
      }

}
extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
