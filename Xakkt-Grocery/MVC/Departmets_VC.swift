//
//  Departmets_VC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 07/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class Department_Cell: UITableViewCell {
   
    @IBOutlet weak var department_Image: UIImageView!
    @IBOutlet weak var departmentName_Lbl: UILabel!
    @IBOutlet weak var stores_Lbl: UILabel!

    static let identifier = "Department_Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class Departmets_VC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var departments_TableView: UITableView!
    var departments = [categories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        get_Department_List()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menu_Btn(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = departments_TableView.dequeueReusableCell(withIdentifier: "Department_Cell", for: indexPath) as? Department_Cell
        cell?.departmentName_Lbl.text = self.departments[indexPath.row].name
        cell?.stores_Lbl.text = "No_Of_Stores: \(self.departments[indexPath.row].no_of_stores ?? 0)"
        let url = "http://3.131.128.9:4800/images/departments/"
        let image = url +  self.departments[indexPath.row].logo!
         cell?.department_Image?.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func get_Department_List() {
        Webservices.instance.getMethod(API.departments) { (data : Categories?, error) in
                    //print(data)
            if data?.status == "success", let array = data?.data, array.count > 0 {
                self.departments = array
                print(self.departments)
                DispatchQueue.main.async {
                    self.departments_TableView.reloadData()
                }
            }else if data?.status == "success", let array = data?.data, array.count == 0 {
                let noOrderView = self.view.viewWithTag(100)
                noOrderView?.isHidden = false
            }else if data?.status == "success", let array = data?.data, array.count == 0 {
            }
        }
    }
    
}
