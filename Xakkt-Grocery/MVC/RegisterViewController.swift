//
//  RegisterViewController.swift
//  ElSuperNew
//
//  Created by SABYASACHI POLLEY on 21/02/18.
//  Copyright © 2018 Sabyasachi Polley. All rights reserved.
//

import UIKit
import WebKit

protocol RegisterScreenDelegate{
    func isSucusssFull(isSign : Bool)
}

class RegisterViewController: UIViewController, Storyboardable {
    var firstTimeCount = 0
    var activePin = ""
    let app = AppDelegate.shared
    var searchingStore = false
    var delegate : RegisterScreenDelegate?

    @IBOutlet var lblSelectStoreUsingZip: UILabel!
    @IBOutlet var selectStoreUsingZip_Stack: UIStackView!
    @IBOutlet var store_Height: NSLayoutConstraint!
    @IBOutlet var setEmailAndPassword: UILabel!
    @IBOutlet var letsGetToKnow: UILabel!
    @IBOutlet var first_name: UITextField!
    @IBOutlet var last_name: UITextField!
    @IBOutlet var email_adress: UITextField!
    @IBOutlet var phone_number: UITextField!
    @IBOutlet var birthday: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var confirm_password: UITextField!
    @IBOutlet var gender_label: UILabel!
    @IBOutlet var gender_view: UIView!
    @IBOutlet var date_picker_view: UIDatePicker!
    @IBOutlet var done_button: UIButton!
    @IBOutlet var promotion: UIButton!
    @IBOutlet var scrollviewmain: UIScrollView!
    @IBOutlet var zipcode: UITextField!
    @IBOutlet var trmsConditionTxt: UITextView!
    @IBOutlet var lblStorename: UILabel!
    @IBOutlet weak var lblHi: UILabel!
    @IBOutlet weak var letsGetToKnowYou: UILabel!
    @IBOutlet weak var requiredField: UILabel!
    @IBOutlet weak var setHomeStore: UILabel!
    @IBOutlet weak var useCurrentLocation: UILabel!
    @IBOutlet weak var checkHereToOpt: UILabel!
    @IBOutlet weak var setAPassword: UILabel!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var storePicker: UIPickerView!
    @IBOutlet var lblSignUpToElSuper: UILabel!
    @IBOutlet var btnTerms: UIButton!
    @IBOutlet var btnPrivacy: UIButton!
    
    let BundlePath = AppDelegate.shared.bundlePath
    var otpTextRef = UITextField()
    var gender: String!
    var promotion_txt = "0"
    var elsuperBucksChecked = true
    @IBOutlet weak var topHeoght: NSLayoutConstraint!
    var parameters:[String: String] = [:]
    
    @IBAction func showStorePicker(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(removePickers))
        scrollviewmain.addGestureRecognizer(tap)
        storePicker.isHidden = false
       // if arrStore.count > 0{
//            zipcode.text = arrStore[0].name ?? ""
//            let app = (UIApplication.shared.delegate as! AppDelegate)
//            app.selectedMallID = arrStore[0].id!
//            app.selectedMallName = arrStore[0].name!
//            UserDefaults.standard.set(arrStore[0].id!, forKey: "MallName")
//            UserDefaults.standard.set("\(arrStore[0].name!)", forKey: "MallId")
//
//        }else{
            storePicker.isHidden = true
            selectStoreUsingZip_Stack.isHidden = true
            store_Height.constant = 8
            lblSelectStoreUsingZip.text = "Select store using zip code \(activePin)"
            self.getOtherStore()
       // }
    }
    
    @IBAction func showTearmsElsuperBucks() {
        let app = UIApplication.shared.delegate as! AppDelegate
        let urlString = "https://elsupermarkets.com/mobiles/superbucks_cms/term/\(app.language)"
        UIApplication.shared.open(URL(string: urlString)!, options: [:], completionHandler: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "captcha"{
//        let vc = segue.destination as! CaptchaViewController
//        vc.params = self.parameters
//        vc.email  = self.email_adress.text!
//        vc.password = self.password.text!
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let app = UIApplication.shared.delegate as! AppDelegate
        app.selectedMallID = "abc"
       // setupTerms()
   
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        self.date_picker_view.backgroundColor = .white
        self.date_picker_view.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(removePickers))
        scrollviewmain.addGestureRecognizer(tap)
        let calendar = Calendar.current
        let sixteenAgo = calendar.date(byAdding: .year, value: -15, to: Date())
        date_picker_view.maximumDate = sixteenAgo
        NotificationCenter.default.addObserver(self, selector: #selector(storeSelected), name: NSNotification.Name(rawValue: "STORESELECTED"), object: nil)
        if UIScreen.main.nativeBounds.height  == 2436{
            self.topHeoght.constant = 84
        }
       // setUp()
        print(self.view.frame.height)
        scrollviewmain.frame = CGRect(x: 0, y: 0, width: 320, height: 1032)
        scrollviewmain.setNeedsDisplay()
       // captchaWebView.loadRequest(URLRequest(url: URL(string: "http://debtech.net/demous/elsupermarkets/users/recaptcha")!))
      //  getStoreLIst(lat: app.lat, lng: app.lng)//33.900731, -118.145274
        self.navigationController?.navigationBar.isHidden = true

    }
   
    
    func setUp(){
        lblSignUpToElSuper.text = BundlePath?.localizedString(forKey: "Sign up to ElsuperBucks", value: nil, table: nil)
        btnTerms.setTitle(BundlePath?.localizedString(forKey: "(Terms", value: nil, table: nil), for: .normal)
        btnPrivacy.setTitle(BundlePath?.localizedString(forKey: "Privacy Policy)", value: nil, table: nil), for: .normal)
        //lblHi.text  = BundlePath?.localizedString(forKey: "Hi!", value: nil, table: nil)
        setEmailAndPassword.text = BundlePath?.localizedString(forKey: "SET EMAIL AND PASSWORD", value: nil, table: nil)
        letsGetToKnowYou.text = BundlePath?.localizedString(forKey: "Let's get to know you:", value: nil, table: nil)
        //setAPassword.text = BundlePath?.localizedString(forKey: "Set a password:", value: nil, table: nil)
        //requiredField.text = BundlePath?.localizedString(forKey: "*Required Fields", value: nil, table: nil)
        setHomeStore.text = BundlePath?.localizedString(forKey: "Set your home store:", value: nil, table: nil)
        checkHereToOpt.text = BundlePath?.localizedString(forKey: "Check here to opt out of promotional emails", value: nil, table: nil)
        useCurrentLocation.text = BundlePath?.localizedString(forKey: "Find other store using zip code", value: nil, table: nil)
        zipcode.placeholder = BundlePath?.localizedString(forKey: "Select store", value: nil, table: nil)
        confirm_password.placeholder = BundlePath?.localizedString(forKey: "Confirm Password", value: nil, table: nil)
        password.placeholder = BundlePath?.localizedString(forKey: "Password", value: nil, table: nil)
        genderTextField.placeholder =  BundlePath?.localizedString(forKey: "Gender (Optional)", value: nil, table: nil)
        birthday.placeholder =  BundlePath?.localizedString(forKey: "Birthday (MM/YY)", value: nil, table: nil)
        phone_number.placeholder = BundlePath?.localizedString(forKey: "Phone number*", value: nil, table: nil)
        email_adress.placeholder = BundlePath?.localizedString(forKey: "Email Address*", value: nil, table: nil)
        last_name.placeholder = BundlePath?.localizedString(forKey: "Last Name*", value: nil, table: nil)
        first_name.placeholder = BundlePath?.localizedString(forKey: "First Name*", value: nil, table: nil)
        maleButton.setTitle(BundlePath?.localizedString(forKey: "Male", value: nil, table: nil), for: .normal)
        femaleButton.setTitle(BundlePath?.localizedString(forKey: "Female", value: nil, table: nil), for: .normal)
        declineButton.setTitle(BundlePath?.localizedString(forKey: "Decline", value: nil, table: nil), for: .normal)
        done_button.setTitle(BundlePath?.localizedString(forKey: "Submit", value: nil, table: nil), for: .normal)
    }
    
    
    @objc func storeSelected() {
        let myString = (BundlePath?.localizedString(forKey: "Selected", value: nil, table: nil))!+": "+AppDelegate.shared.selectedMallName
        let myRange = NSRange(location: 0, length: (BundlePath?.localizedString(forKey: "Selected", value: nil, table: nil))!.count+1)
        let attributed = NSMutableAttributedString(string: myString)
        attributed.addAttribute(NSAttributedString.Key  .font, value: UIFont.systemFont(ofSize: 15) , range: myRange)
        lblStorename.attributedText = attributed
    }
    @IBAction func currentLocation(){
        AppDelegate.shared.getAddress(lat: Double(AppDelegate.shared.lat)!, lng: Double(AppDelegate.shared.lng)!,completion: { zip in
            print(zip)
            self.validatePin(pin: zip)
        })
    }
    
    func validatePin(pin: String){
 //       showLoader(text: "Validating zip..")
//        API.shared.validate(pin: pin) { (result) in
//            self.hideLoader()
//            let dict = NSDictionary(dictionary: result!)
//            let storyBoard = UIStoryboard(name: "LoginStory", bundle: Bundle.main)
//            let chooseMall = storyBoard.instantiateViewController(withIdentifier: "chooseMall") as! ChooseMallViewController
//            chooseMall.comingFromRegViewController = true
//            let app = AppDelegate.shared
//            let dictEncData = dict["encData"]! as! [String: Any]
//            print(dictEncData["latitude"]!)
//            print(dictEncData["longitude"]!)
//            app.lat = dictEncData["latitude"]! as! String
//            app.lng = dictEncData["longitude"]! as! String
//            if dictEncData["message"]! as! String != "Zip code exist."{
//                self.showAlert(title: "Alert!", msg: "Zip code does not exist.")
//                return
//            }
//            self.present(chooseMall, animated: true, completion: nil)
//            self.zipcode.text = ""
//        }
    }
    @objc func keyboardWillShow(notification:Notification){
        
//        var userInfo = notification.userInfo!
//        var keyboardFrame:CGRect = (userInfoUIResponder.keyboardFrameBeginUserInfoKeyy,] as! NSValue).cgRectValue
        
       // keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardFrame:CGRect  = keyboardSize
            var contentInset:UIEdgeInsets = self.scrollviewmain.contentInset
                  contentInset.bottom = keyboardFrame.size.height
                  scrollviewmain.contentInset = contentInset
          }
        
      
    }
    
    @objc func keyboardWillHide(notification:Notification){
        
        let contentInset:UIEdgeInsets = .zero
        scrollviewmain.contentInset = contentInset
    }
    @objc func removePickers(){
        self.date_picker_view.isHidden = true
        self.toolBar.isHidden = true
        self.gender_view.isHidden = true
//        self.storePicker.isHidden = true
        if searchingStore == true {
            selectStoreUsingZip_Stack.isHidden = false
            store_Height.constant = 32
            lblSelectStoreUsingZip.text = "Select store using zip code \(activePin)"
        }else{
            selectStoreUsingZip_Stack.isHidden = true
            store_Height.constant = 8
        }
        self.view.endEditing(true)
    }
    
    @IBAction func selectPromotion(sender:UIButton){
        if sender.image(for: .normal) == #imageLiteral(resourceName: "unchecked"){
            self.promotion_txt = "1"
            sender.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        }else{
            self.promotion_txt = "0"
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        }
    }
    @IBAction func getDateOfBirth(){
        self.date_picker_view.isHidden = false
        self.toolBar.isHidden = false
    }
    
    @IBAction func setGender(){
        gender_view.isHidden = false
    }
    @IBAction func doneWithDatePicker(){
        if self.birthday.text != ""{
            if calculateAge(birthDate: date_picker_view.date){
                self.removePickers()
            }else{
                self.showAlert(title: "Alert!", msg: "Your age must be above 15 years.")
            }
        }else{
            self.removePickers()
        }
    }
    
    @IBAction func dateSelected(picker: UIDatePicker){
        let df = DateFormatter()
        df.dateFormat = "MM-dd-yyyy"
        let stringDate = df.string(from: picker.date)
        self.birthday.text = stringDate
    }
    func calculateAge(birthDate: Date)->Bool{
        let now = Date()
        let birthday: Date = birthDate
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year!
        if age>=15{
            return true
        }else{
            return false
        }
    }
    @IBAction func selectGender(sender: UIButton){
        switch sender.tag {
        case 0:
            self.gender="male"
            self.genderTextField.text = "male"
        case 1:
            self.gender="female"
            self.genderTextField.text="female"
        default:
            self.gender=""
            self.genderTextField.text="Decline"
        }
        self.hideGenderView()
    }
    
    func hideGenderView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.gender_view.isHidden=true
        }, completion: nil)
    }
    
    func checkValues()->Bool{
        for txtF in scrollviewmain.subviews{
            if txtF.isKind(of: UITextField.self){
                txtF.resignFirstResponder()
            }
        }
        if first_name.text == ""{
            self.showAlert(title: "Alert!", msg: "First Name Missing")
            first_name.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if last_name.text == ""{
            self.showAlert(title: "Alert!", msg: "Last Name Missing")
            last_name.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if email_adress.text == ""{
            self.showAlert(title: "Alert!", msg: "Email Missing")
            email_adress.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if !self.isValidEmail(testStr: email_adress.text!){
            self.showAlert(title: "Alert!", msg: "Email is invalid. Please provide a valid email address.")
            email_adress.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if phone_number.text == ""{
            self.showAlert(title: "Alert!", msg: "Phone Number Missing")
            phone_number.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if phone_number.text?.replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "+1", with: "").replacingOccurrences(of: " ", with: "").count   != 10{
            self.showAlert(title: "Alert!", msg: "Invalid Phone Number")
            phone_number.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if self.birthday.text != ""{
            if !calculateAge(birthDate: date_picker_view.date){
                self.showAlert(title: "Alert!", msg: "Your age must be above 15 years.")
            
                return false
            }
        }
        if genderTextField.text == ""{
            self.showAlert(title: "Alert!", msg: "Gender Missing")
            genderTextField.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if password.text! == "" {
            self.showAlert(title: "Alert!", msg: "Please enter a password")
            password.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if validatePassword(text: password.text!) != "Password must contain "{
            self.showAlert(title: "Alert!", msg: validatePassword(text: password.text!))
            password.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if confirm_password.text! == "" || confirm_password.text!.count < 8 {
            self.showAlert(title: "Alert!", msg: "Passwords Don't Match!")
            confirm_password.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        if password.text! != confirm_password.text!{
            self.showAlert(title: "Alert!", msg: "Passwords Don't Match!")
            confirm_password.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            return false
        }
        return true
    }
    
    @IBAction func register(){

        if checkValues()==true {
            let app = AppDelegate.shared
            if app.selectedMallID != "" {
                //self.showLoader(text: (BundlePath?.localizedString(forKey: "Registering...", value: nil, table: nil))!)
                //self.showLoader(text: "Registering...")
                var elsuperBucksValue = "1"
                if elsuperBucksChecked == false{
                    elsuperBucksValue = "0"
                }
                let params = ["first_name":self.first_name.text ?? "", "last_name":self.last_name.text ?? "", "email":self.email_adress.text ?? "", "password":self.password.text ?? "", "dob":self.birthday.text ?? "", "gender":self.gender ?? "", "mobile_no":self.phone_number.text ?? "", "zip":activePin, "store_zip":activePin, "need_promotional_emails":self.promotion_txt, "mall_id":app.selectedMallID,"need_elsuper_bucks":elsuperBucksValue]
                self.parameters = params
                UserDefaults.standard.removeObject(forKey: "uid")
                UserDefaults.standard.removeObject(forKey: "pwd")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(self.email_adress.text!, forKey: "uid")
                UserDefaults.standard.set(self.password.text!, forKey: "pwd")
                UserDefaults.standard.synchronize()
                self.performSegue(withIdentifier: "captcha", sender: nil)
                
            }else{
                self.showAlert(title: "Alert!", msg: "Please choose your favourite store!")
                zipcode.becomeFirstResponder()
            }
        }
    }
    
    func checkOTP(){
//        let story = UIStoryboard(name: "LoginStory", bundle: Bundle.main)
//        let otpVC = story.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
//        otpVC.email = self.email_adress.text!
//        otpVC.pwd = self.password.text!
//        self.present(otpVC, animated: true, completion: nil)
//        let alert = UIAlertController(title: "OTP Verification", message: "A one time verification code hasbeen sent to your email. Please use this code to verify your email address with us and proceed to next step. Thank you!", preferredStyle: .alert)
//        alert.addTextField { (textField) in
//            textField.placeholder = "Enter Verification Code";
//            textField.clearButtonMode = .whileEditing
//            textField.borderStyle = .roundedRect
//            textField.isSecureTextEntry = true
//            textField.textAlignment = .center
//            self.otpTextRef = textField;
//        }
//        alert.addAction(UIAlertAction(title: "Proceed to next step", style: .default, handler: { (_) in
//            self.verifyOTP()
//        }))
//        self.present(alert, animated: true, completion: nil)
    }
    
    func verifyOTP(){
        /*
        self.showLoader(text: (BundlePath?.localizedString(forKey: "OTP verifying..", value: nil, table: nil))!)
        //self.showLoader(text: "OTP verifying..")
        API.shared.verifyOTP(otp: otpTextRef.text!) { (result) in
            if result.error! != "0"{
                self.checkOTP()
                self.hideLoader()
            }else{
                self.hideLoader()
                self.showLoader(text: (self.BundlePath?.localizedString(forKey: "Logging In..", value: nil, table: nil))!)
                print("OTP verifies")
                let app = AppDelegate.shared
                
               
                API.shared.Login(userName: self.email_adress.text!, password: self.password.text!) { (result) in
                    //if result?.encData?.message == "You are logged in successfully."{
                    if result?.encData?.error == "0"{
                        UserDefaults.standard.removeObject(forKey: "uid")
                        UserDefaults.standard.removeObject(forKey:  "pwd")
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(self.email_adress.text!, forKey: "uid")
                        UserDefaults.standard.set(self.password.text!, forKey: "pwd")
                        UserDefaults.standard.synchronize()
                       // app.saveUserDataToDefaults(user: (result?.encData?.user!)!)
                        self.performSegue(withIdentifier: "superbucks", sender: nil)
                    }
                    self.hideLoader()
                }
            }
        }
    }
    
    func setupTerms(){
        let strAll = BundlePath?.localizedString(forKey: "By clicking 'Next', you agree to our Terms and that you read our Privacy Policy, which includes our Cookie Use", value: nil, table: nil)
        let attributed = NSMutableAttributedString(string: strAll!)
        let termsRange = attributed.mutableString.range(of: (BundlePath?.localizedString(forKey: "Terms", value: nil, table: nil))!)
        let privRange = attributed.mutableString.range(of: (BundlePath?.localizedString(forKey: "Privacy Policy", value: nil, table: nil))!)
        let termsRange1 = attributed.mutableString.range(of: (BundlePath?.localizedString(forKey: "terminos", value: nil, table: nil))!)
        let privRange1 = attributed.mutableString.range(of: (BundlePath?.localizedString(forKey: "poliza de privasidad", value: nil, table: nil))!)
        attributed.addAttribute(NSAttributedStringKey.link, value: "https://elsupermarkets.com/mobiles/cms/terms/\(AppDelegate.shared.language)", range: termsRange)
        attributed.addAttribute(NSAttributedStringKey.link, value: "https://elsupermarkets.com/mobiles/cms/privacy/\(AppDelegate.shared.language)", range: privRange)
        attributed.addAttribute(NSAttributedStringKey.link, value: "https://elsupermarkets.com/mobiles/cms/terms/\(AppDelegate.shared.language)", range: termsRange1)
        attributed.addAttribute(NSAttributedStringKey.link, value: "https://elsupermarkets.com/mobiles/cms/privacy/\(AppDelegate.shared.language)", range: privRange1)
        trmsConditionTxt.attributedText = attributed
        trmsConditionTxt.isEditable = false
        trmsConditionTxt.delegate = self
 */
    }
    @IBAction func back(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func validatePassword(text: String) -> String{
        var msg = "Password must contain "
        if text.count < 8{
            msg = msg + "at least 8 characters "
        }
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if text.rangeOfCharacter(from: characterset.inverted) == nil {
            msg =  msg + ", " + "one special character "
        }
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = text.rangeOfCharacter(from: decimalCharacters)
        if decimalRange == nil {
            msg = msg + ", " + "one number "
        }
        let upperCharacters = CharacterSet.uppercaseLetters
        let uppRange = text.rangeOfCharacter(from: upperCharacters)
        if uppRange == nil {
            msg =  msg + ", " + "one upper case letter "
        }
        return msg
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {

        if checkValues()==true {
            let app = AppDelegate.shared
            if app.selectedMallID != "" {
                //self.showLoader(text: (BundlePath?.localizedString(forKey: "Registering...", value: nil, table: nil))!)
                //self.showLoader(text: "Registering...")
//                var elsuperBucksValue = "1"
//                if elsuperBucksChecked == false{
//                    elsuperBucksValue = "0"
//                }
                let params = ["first_name":self.first_name.text ?? "", "last_name":self.last_name.text ?? "", "email":self.email_adress.text ?? "", "password":self.password.text ?? "", "dob":self.birthday.text ?? "", "gender":self.gender ?? "", "contact_no":self.phone_number.text ?? "", "status":true,"ncrStatus":true, "superbuckId":"123456","timezone":"US"] as [String : Any]
                
                UserDefaults.standard.removeObject(forKey: "uid")
                UserDefaults.standard.removeObject(forKey:  "pwd")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(self.email_adress.text!, forKey: "uid")
                UserDefaults.standard.set(self.password.text!, forKey: "pwd")
                UserDefaults.standard.synchronize()
                

                Webservices.instance.postMethod(API.createUser, param: params, header: false, completion: {(error :String?, data: Register?) -> Void in
                    
                    print(data as Any)
                    if data != nil {
                        DispatchQueue.main.async {
                            if data?.status?.integerValue == 1 {
//                                Utilities.saveModelInKeychan(ModelObj: data, key: keys.userData)
                                    self.delegate?.isSucusssFull(isSign: true)
                                self.navigationController?.dismiss(animated: false, completion: nil)
                            }else if let errormsg = data?.data?.message{
                                self.showAlert(title: "Alert!", msg: errormsg)
                            }else{
                                self.showAlert(title: "Alert!", msg: "User already exists with this email address or contact no")
                            }
                        }
                    }
                    else {
                        //FIXME: REMOVE BELOW COEE
                        DispatchQueue.main.async {
                            //                    Utilities.pushToVC(identifier: .Tab_VC, storyBoard: .Dash, navigation: self.navigationController!)
                        }
                    
                        
                    }
                })
                
//                self.performSegue(withIdentifier: "captcha", sender: nil)
                
            }else{
                self.showAlert(title: "Alert!", msg: "Please choose your favourite store!")
                zipcode.becomeFirstResponder()
            }
        }
    }
}
extension RegisterViewController: UITextFieldDelegate, UITextViewDelegate{
    
    @IBAction func showTermsAndCondition(sender: UIButton){
        
        let story = UIStoryboard(name: "Main", bundle: Bundle.main)
        let privacy = story.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
        if sender.tag == 0{
            privacy.titleStr = "Terms & Conditions"
            privacy.url = "https://elsupermarkets.com/mobiles/cms/terms/\(AppDelegate.shared.language)"
        }else{
            privacy.titleStr = "Privacy Policy"
            privacy.url = "https://elsupermarkets.com/mobiles/cms/privacy/\(AppDelegate.shared.language)"
            
        }
        self.present(privacy, animated: true, completion: nil)
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool{
        let story = UIStoryboard(name: "Main", bundle: Bundle.main)
        let privacy = story.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
        if URL.absoluteString == "https://elsupermarkets.com/mobiles/cms/terms/\(AppDelegate.shared.language)"{
            privacy.titleStr = BundlePath?.localizedString(forKey: "Terms & Conditions", value: nil, table: nil)
            privacy.url = "https://elsupermarkets.com/mobiles/cms/terms/\(AppDelegate.shared.language)"
        }else{
            privacy.titleStr = BundlePath?.localizedString(forKey: "Privacy Policy", value: nil, table: nil)
            privacy.url = "https://elsupermarkets.com/mobiles/cms/privacy/\(AppDelegate.shared.language)"
            
        }
        self.present(privacy, animated: true, completion: nil)
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField==email_adress && textField.text != "" && !self.isValidEmail(testStr: textField.text!){
            self.showAlert(title: "Alert!", msg: "Email is invalid. Please provide a valid email address.")
            textField.becomeFirstResponder()
        }
        if textField==phone_number && textField.text?.count != 13{
            self.showAlert(title: "Check Phone Number", msg: "Phone Number Must be 10 digits.")
            phone_number.becomeFirstResponder()
        }
        if textField==password{
            if password.text! == "" {
                self.showAlert(title: "Alert!", msg: "Please enter a password")
                password.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
                
            }
            if validatePassword(text: password.text!) != "Password must contain "{
                self.showAlert(title: "Alert!", msg: validatePassword(text: password.text!))
                password.perform(#selector(becomeFirstResponder), with: nil, afterDelay: 0.1)
            }
        }
        if textField==confirm_password{
        if confirm_password.text! == "" || confirm_password.text!.count < 8 {
            self.showAlert(title: "Passwords Don't Match!", msg: "Passwords Don't Match!")
        }
        if password.text! != confirm_password.text!{
            self.showAlert(title: "Alert!", msg: "Passwords Don't Match!")
            confirm_password.text = ""
            confirm_password.becomeFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == password{
            if let text = textField.text as NSString? {
            let password = text.replacingCharacters(in: range, with: string)
            textField.superview?.viewWithTag(5500)?.removeFromSuperview()
            let vw = UIView(frame: CGRect(x: Int(textField.frame.minX), y: Int(textField.frame.minY) - 100, width: Int(textField.frame.width), height: 100))
            vw.tag = 5500
            vw.backgroundColor = .black
            textField.superview?.addSubview(vw)
            let msg = validatePassword(text: password)
            let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: vw.frame.width, height: vw.frame.height))
            lbl.textColor = .white
            lbl.text = msg
            lbl.numberOfLines = 0
            lbl.textAlignment = .center
            vw.addSubview(lbl)
            if msg == "Password must contain "{
                textField.superview?.viewWithTag(5500)?.removeFromSuperview()
            }
            }
        }
        if textField==first_name || textField==last_name{
            let characterSet = CharacterSet.letters
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            return true
        }
        if textField==phone_number{
            if let text = textField.text as NSString? {
                let phone = text.replacingCharacters(in: range, with: string)
                textField.text =   self.formattedNumber(number: phone.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: ""))
                if textField.text?.count == 13 {
                    textField.resignFirstResponder()
                    self.getDateOfBirth()
                    return true
                }
            }
            return false
        }
        if textField.tag==10000{
            if let text = textField.text as NSString? {
                let pin = text.replacingCharacters(in: range, with: string)
                if pin.count == 5 {
                    self.activePin = pin
//                    self.showLoader(text: (BundlePath?.localizedString(forKey: "Validating ZIP..", value: nil, table: nil))!)
//                        API.shared.validate(pin: pin) { (result) in
//                            self.hideLoader()
//                            let dict = NSDictionary(dictionary: result!)
//                            let app = AppDelegate.shared
//                            let dictEncData = dict["encData"]! as! [String: Any]
//                            print(dictEncData["latitude"]!)
//                            print(dictEncData["longitude"]!)
////                            app.lat = dictEncData["latitude"]! as! String
////                            app.lng = dictEncData["longitude"]! as! String
//                            if dictEncData["message"]! as! String != "Zip code exist."{
//                                self.dismiss(animated: true, completion: nil)
//                                self.showAlert(title: "Alert!", msg: (self.BundlePath?.localizedString(forKey: "Zip code does not exist!", value: nil, table: nil))!)
//
//                                return
//                            }
//                            self.searchingStore = true
//                            self.getStoreLIst(lat: dictEncData["latitude"]! as! String, lng: dictEncData["longitude"]! as! String)
//                            self.dismiss(animated: true, completion: nil)
//                            self.storePicker.isHidden = false
//                            if self.arrStore.count > 0{
//                                self.zipcode.text = self.arrStore[0].name ?? ""
//                                let app = (UIApplication.shared.delegate as! AppDelegate)
//                                app.selectedMallID = self.arrStore[0].id!
//                                app.selectedMallName = self.arrStore[0].name!
//                                UserDefaults.standard.set(self.arrStore[0].name!, forKey: "MallName")
//                                UserDefaults.standard.set("\(self.arrStore[0].id!)", forKey: "MallId")
//                            }
//                        }
                }
            }
        }
           return true
        }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
//        switch textField {
//        case self.first_name:
//            last_name.becomeFirstResponder()
//        case self.last_name:
//            email_adress.becomeFirstResponder()
//        case self.email_adress:
//            phone_number.becomeFirstResponder()
//        case self.password:
//            confirm_password.becomeFirstResponder()
//        default:
//            zipcode.becomeFirstResponder()
//        }
        return true
    }
    
}

extension RegisterViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "D"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        zipcode.text = arrStore[row].name
//        pickerView.isHidden = true
//        let app = (UIApplication.shared.delegate as! AppDelegate)
//        app.selectedMallID = arrStore[row].id!
//        app.selectedMallName = arrStore[row].name!
//        UserDefaults.standard.set(arrStore[row].name!, forKey: "MallName")
//        UserDefaults.standard.set("\(arrStore[row].id!)", forKey: "MallId")
//        if searchingStore == true {
//        selectStoreUsingZip_Stack.isHidden = false
//        store_Height.constant = 32
//        lblSelectStoreUsingZip.text = "Select store using zip code \(activePin)"
//        }else{
//            selectStoreUsingZip_Stack.isHidden = true
//            store_Height.constant = 8
//        }
    }
    func getStoreLIst(lat:String,lng: String){
        /*
        self.showLoader(text: (BundlePath?.localizedString(forKey: "Loading..", value: nil, table: nil))!)
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateString = df.string(from: Date())
        df.dateFormat = "HH:mm:ss"
        let timeString = df.string(from: Date())
        let app = AppDelegate.shared
//        app.lat = "33.900731"
//        app.lng = "-118.145274"
        let param = ["latitude":"\(lat)", "longitude":"\(lng)", "radius":"50","limit":"100","cur_datetime":"\(dateString) \(timeString)", "zip": activePin,"findbyzip":"1"] as [String : Any]
        
        API.shared.getDataFor(url: API.shared.getmaillinks , params: param, method: .post) { (result) in
            print(result ?? "no result")
            self.hideLoader()
            let arr = result!.storeList!
            if arr.count == 0{
                if self.firstTimeCount != 0{
                let msg = "There is no El Super store available near you. Please enter the zip code to find a store."
                self.showAlert(title: "Alert!", msg: (self.BundlePath?.localizedString(forKey: msg, value: nil, table: nil))!)
                    return
                    
                }else{
                    self.firstTimeCount+=1
                }
            }
            var count = 0
            if arr.count > 0{
            self.arrStore.removeAll()
            for store in arr{
                if count <= 2{
                self.arrStore.append(store)
                }
                count += 1
            }
                if arr.count > 0{
                    let app = AppDelegate.shared
                    self.zipcode.text = arr[0].name ?? ""
                    app.selectedMallID = arr[0].id!
                    app.selectedMallName = arr[0].name!
                    UserDefaults.standard.set(arr[0].name!, forKey: "MallName")
                    UserDefaults.standard.set("\(arr[0].id!)", forKey: "MallId")
                }
            }
            self.storePicker.reloadAllComponents()
        }
        */
    }
    
    
    @IBAction func getOtherStore(){
        let alert = UIAlertController(title: "Please Enter a zip code to search a store", message: "", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Serch", style: .default) { _ in
            
        }
        let action2 = UIAlertAction(title: "Cancel", style: .destructive) { _ in
            
        }
        alert.addTextField { searchTextField in
            searchTextField.placeholder = "zip code"
            searchTextField.tag = 10000
            searchTextField.delegate = self
            searchTextField.keyboardType  = UIKeyboardType.phonePad
        }
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func elsuperbucksCheck(sender: UIButton){
        elsuperBucksChecked = !elsuperBucksChecked
        if elsuperBucksChecked == false{
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        }
    }
    
    @IBAction func closeSelectStoreUsingZip(){
        store_Height.constant = 8
        selectStoreUsingZip_Stack.isHidden = true
        zipcode.text = ""
        searchingStore = false
      //  self.arrStore.removeAll()
        getStoreLIst(lat: app.lat, lng: app.lng)
    }
}



