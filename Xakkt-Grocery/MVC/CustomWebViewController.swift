//
//  CustomWebViewController.swift
//  ElSuperNew
//
//  Created by SABYASACHI POLLEY on 08/03/18.
//  Copyright © 2018 Sabyasachi Polley. All rights reserved.
//

import UIKit

class CustomWebViewController: UIViewController {
   
    //variables and outlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var webview: UIWebView!
    var url: String!
    var titleStr: String!
    @IBOutlet weak var topHeoght: NSLayoutConstraint!

   
    //viewdidload load the webview
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = titleStr
        webview.loadRequest(URLRequest(url: URL(string: url)!))
        if UIScreen.main.nativeBounds.height  == 2436{
            self.topHeoght.constant = 84
        }
    }
    
    //Dismiss the webview
    @IBAction func cancel(){
        self.dismiss(animated: true, completion: nil)
    }
}
