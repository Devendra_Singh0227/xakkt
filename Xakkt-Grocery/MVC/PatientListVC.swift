//
//  viewcontroller2ViewController.swift
//  ChoicePopup
//
//  Created by Ankur Nautiyal on 07/04/20.
//  Copyright © 2020 Ralf Ebert. All rights reserved.
//

import UIKit

protocol popDidSelectDelegate {
    func addProductInTheList(_ selectedList: ListData?, selectedIndex: Int)
    func deleteShoppingList(index:Int)
}

extension popDidSelectDelegate{
    func deleteShoppingList(index:Int){
        
    }
}


class PatientListVC: UIViewController,Storyboardable {
    
    var shoppingObj : [ListData]? = nil
    var selectedIndex = 0
    var delegate : popDidSelectDelegate? = nil

    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(selectedIndex)
        self.tblView.register(UINib(nibName: "ListPopUpCell", bundle: Bundle.main), forCellReuseIdentifier: "ListPopUpCell")

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func removeListName(_ sender : UIButton) {
        let url = API.removeShoppingList + "\(self.shoppingObj?[sender.tag].id ?? "")"
        let param:[String : Any] = ["_store":GlobalObject.selectedStore!.id!]
        Webservices.instance.deleteMethod(url, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
            if data?.status?.integerValue == 1 {
                DispatchQueue.main.async {
                  self.delegate?.deleteShoppingList(index: sender.tag)
                  self.shoppingObj?.remove(at: sender.tag)
                  self.tblView.reloadData()
                }
            }
        })
    }
}

extension PatientListVC: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.shoppingObj?.count ?? 0)

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListPopUpCell") as! ListPopUpCell
        
        cell.removeBtn.addTarget(self, action: #selector(self.removeListName(_:)), for: .touchUpInside)
        cell.removeBtn.tag = indexPath.row
        cell.lblFmailyName.text = self.shoppingObj?[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //selectedIndex = indexPath.item
        self.delegate?.addProductInTheList(self.shoppingObj?[indexPath.row], selectedIndex: self.selectedIndex)
        self.dismiss(animated: true)
        self.tblView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34.0
    }
    
    
}
