//
//  Reviews_ViewController.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 04/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class ReviewsCell: UITableViewCell {
    
    static let identifier = "ReviewsCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


class Reviews_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var reviewsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func Profile_Btn(_ sender: UIButton) {
           let VC = storyboard?.instantiateViewController(identifier: "PersonalDetail_VC") as? PersonalDetail_VC
           navigationController?.pushViewController(VC!, animated: true)
       }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 4
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = reviewsTableView.dequeueReusableCell(withIdentifier: "ReviewsCell", for: indexPath) as? ReviewsCell
          return cell!
      }
          
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 80
      }
}
