//
//  FilterVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 11/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class Filter_Cell: UITableViewCell {
    
    @IBOutlet weak var filter_Lbl: UILabel!
    static let identifier = "Filter_Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class FilterVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var filter_TableView: UITableView!
    var filterArray = ["All Orders", "Open Orders", "Return Refund Orders", "Cancelled Refund Orders", "Return Orders", "Cancelled Orders", "Alteration Orders"]
    var index = -1
    var filterCallback: ((String) -> ())?
    var filter = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func close_Btn(_ sender: UIButton) {
        filterCallback?(filter)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = filter_TableView.dequeueReusableCell(withIdentifier: "Filter_Cell", for: indexPath) as? Filter_Cell
        cell?.filter_Lbl.text = filterArray[indexPath.row]
        if indexPath.row == index {
            cell?.filter_Lbl.textColor = UIColor.red.withAlphaComponent(0.8)
        } else {
            cell?.filter_Lbl.textColor = UIColor.darkGray
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        filter = filterArray[indexPath.row]
        filter_TableView.reloadData()
    }
    
}
