//
//  SettingVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 12/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class SettingVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var setting_TableView: UITableView!
    var settingArray = ["Settings", "Change Password", "Settings3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func menu_Btn(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func save_Btn(_ sender: DesignableButton) {
        save_Setting()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = setting_TableView.dequeueReusableCell(withIdentifier: "Filter_Cell", for: indexPath) as? Filter_Cell
        cell?.filter_Lbl.text = settingArray[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let VC = storyboard?.instantiateViewController(identifier: "UpdatePwdVC") as? UpdatePwdVC
            navigationController?.pushViewController(VC!, animated: true)
        }
    }
    
    func save_Setting() {
        let param:[String : Any] = ["key": "", "images": ""]
        
        Webservices.instance.postMethod(API.add_Setting, param: param, header: false, completion: {(error :String?, data: Categories?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if let array = data?.data, array.count > 0 {
//                self.coupon_Array = data?.data
//                    DispatchQueue.main.async {
//                     self.coupon_TableView.reloadData()
//                    }
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}
