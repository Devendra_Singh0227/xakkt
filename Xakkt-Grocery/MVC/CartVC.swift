//
//  CardVC.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 28/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class CartVC: UIViewController, Storyboardable {
    
    var cartdata:Cart?{
        didSet {
            DispatchQueue.main.async {
            if let cart = self.cartdata, let cartProduct = cart.data?.cart {
                    let ids = cartProduct.map({ (cartData: CartElement) -> Int in
                        (cartData.product?.in_cart?.integerValue ?? 0)
                        }).reduce(0,+)
                    
                    self.productCount.text = "Cart" + "(\(ids))"
            }else{
                self.productCount.text = "Cart(0)"
            }
            self.dataTable.reloadData()
            }
        }
    }
    
    @IBOutlet weak var noCartProductView:UIView!
    @IBOutlet weak var productCount:UILabel!
    @IBOutlet weak var dataTable:UITableView!
    @IBOutlet weak var nocart_Lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataTable.dataSource = self
        dataTable.delegate = self
        setNavigationBarItem()
        
        self.setupview()
        self.getCartDetail()
    }
    
    
    private func setupview(){
        self.dataTable.isHidden = true
        self.noCartProductView.isHidden = true
    }

    func getCartDetail() {
        Webservices.instance.getMethod(API.getCartDetail + "/" + (GlobalObject.selectedStore?.id ?? "")) { (data : Cart?, error) in
            DispatchQueue.main.async {
                if data?.status.integerValue == 1 {
                    self.noCartProductView.isHidden = true
                    self.dataTable.isHidden = false
                    self.cartdata = data
                }else{
                    self.noCartProductView.isHidden = false
                    self.nocart_Lbl.text  = "Cart is empty."
                }
            }
        }
    }
    
    @IBAction func changeAddress(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "CustomerAddressVC") as? CustomerAddressVC
        self.navigationController?.pushViewController(dashboardVC!, animated: true)
    }
    @IBAction func didSelectStartShopping(_ sender: UIButton) {
        SceneDelegate.sharedInstance().createMenuView()
    }
    
    @IBAction func menuButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cardButtonAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func apply_Coupon_Btn(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "ApplyCoupon_VC") as? ApplyCoupon_VC
        navigationController?.pushViewController(VC!, animated: true)
    }
    
    @objc func removeProductFromCart(sender: UIButton){
        if let cart =  self.cartdata?.data?.cart?[sender.tag], let product = cart.product{
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!]
            
            Webservices.instance.deleteMethod(API.removeProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
                if data?.status.integerValue == 1 {
                    self.cartdata = data
                }else{
                    DispatchQueue.main.async {
                    self.dataTable.isHidden = true
                    self.noCartProductView.isHidden = false
                    self.cartdata = nil
                    }
                }
            })
        }
    }
}

extension CartVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cart = self.cartdata, let cartProduct = cart.data?.cart, cartProduct.count > 0 {
            return cartProduct.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FavCell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavCell
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(self.removeProductFromCart), for: .touchUpInside)

        cell.stepper.tag = indexPath.row
        cell.stepper.delegate = self
        if let cart =  self.cartdata?.data?.cart?[indexPath.row], let product = cart.product{
            cell.configureDataForCart(product:product)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = GlobalView.totalInstanceFromNib()

        view.deliveryStackView.isHidden = true
        view.couponStackView.isHidden = true
        if let cart =  self.cartdata?.subtotal{
            if let shipping_cost =  cart.shippingCost?.stringValue{
                view.deliveryFeelbl.text = shipping_cost
            }
            view.checkOut_Btn.addTarget(self, action: #selector(checkOut), for: .touchUpInside)
            view.apply_Coupon.addTarget(self, action: #selector(apply_Coupon), for: .touchUpInside)
            view.subTotalLbl.text =  "\(GlobalObject.selectedStore?.currency?.name ?? "$")" + (cart.subTotal?.stringValue!)!
            view.esitmatedLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$")" + (cart.price?.stringValue)!
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 200  
    }
    
    
    
    
    @objc func checkOut() {
        let storyboard = UIStoryboard(name: "Checkout", bundle: nil)
        let VC = storyboard.instantiateViewController(identifier: "CheckouVC") as? CheckouVC
        navigationController?.pushViewController(VC!, animated: true)
        
//        guard let address_id = self.addressId else {
//            return
//        }
//
//        guard let cart = self.cartdata, let cartProduct = cart.data?.cart, cartProduct.count > 0 else {
//            return
//        }
//        var productArray = [[String : Any]]()
//        for cart in cartProduct {
//            if let productId = cart.product?.id, let productQnty = cart.product?.in_cart?.integerValue {
//                let productDict = ["_product": productId, "quantity": productQnty] as [String : Any]
//                productArray.append(productDict)
//            }
//        }
//        guard let total = self.cartdata?.subtotal?.subTotal else {
//            return
//        }
//        let param:[String : Any] = ["_store":GlobalObject.selectedStore!.id!, "products": productArray,
//                                    "address": address_id,"delivery_notes": "","payment_method": 0,"total_cost":total]
//        Webservices.instance.postMethod(API.createOrder, param: param, header: true, completion: {(error :String?, data: CreateOrder?) -> Void in
//            DispatchQueue.main.async {
//                if data?.status?.integerValue == 1 {
//                    let storyboard = UIStoryboard(name: "Order", bundle: nil)
//                    let vc = storyboard.instantiateViewController(identifier: "OrderSuccessVC") as! OrderSuccessVC
//                    vc.order_id = data?.data?.order_id
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//            }
//        })
    }
    
    @objc func apply_Coupon() {
        let VC = storyboard?.instantiateViewController(identifier: "ApplyCoupon_VC") as? ApplyCoupon_VC
       // VC?.cart_Id = self.cartdata?.data?.cart[0].id
        navigationController?.pushViewController(VC!, animated: true)
    }
}

extension CartVC:StepperViewDelegate{
    func setValue(value: Int, tag: Int, isIncrease: Bool) {
        let indexPath = IndexPath(row: tag, section: 0)
        let cell = self.dataTable.cellForRow(at: indexPath) as! FavCell
        
        guard let cart =  self.cartdata?.data?.cart?[tag], let product = cart.product else {
            return
        }
        let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"quantity":cell.stepper.value]
        Webservices.instance.putMethod(API.updateProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
            if data?.status.integerValue == 1 {
                self.cartdata = data
            }
        })
    }
}


