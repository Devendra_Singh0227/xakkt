//
//  ForgotPasswordVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 14/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var email_TxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirm_Btn(_ sender: UIButton) {
        Forgot_Pwd()
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
   
    func Forgot_Pwd() {
        let param = ["email": email_TxtField.text!] as [String : Any]

        Webservices.instance.postMethod(API.forgotPwd, param: param, header: false, completion: {(error :String?, data: ForgotPwd?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    self.Aler_WithAction(message: data?.data ?? "", completion: {_ in
                        if true {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                   
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }

}
