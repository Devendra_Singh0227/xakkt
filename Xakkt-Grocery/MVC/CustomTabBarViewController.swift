//
//  CustomTabBarViewController.swift
//  TabbarWithSideMenu
//
//  Created by Sunil Prajapati on 20/04/18.
//  Copyright © 2018 sunil.prajapati. All rights reserved.
//

import UIKit


class CustomTabBarViewController: UITabBarController,UITabBarControllerDelegate,Storyboardable {
    var previousSelectedTabIndex:Int = 0
    var id = ""
    //MARK:- UIViewController Initialize Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.title = LeftMenuItems.tabView.rawValue
         setupMiddleButton()
        
//        setNavigationBarItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        self.previousSelectedTabIndex = tabBarController.selectedIndex
    }
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item:
        UITabBarItem) {
        let vc = self.viewControllers![previousSelectedTabIndex] as! UINavigationController
        vc.popToRootViewController(animated: false)
    }
    
    // TabBarButton – Setup Middle Button
    func setupMiddleButton() {
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        var menuButtonFrame = menuButton.frame

        if UIDevice.current.screenType == .iPhone4_4S || UIDevice.current.screenType == .iPhones_5_5s_5c_SE || UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus || UIDevice.current.screenType == .iPhones_6_6s_7_8{
            menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - 25
        }
        else {
            menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - 30
        }
        
        menuButtonFrame.origin.x = self.view.bounds.width / 2 - menuButtonFrame.size.width / 2
        menuButton.frame = menuButtonFrame
        self.view.addSubview(menuButton)

        menuButton.setImage(UIImage(named: "X-midd"), for: UIControl.State.normal)
        menuButton.addTarget(self, action: #selector(self.menuButtonAction), for: UIControl.Event.touchUpInside)

        self.view.layoutIfNeeded()
    }
    
    // Menu Button Touch Action
   @objc func menuButtonAction(sender: UIButton) {

        self.selectedIndex = 2
        // console print to verify the button works
        print("Middle Button was just pressed!")
       }
}


