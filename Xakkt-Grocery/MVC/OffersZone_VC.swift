//
//  OffersZone_VC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 30/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import Alamofire

class Offers_Cell: UITableViewCell {
   
    @IBOutlet weak var offer_Image: UIImageView!
    @IBOutlet weak var originalPrice_Lbl: UILabel!
    @IBOutlet weak var offerPrice_Lbl: UILabel!
    @IBOutlet weak var name_Lbl: UILabel!
    @IBOutlet weak var quantity_Lbl: DesignableLabel!
    @IBOutlet weak var color_View: DesignableView!
    @IBOutlet weak var offer_percent_Lbl: DesignableLabel!
    
    @IBOutlet weak var addToCart_Btn: DesignableButton!
    static let identifier = "Offers_Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


class OffersZone_VC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var offers_TableView: UITableView!
    var products = [List]()
    @IBOutlet weak var search_TxtField: UITextField!
    @IBOutlet weak var no_ProductView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        search_TxtField.delegate = self
        get_Product_List()
        
        // Do any additional setup after loading the view.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            get_Product_List()
        } else {
            search_Product_List()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func Profile_Btn(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(identifier: "PersonalDetail_VC") as? PersonalDetail_VC
        navigationController?.pushViewController(VC!, animated: true)
    }
    
    @IBAction func sideMenu_Btn(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = offers_TableView.dequeueReusableCell(withIdentifier: "Offers_Cell", for: indexPath) as? Offers_Cell
        cell?.originalPrice_Lbl.attributedText = addline(text: "Rs 800")
        
        cell?.offer_Image?.sd_setImage(with: URL(string: "\(products[indexPath.row].image ?? "")"), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        cell?.name_Lbl.text = products[indexPath.row].name?.english
        cell?.offerPrice_Lbl.text = "\(products[indexPath.row].deal_price ?? 0.0)"
        cell?.originalPrice_Lbl.text = "\(products[indexPath.row].regular_price ?? 0)"
        let percent = Int(products[indexPath.row].deal_percentage)
        cell?.offer_percent_Lbl.text = "\(percent)%"
        cell?.addToCart_Btn.tag = indexPath.row
        cell?.addToCart_Btn.addTarget(self, action: #selector(add_to_cart(sender:)), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let VC = storyboard?.instantiateViewController(identifier: "OffersDetail_VC") as? OffersDetail_VC
        VC?.product_id = products[indexPath.row].id
        navigationController?.pushViewController(VC!, animated: true)
    }
    
    func get_Product_List() {
        Webservices.instance.getMethod("\(API.list_product)/\(GlobalObject.selectedStore?.id ?? "")") { (data : productList?, error) in
                    //print(data)
            if data?.status == "success", let array = data?.data, array.count > 0 {
                self.products = array
                //print(self.products)
                DispatchQueue.main.async {
                    self.no_ProductView.isHidden = true
                    self.offers_TableView.reloadData()
                }
            }else if data?.status == "success", let array = data?.data, array.count == 0 {
                 self.no_ProductView.isHidden = false
            }
        }
    }
    
    func search_Product_List() {
        let param:[String : Any] = ["search": search_TxtField.text!, "page": 1, "limit": 5]
        Webservices.instance.postMethod(API.search_product, param: param, header: true, completion: {(error :String?, data: productList?) -> Void in
            DispatchQueue.main.async {
                self.products.removeAll()
                if data?.status == "success", let array = data?.data, array.count > 0 {
                    self.products = array
                    //print(self.products)
                    self.offers_TableView.reloadData()
                    self.no_ProductView.isHidden = true
                } else {
                    if (data?.data!.count)! <= 0 {
                        self.offers_TableView.reloadData()
                        self.no_ProductView.isHidden = false
                    }
                }
            }
        })
    }
    
    @objc func add_to_cart(sender: UIButton) {
        
        if let product_id = self.products[sender.tag].id {
            let param:[String : Any] = ["_product": product_id, "_store":GlobalObject.selectedStore!.id!, "quantity": 1]
            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
//                if data?.status == "success" {
//
//                }
            })
        }
    }
}


