//
//  ProductListBYBanner.swift
//  Xakkt-Grocery
//
//  Created by Developer on 22/11/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit


class BannerProductTableCell: UITableViewCell {
    
    //weak var updateProductDelegate:CarouselCollectionViewCellDelegate?
    
    @IBOutlet weak var stepper: StepperView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productWgtLbl: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var productPrice: PriceLabel!
    @IBOutlet weak var productDealPrice: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}



class ProductListBYBanner: UIViewController {

    @IBOutlet weak var DealTitleLbl:UILabel!
    @IBOutlet weak var productListTable:UITableView!
    
    var dealId:String?
    var dealTitle:String = ""
    var shoppingProductList : BannerProduct? {
        didSet {
            DispatchQueue.main.async {
                self.productListTable.reloadData()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.DealTitleLbl.text = dealTitle + " @Deal"
        if let  deal_Id = dealId{
            self.getAllProductList(deal_Id: deal_Id)
        }
        // Do any additional setup after loading the view.
    }
    
    private func getAllProductList(deal_Id:String) {
        
        let param = ["_store": GlobalObject.selectedStore?.id ?? "", "_deal":deal_Id] as [String : Any]
        Webservices.instance.genericPost(API.bannerProduct, param: param) { (data: BannerProduct?, error :String?) in
            if let success = data?.status?.integerValue, success == 1 {
                self.shoppingProductList = data
            }else{
                DispatchQueue.main.async {
                     let noProductLbl = self.view.viewWithTag(100)
                    noProductLbl?.isHidden = false
                }               
            }
        }
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func openCartAction(_ sender: Any) {
        if Utilities.checkUserLogin() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
}



extension ProductListBYBanner:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.shoppingProductList?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:BannerProductTableCell = tableView.dequeueReusableCell(withIdentifier: "BannerProductTableCell", for: indexPath) as! BannerProductTableCell
        cell.selectionStyle = .none
        guard let product =  self.shoppingProductList?.data?[indexPath.row].product else {
            return cell
        }
        if let weight =  product.weight?.stringValue, let unit = product.unit{
            cell.productWgtLbl.text = weight + unit
        }
        
        if let regularPrice = product.price?.doubleValue, let dealPrice = product.deal_price?.doubleValue, (regularPrice <= dealPrice ||  dealPrice == 0.0) {
            cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            cell.productDealPrice.isHidden = true
            cell.productPrice.textColor = .black
            cell.productPrice.clearStrikeOnLabel()
        }else{
            cell.productPrice.textColor = .lightGray
            cell.productDealPrice.isHidden = false
            cell.productDealPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.deal_price?.stringValue ?? "0.00")"
            cell.productPrice.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") \(product.price?.stringValue ?? "0.00")"
            cell.productPrice.strikeOnLabel()
        }
        
        
        if let in_shoppinglist = product.in_shoppinglist, in_shoppinglist == 1{
            cell.listButton.isSelected = true
        }else
        {
            cell.listButton.isSelected = false
        }
        if let is_fav = product.is_favourite, is_fav == 1{
            cell.likeButton.isSelected = true
        }else{
            cell.likeButton.isSelected = false
        }
    
        cell.stepper.tag = indexPath.item
        cell.stepper.delegate = self
    
        if let in_cart = product.in_cart?.integerValue, in_cart > 0{
            cell.stepper.value = in_cart
            cell.cartButton.isSelected = true
        }else{
            cell.stepper.value = 1
            cell.cartButton.isSelected = false
        }
        if let image_url = product.image{
            cell.productImage?.sd_setImage(with: URL(string: image_url), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        }
        
        cell.productName.text = product.name?.english ?? ""
            
        cell.likeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(self.addFavAction), for: .touchUpInside)
    
        cell.cartButton.tag = indexPath.row
        cell.cartButton.addTarget(self, action: #selector(self.addProductInCart), for: .touchUpInside)
        
        cell.listButton.tag = indexPath.row
        cell.listButton.addTarget(self, action: #selector(self.ShoppingListButton(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let product =  self.shoppingProductList?.data?[indexPath.row].product else {
            return
        }
        let storyboard = UIStoryboard(name: "Product", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProductDetailTableViewController") as! ProductDetailTableViewController
        vc.product = product
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    @objc func favButtonClick(_ sender: UIButton) {
        
    }

    
    @objc func minusButtonIcon(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].id  , let quantity = self.shoppingProductList?.data?[sender.tag].quantity {
            self.updateQuatitiy(id, Quatitiy: quantity - 1)
        }
    }
    
    @objc func plusButtonIcon(_ sender: UIButton) {
        if let id = self.shoppingProductList?.data?[sender.tag].id, let quantity = self.shoppingProductList?.data?[sender.tag].quantity {
            self.updateQuatitiy(id, Quatitiy: quantity + 1)
        }
    }
    
    func updateQuatitiy(_ id : String, Quatitiy: Int )  {
        let param = ["_shoppinglist": id ,"quantity": Quatitiy ] as [String : Any]
        
        Webservices.instance.patchMethod(API.quantity, param: param) { (data : RemoveList? , error) in
           
        }
    }
    
    @objc func addProductInCart(sender: UIButton){
        if Utilities.checkUserLogin() {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = productListTable.cellForRow(at: indexPath) as! BannerProductTableCell
            
            sender.isSelected = true
            guard let product =  self.shoppingProductList?.data?[sender.tag].product else {
                return
            }
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!, "quantity":cell.stepper.value]

            Webservices.instance.postMethod(API.addProductInCart, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                if data?.status?.integerValue == 1 {
                    
                }
            })
        }else{
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
    @objc func addFavAction(sender: UIButton){
        if Utilities.checkUserLogin() {
            guard let product =  self.shoppingProductList?.data?[sender.tag].product else {
                return
            }
            //let product = self.productArray![sender.tag]
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"wish_price":1.0, "max_price":2.0]

                Webservices.instance.postMethod(API.addWishList, param: param, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        var dict = product
                        dict.is_favourite = 1
                        sender.isSelected = true
                    }
                })
            }else{
                Webservices.instance.deleteMethod(API.addWishList, param: nil, header: true, completion: {(error :String?, data: ProductAddList?) -> Void in
                    if data?.status?.integerValue == 1 {
                        var dict = product
                        dict.is_favourite = 1
                    }
                })
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
        
        
        
    }
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down, .up]
        self.navigationController?.viewControllers.first?.present(controller, animated: true)
    }
    @objc func ShoppingListButton(_ sender: UIButton) {
        if Utilities.checkUserLogin() {
            if let array = AppDelegate.shared.listArray, array.count > 0 {
                let controller =  PatientListVC.storyboardViewController()
                controller.shoppingObj = array
                controller.delegate = self
                controller.selectedIndex = sender.tag
                print(sender.tag)
                if array.count <= 6 {
                    controller.preferredContentSize = CGSize(width: 160, height: 35*(array.count ) + 40)
                }
                else {
                    controller.preferredContentSize = CGSize(width: 160, height: 250)
                }
                showPopup(controller, sourceView: sender)
            }else{
                self.navigationController?.tabBarController?.selectedIndex = 1
            }
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            loginVC.delegate = self
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    
}



extension ProductListBYBanner:LoginScreenDelegate{
    func isSucusssFull(isLogin: Bool) {
        
    }
}

extension ProductListBYBanner:popDidSelectDelegate{
    func addProductInTheList(_ selectedList: ListData?, selectedIndex: Int) {
        guard let list_id = selectedList?.id else {
            return
        }
        
        
        guard let _product = self.shoppingProductList?.data?[selectedIndex].product?.id else {
            return
        }
        
        let indexPath = IndexPath(row: selectedIndex, section: 0)
        let cell = productListTable.cellForRow(at: indexPath) as! BannerProductTableCell
        cell.listButton.isSelected = true
        
        let param = ["_shoppinglist": list_id ,"_product": _product, "quantity":1 ] as [String : Any]
        Webservices.instance.postMethod(API.addProduct, param: param, header: false, completion: {(error :String?, data: ProductAddList?) -> Void in
            if data?.status?.integerValue == 1 {
               //self.listArray = data?.data ?? []
            }
        })
      }
}


extension ProductListBYBanner:StepperViewDelegate{
    func setValue(value: Int, tag: Int, isIncrease: Bool) {
        guard let product = self.shoppingProductList?.data?[tag].product else {
            return
        }
        let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"quantity":value]

        Webservices.instance.putMethod(API.updateProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
            if data?.status.integerValue == 1 {

            }
        })
    }
    
    
}
