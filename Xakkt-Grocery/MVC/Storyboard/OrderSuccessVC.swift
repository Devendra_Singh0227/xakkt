//
//  OrderSuccessVC.swift
//  Xakkt-Grocery
//
//  Created by Developer on 14/09/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class OrderSuccessVC: UIViewController {

    var order_id:String?
    @IBOutlet weak var successImg:UIImageView!
    @IBOutlet weak var orderIdLbl:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.orderIdLbl.text = order_id
        // Do any additional setup after loading the view.
    }
    
    @IBAction func continueShopping(_ sender: Any) {
        SceneDelegate.sharedInstance().createMenuView()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
