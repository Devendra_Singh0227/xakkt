//
//  CheckouVC.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 28/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit


enum DeliveryType {
    case pickup
    case delivery
}
enum PaymentMode {
    case cod
    case payonline
}

class CheckouVC: UIViewController, Storyboardable {
    @IBOutlet weak var addressLbl:UILabel!
    var addressId:String?
    var deliveryType:DeliveryType = .pickup
    
    var paymentType:PaymentMode = .cod
    var cartdata:Cart?{
        didSet {
            DispatchQueue.main.async {
            self.dataTable.reloadData()
            }
        }
    }
    
    @IBOutlet weak var dataTable:UITableView!
    @IBOutlet weak var pickupBtn:UIButton!
    @IBOutlet weak var deliveryBtn:UIButton!
    
    @IBOutlet weak var codBtn:UIButton!
    @IBOutlet weak var payOnlineBtn:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataTable.dataSource = self
        dataTable.delegate = self
        setNavigationBarItem()

        self.pickupBtn.layer.borderWidth = 1.0
        self.pickupBtn.layer.borderColor = UIColor.black.cgColor
        
        self.deliveryBtn.layer.borderWidth = 1.0
        self.deliveryBtn.layer.borderColor = UIColor.black.cgColor
        
        if paymentType == .cod {
            self.codBtn.isSelected = true
        }else{
            self.codBtn.isSelected = false
        }
        
        self.setupview()
        self.getCartDetail()
        self.fetchCustomerAddressList()
    }
    
    
    
    func fetchCustomerAddressList(){
        Webservices.instance.getMethod(API.getdefaultAddress) { (data : CustomerAddress?, error) in
            if let address = data?.data?.address?.first  {
                DispatchQueue.main.async {
                    self.addressLbl.text = address.address1
                    self.addressId = address.addressID
                }
            }
        }
    }
    
    private func setupview(){
        self.dataTable.isHidden = true
    }

    func getCartDetail() {
        Webservices.instance.getMethod(API.getCartDetail + "/" + (GlobalObject.selectedStore?.id ?? "")) { (data : Cart?, error) in
            DispatchQueue.main.async {
                if data?.status.integerValue == 1 {
                    self.dataTable.isHidden = false
                    self.cartdata = data
                }else{
                }
            }
        }
    }
    
    @IBAction func changeAddress(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "CustomerAddressVC") as? CustomerAddressVC
        self.navigationController?.pushViewController(dashboardVC!, animated: true)
    }

    
    @IBAction func selectDeliveryType(_ sender: UIButton) {
        if sender.tag == 102 {
            self.deliveryType = .delivery
            self.pickupBtn.setTitleColor(.black, for: .normal)
            self.deliveryBtn.setTitleColor(.white, for: .normal)
            self.deliveryBtn.backgroundColor = .darkGray
            self.pickupBtn.backgroundColor = .clear
            self.codBtn.setTitle("Payment at Pickup", for: .normal)
            self.payOnlineBtn.setTitle("Credit Card", for: .normal)
        }else{
            self.deliveryType = .pickup
            self.pickupBtn.setTitleColor(.white, for: .normal)
            self.deliveryBtn.setTitleColor(.black, for: .normal)
            self.pickupBtn.backgroundColor = .darkGray
            self.deliveryBtn.backgroundColor = .clear
            self.codBtn.setTitle("Cash on Delivery", for: .normal)
            self.payOnlineBtn.setTitle("Credit Card", for: .normal)
        }
        self.dataTable.reloadData()
    }
    
    @IBAction func selectPaymentMode(_ sender: UIButton) {
        if sender.tag == 1001 {
            self.codBtn.isSelected = true
            self.payOnlineBtn.isSelected = false
            self.paymentType = .cod
        }else{
            self.codBtn.isSelected = false
            self.payOnlineBtn.isSelected = true
            self.paymentType = .payonline
        }
        self.dataTable.reloadData()
    }
    
    @IBAction func menuButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func apply_Coupon_Btn(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "ApplyCoupon_VC") as? ApplyCoupon_VC
        navigationController?.pushViewController(VC!, animated: true)
    }
    
    @objc func removeProductFromCart(sender: UIButton){
        if let cart =  self.cartdata?.data?.cart?[sender.tag], let product = cart.product{
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!]
            
            Webservices.instance.deleteMethod(API.removeProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
                if data?.status.integerValue == 1 {
                    self.cartdata = data
                }else{
                    DispatchQueue.main.async {
                    self.dataTable.isHidden = true
                    self.cartdata = nil
                    }
                }
            })
        }
    }
}

extension CheckouVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cart = self.cartdata, let cartProduct = cart.data?.cart, cartProduct.count > 0 {
            return cartProduct.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FavCell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavCell
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(self.removeProductFromCart), for: .touchUpInside)

        cell.stepper.tag = indexPath.row
        cell.stepper.delegate = self
        if let cart =  self.cartdata?.data?.cart?[indexPath.row], let product = cart.product{
            cell.configureDataForCart(product:product)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = GlobalView.totalInstanceFromNib()
        
        view.deliveryStackView.isHidden = self.deliveryType == .pickup ? true:false

        if let cart =  self.cartdata?.subtotal{
            if let coupon =  cart.coupon{
                view.apply_Coupon.isHidden = true
                view.couponStackView.isHidden = false
                view.couponCodeLbl.text = coupon.code
                view.couponCodeLbl.isHidden = false
                view.remove_CouponBtn.isHidden = false
            }else{
                view.remove_CouponBtn.isHidden = true
                view.couponCodeLbl.text = ""
                view.couponCodeLbl.isHidden = true
                view.apply_Coupon.isHidden = false
                view.couponStackView.isHidden = false
            }
            view.checkOut_Btn.setTitle("Place Order", for: .normal)
            view.checkOut_Btn.addTarget(self, action: #selector(checkOut), for: .touchUpInside)
            view.apply_Coupon.addTarget(self, action: #selector(apply_Coupon), for: .touchUpInside)
            
            
            if let shipping_cost =  cart.shippingCost?.doubleValue, let total_price =  cart.price?.doubleValue, deliveryType == .delivery{
                view.deliveryFeelbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") " + "\(shipping_cost)"
                
                view.esitmatedLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") " + "\((shipping_cost + total_price))"
            }else{
                view.esitmatedLbl.text = "\(GlobalObject.selectedStore?.currency?.name ?? "$") " + (cart.price?.stringValue)!
            }
            view.subTotalLbl.text =  "\(GlobalObject.selectedStore?.currency?.name ?? "$") " + (cart.subTotal?.stringValue)!
            
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 200  
    }
    
    
    
    
    @objc func checkOut() {
        
        guard let address_id = self.addressId else {
            return
        }
        
        guard let cart = self.cartdata, let cartProduct = cart.data?.cart, cartProduct.count > 0 else {
            return
        }
        var productArray = [[String : Any]]()
        for cart in cartProduct {
            if let productId = cart.product?.id, let productQnty = cart.product?.in_cart?.integerValue {
                let productDict = ["_product": productId, "quantity": productQnty] as [String : Any]
                productArray.append(productDict)
            }
        }
        guard let total = self.cartdata?.subtotal?.price?.doubleValue else {
            return
        }
        
        let paymentMode = self.paymentType == .payonline ? 1:0
        
        let param:[String : Any] = ["_store":GlobalObject.selectedStore!.id!, "products": productArray,
                                    "address": address_id,"delivery_notes": "","payment_method": paymentMode,"total_cost":total]
        Webservices.instance.postMethod(API.createOrder, param: param, header: true, completion: {(error :String?, data: CreateOrder?) -> Void in
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    self.makeCartEmpty()
                    let storyboard = UIStoryboard(name: "Order", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "OrderSuccessVC") as! OrderSuccessVC
                    vc.order_id = data?.data?.order_id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        })
    }
    
    
    func makeCartEmpty() -> Void {
        let api_url = API.empty_cart + "\(GlobalObject.selectedStore!.id!)"
        Webservices.instance.deleteMethod(api_url, param: nil) { (data : CustomerAddress?, error) in
            if data?.state?.integerValue == 1 {
                
            }
        }
    }
    
    @objc func apply_Coupon() {
        let VC = storyboard?.instantiateViewController(identifier: "ApplyCoupon_VC") as? ApplyCoupon_VC
       // VC?.cart_Id = self.cartdata?.data?.cart[0].id
        navigationController?.pushViewController(VC!, animated: true)
    }
}

extension CheckouVC:StepperViewDelegate{
    func setValue(value: Int, tag: Int, isIncrease: Bool) {
        let indexPath = IndexPath(row: tag, section: 0)
        let cell = self.dataTable.cellForRow(at: indexPath) as! FavCell
        
        if let cart =  self.cartdata?.data?.cart?[tag], let product = cart.product{
            let param:[String : Any] = ["_product": product.id!, "_store":GlobalObject.selectedStore!.id!,"quantity":cell.stepper.value]
            Webservices.instance.putMethod(API.updateProductFromCart, param: param, header: true, completion: {(error :String?, data: Cart?) -> Void in
                if data?.status.integerValue == 1 {
                    self.cartdata = data
                }
            })
        }
    }
}


