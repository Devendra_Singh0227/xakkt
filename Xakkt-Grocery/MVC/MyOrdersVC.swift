//
//  MyOrdersVC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 11/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit
import HCSStarRatingView

class Orders_Cell: UITableViewCell {
    
    @IBOutlet weak var store_name_Lbl: UILabel!
    @IBOutlet weak var status_Lbl: UILabel!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var order_id_lbl: UILabel!
    @IBOutlet weak var items_count_lbl: UILabel!
    static let identifier = "Orders_Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


class MyOrdersVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var order_Lbl: UILabel!
    @IBOutlet weak var orders_TableView: UITableView!
    @IBOutlet weak var noCartProductView: UIView!
    var orderData : [Order]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMyOrder()
    }
    
//    @IBAction func openMenu(_ sender: Any) {
//        slideMenuController()?.toggleLeft()
//    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filter_Btn(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(identifier: "FilterVC") as? FilterVC
        VC?.filterCallback = { (value) in
            self.order_Lbl.text = "Showing: \(value)"
        }
        self.present(VC!, animated: true, completion: nil)        
    }
    @IBAction func openCartAction(_ sender: Any) {
        if Utilities.checkUserLogin() {
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let loginVC = LoginViewController.storyboardViewController()
            let navC = UINavigationController(rootViewController: loginVC)
            self.navigationController?.present(navC, animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orders_TableView.dequeueReusableCell(withIdentifier: "Orders_Cell", for: indexPath) as? Orders_Cell
        cell?.store_name_Lbl.text = orderData?[indexPath.row].store?.name
        cell?.items_count_lbl.text = "\(orderData?[indexPath.row].products?.count ?? 0)"
        cell?.order_id_lbl.text = "\(orderData?[indexPath.row].shipping?.order_id ?? "0")"
        cell?.status_Lbl.text = orderData?[indexPath.row].shipping?.tracking?.status
        let date = orderData?[indexPath.row].createdAt
        let date2 = Utilities.formatDate(date: date!)
        cell?.date_lbl.text = date2
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func getMyOrder() {
        
        Webservices.instance.getMethod(API.myOrder) { (data: OrderList?, error) in
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    self.noCartProductView.isHidden = true
                    self.orderData = data?.data
                    self.orders_TableView.reloadData()
                }else{
                    self.noCartProductView.isHidden = false
                }
            }
        }
    }
}
