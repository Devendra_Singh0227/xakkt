//
//  PaymentViewController.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 06/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pay_Btn(_ sender: DesignableButton) {
        paymentApi()
    }
    
    func paymentApi() {
        let param:[String : Any] = ["orderid": "fseffdsdf3rfd3rdf"]
        
        Webservices.instance.postMethod(API.payment, param: param, header: false, completion: {(error :String?, data: Categories?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if let array = data?.data, array.count > 0 {
                    //                self.coupon_Array = data?.data
                    //                    DispatchQueue.main.async {
                    //                     self.coupon_TableView.reloadData()
                    //                    }
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}
