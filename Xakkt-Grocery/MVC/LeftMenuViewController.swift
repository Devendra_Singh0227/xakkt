//
//  LeftMenuViewController.swift
//  TabbarWithSideMenu
//
//  Created by Sunil Prajapati on 20/04/18.
//  Copyright © 2018 sunil.prajapati. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Alamofire
import StoreKit


class LeftMenuTableCell: UITableViewCell {
    
    
    @IBOutlet weak var callStatusHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var activeOrderBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

//HelpVC
class LeftMenuViewController: UIViewController,Storyboardable {

    var mainViewController: UIViewController!
    var activeOrder:Int = 0
    //MARK:- IBOutlet And Variable Declaration
    //var leftMenu:[String] = ["Home","Help","My Orders","My Account","User Agreement","Return Policy","Offers Zone","Categories", "Cart","Change Store","Departments", "Setting", "Logout"]
    var leftMenu:[String] = ["Home","Shop","My Account","Order Again","Orders","Shopping Lists","Favourites", "buX", "Help", "Legal","Logout"]
    @IBOutlet weak var tableLeftMenu: UITableView!
    
    //MARK:- UIViewController Initialize Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        tableLeftMenu.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getActiveOrder), name:NSNotification.Name(rawValue: "GetActiveOrderIdentifier"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.value(forKey: "iSLogin") == nil, leftMenu.contains("Logout"){
            leftMenu.removeLast()
        }
    }
    
    @objc func getActiveOrder(notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary?, let active_orders = dict.value(forKey: "active_orders") as? Int        {
            self.activeOrder = active_orders
            self.tableLeftMenu.reloadData()
        }
    }
    
    @IBAction func openEmail(_ sender: Any) {
        // Track Event
        var tackingDict: [String: String] = [:]
        tackingDict["device_type"] = "iOS"
                
        let email = "customercare@mykirana.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func openCall(_ sender: Any) {
        // Track Event
        var tackingDict: [String: String] = [:]
        tackingDict["device_type"] = "iOS"
        
        let phone = "18001003070"
        if let url = URL(string: "tel:\(phone)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK:- Other Methods
    
    func changeMainViewController(to viewController: UIViewController) {
        //Change main viewcontroller of side menu view controller
        let navigationViewController = UINavigationController(rootViewController: viewController)
        navigationViewController.navigationBar.isHidden = true
        navigationViewController.hidesBottomBarWhenPushed = false
        slideMenuController()?.changeMainViewController(navigationViewController, close: true)
    }
}

//MARK:- UITableViewDataSource Methods
extension LeftMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenu.count
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
//        if indexPath.row == 0 {
//            let tableViewCell:LeftMenuTableCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTopTableCell", for: indexPath) as! LeftMenuTableCell
//            tableViewCell.selectionStyle = .none
//
//            return tableViewCell
//        }
        let tableViewCell:LeftMenuTableCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableCell", for: indexPath) as! LeftMenuTableCell
        tableViewCell.selectionStyle = .none
        if indexPath.row == 1 {
            tableViewCell.nameLbl.text = "\(leftMenu[indexPath.row]) @ \(GlobalObject.selectedStore?.name ?? "")"
        }else{
            tableViewCell.nameLbl.text = leftMenu[indexPath.row]
        }
        return tableViewCell
    }
}

//MARK:- UITableViewDelegate Methods
extension LeftMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let leftHeaderView = loadFromNibNamed(ViewID.leftHeaderView) as? LeftHeaderView else {
            print("Left Header view not found")
            return nil
        }
        if let userDAta : Login = Utilities.getModelKeyChange(keys: keys.userData) as Login? {
            leftHeaderView.userNameLbl.text = (userDAta.data?.user?.firstName ?? "") + " " + (userDAta.data?.user?.lastName ?? "")
        }
        leftHeaderView.headerClickBtn.addTarget(self, action:#selector(pressButton), for: .touchUpInside)
        
        return leftHeaderView
    }
    //The target function
    @objc func pressButton(_ sender: UIButton){
        
    }
    
    
    private func openLoginScreen() -> Void {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nonMenuController:HomeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC else {
            return
        }
        changeMainViewController(to: nonMenuController)
        
        
        
//        if let customTabBar = ((slideMenuController()?.mainViewController as? UINavigationController)?.viewControllers.first as? CustomTabBarViewController) {
//            customTabBar.selectedIndex = 0
//            changeMainViewController(to: customTabBar)
//
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenLogin"), object: nil, userInfo: nil)
//        } else {
//            guard let customTabBar = storyboard?.instantiateViewController(withIdentifier: ViewControllerID.customTabbarViewController) as? CustomTabBarViewController else {
//                return
//            }
//            customTabBar.selectedIndex = 0
//            changeMainViewController(to: customTabBar)
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenLogin"), object: nil, userInfo: nil)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let leftMenuItem = LeftMenuItems(rawValue: leftMenu[indexPath.row]) else {
            print("Left Menu Item not found")
            return
        }
        
        switch leftMenuItem {
        case .changeStore:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:ChangeStore_VC = storyboard.instantiateViewController(withIdentifier: "ChangeStore_VC") as? ChangeStore_VC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .shop:
            let storyboard = UIStoryboard(name: "StoreProduct", bundle: nil)
            guard let nonMenuController:StoreProductListVC = storyboard.instantiateViewController(withIdentifier: "StoreProductListVC") as? StoreProductListVC else {
                return
            }
            self.pushToNonTabbarController(nonMenuController)
            //changeMainViewController(to: nonMenuController)
            break
        case .list:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:MyListVC = storyboard.instantiateViewController(withIdentifier: "MyListVC") as? MyListVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .myAccount:
            let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            guard let nonMenuController:MyAccountVC = storyboard.instantiateViewController(withIdentifier: "MyAccountVC") as? MyAccountVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
            
        case .Departments:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:Departmets_VC = storyboard.instantiateViewController(withIdentifier: "Departmets_VC") as? Departmets_VC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .home:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:HomeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .offers:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:OffersZone_VC = storyboard.instantiateViewController(withIdentifier: "OffersZone_VC") as? OffersZone_VC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .help:
            let storyboard = UIStoryboard(name: "Help", bundle: nil)
            guard let nonMenuController:HelpVC = storyboard.instantiateViewController(withIdentifier: ViewControllerID.help) as? HelpVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .cart:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:CartVC = storyboard.instantiateViewController(withIdentifier: "CartVC") as? CartVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .Setting:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:SettingVC = storyboard.instantiateViewController(withIdentifier: "SettingVC") as? SettingVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .cartList:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:CartVC = storyboard.instantiateViewController(withIdentifier: "CartVC") as? CartVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .rateUs:
            SKStoreReviewController.requestReview()
            break
        case .activeOrder:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:MyOrdersVC = storyboard.instantiateViewController(withIdentifier: "MyOrdersVC") as? MyOrdersVC else {
                return
            }
            self.pushToNonTabbarController(nonMenuController)
            break
        case .tabView:
            break
        case .terms:
            break
        case .category:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:AllCategoriesVC = storyboard.instantiateViewController(withIdentifier: "AllCategoriesVC") as? AllCategoriesVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .privacy:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:AllCategoriesVC = storyboard.instantiateViewController(withIdentifier: "AllCategoriesVC") as? AllCategoriesVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .fav:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let nonMenuController:FavVC = storyboard.instantiateViewController(withIdentifier: "FavVC") as? FavVC else {
                return
            }
            changeMainViewController(to: nonMenuController)
            break
        case .logout:
            let alertController = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction) in
                
            }
            let action2 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
                AppDelegate.shared.logOut()
            }
            alertController.addAction(action1)
            alertController.addAction(action2)
            self.present(alertController, animated: true, completion: nil)
            break
        }
    }
    
    func pushToNonTabbarController( _ viewcontroller: UIViewController) {
        if let customTabBar = ((slideMenuController()?.mainViewController as? UINavigationController)?.viewControllers.first as? CustomTabBarViewController) {
            if let firstVC = customTabBar.viewControllers?[customTabBar.selectedIndex] as? UINavigationController{
                firstVC.viewControllers.append(viewcontroller)
            }
            changeMainViewController(to: customTabBar)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return UITableView.automaticDimension
        }else{
        
            return 60.0;
        }
        
    }
}
