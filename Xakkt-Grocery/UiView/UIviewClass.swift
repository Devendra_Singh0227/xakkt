//
//  UIviewClass.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 26/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation
import UIKit

class GlobalView: UIView {
    class func instanceFromNib() -> ProductView {
        return UINib(nibName: "ProductView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ProductView
    }
    
    class func totalInstanceFromNib() -> CartSummaryView {
        return UINib(nibName: "CartSummaryView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CartSummaryView
    }
    
    class func OfferHeaderFromNib() -> OfferDetailHeader {
        return UINib(nibName: "OfferDetailHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OfferDetailHeader
    }
    
    class func CartFooterFromNib() -> CartFooterView {
        return UINib(nibName: "CartFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CartFooterView
    }
    
    class func ChangeAdressFromNib() -> ChangeAdressView {
        return UINib(nibName: "ChangeAdressView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ChangeAdressView
    }
}
