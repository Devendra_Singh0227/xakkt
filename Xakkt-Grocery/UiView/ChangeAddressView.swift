//
//  ChangeAddressView.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 05/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import Foundation
import  UIKit

class ChangeAdressView: UIView {

    @IBOutlet weak var cancel_Btn: DesignableButton!
    @IBOutlet weak var manually_Btn: UIButton!
    @IBOutlet weak var currentLocation_Btn: DesignableButton!
    
}
