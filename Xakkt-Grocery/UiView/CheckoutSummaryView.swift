//
//  TotalCalculationView.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 28/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class CheckoutSummaryView: UIView {

    @IBOutlet weak var deliveryStackView:UIStackView!
    @IBOutlet weak var couponStackView:UIStackView!
    
    @IBOutlet weak var couponCodeLbl:UILabel!
    
    @IBOutlet weak var checkOut_Btn: UIButton!
    @IBOutlet weak var subTotalLbl:UILabel!
    @IBOutlet weak var deliveryFeelbl:UILabel!
    @IBOutlet weak var esitmatedLbl:UILabel!
    @IBOutlet weak var apply_Coupon: UIButton!
    @IBOutlet weak var remove_CouponBtn: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
