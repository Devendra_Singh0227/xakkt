//
//  StoreHeaderView.swift
//  Xakkt-Grocery
//
//  Created by Developer on 11/10/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class StoreHeaderView: UIView {

    @IBOutlet weak var storeLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.storeLocation.text = GlobalObject.selectedStore?.name
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
