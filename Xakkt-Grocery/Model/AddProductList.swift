// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let addWishList = try? newJSONDecoder().decode(AddWishList.self, from: jsonData)

import Foundation

// MARK: - AddWishList
struct AddWishList: Codable {
    let status, message: String?
    let data: DataC?
}

// MARK: - DataClass
struct DataC: Codable {
    let id, user, product, store: String?
    let wishPrice, maxPrice: Double?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case user = "_user"
        case product = "_product"
        case store = "_store"
        case wishPrice = "wish_price"
        case maxPrice = "max_price"
        case createdAt, updatedAt
        case v = "__v"
    }
}

struct productList: Codable {
    let status, message: String?
    let data: [List]?
}

struct List: Codable {
    let id: String?
    let regular_price : Int?
    let store : String?
    let deal_price: Double?
    let image: String?
    let deal: String?
    let sku: String?
    let name: Name?
    let deal_percentage : Double
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case store = "_store"
        case regular_price
        case image, deal, sku, name, deal_price
        case deal_percentage
        
    }
}
