//
//  ShoppingProductList.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 11/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation


struct BannerProduct: Codable {
    var status: Minimum?
    var message: String?
    var data: [Datum]?
}

// MARK: - ListProductDetail
struct ListProductDetail: Codable {
    var status:Minimum?
    var message: String?
    var data: [Datum]?
}

// MARK: - Datum
struct Datum: Codable {
    var id: String?
    var product: Product?
    var quantity: Int?
    var createdAt, updatedAt: String?
    var v: Int?
    var image: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case product = "_product"
        case quantity, createdAt, updatedAt
        case v = "__v"
        case image = "image"
    }
}







// MARK: - ListCategory
struct CategoryProductList: Codable {
    var status:Minimum?
    var message: String?
    var data: [Product]?
}



// MARK: - ListCategory
struct StoreCategory: Codable {
    var status:Minimum?
    var message: String?
    var data: [CategoryData]?
}

// MARK: - Datum
struct CategoryData: Codable {
    var id: String?
    var name: String?
    var logo: String?
    var child: [CategoryData]?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name,logo,child
    }
}


