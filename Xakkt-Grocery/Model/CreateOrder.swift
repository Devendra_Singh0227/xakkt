//
//  CreateOrder.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 13/01/21.
//  Copyright © 2021 Xakkt. All rights reserved.
//

import Foundation

struct CreateOrder: Codable {
    let status: Minimum?
    let message: String?
    let data: OrderId?
}

struct OrderId: Codable {
    let order_id: String?
}





struct OrderList: Codable {
    let status: Minimum?
    let message: String?
    let data: [Order]?
}
struct OrderStore: Codable {
    let id, name: String?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
    }
}
struct Order: Codable {
    let store:OrderStore?
    let id, user: String?
    let shipping: Shipping?
    let feedback: Feedback?
    let payment: Payment?
    let products: [Product]?
    let createdAt, updatedAt: String?
    let total_cost:Minimum
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case store = "_store"
        case user = "_user"
        case feedback
        case payment
        case shipping
        case products
        case createdAt, updatedAt,total_cost
    }
}

struct Feedback: Codable {
    
    let comment: String?
    let rating: Minimum?
    
    enum CodingKeys: String, CodingKey {
        
        case comment
        case rating
        
    }
}

struct Payment: Codable {
    let method: Minimum?
    enum CodingKeys: String, CodingKey {
        case method
    }
}

struct Shipping: Codable {
    
    let address:Address
    let order_id, delivery_notes:String?
    let tracking: Tracking?
    
    enum CodingKeys: String, CodingKey {
        case address,delivery_notes
        case order_id
        case tracking
    }
}

struct Tracking: Codable {
    
    let company, estimated_delivery, status, tracking_number: String?
    
    enum CodingKeys: String, CodingKey {
        
        case company, estimated_delivery, status, tracking_number
        
    }
}
