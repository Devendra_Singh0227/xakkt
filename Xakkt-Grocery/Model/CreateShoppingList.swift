//
//  CreateShoppingList.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 09/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation


// MARK: - CreateShoppingList
struct CreateShoppingList: Codable {
    var status:Minimum?
    var message: String?
    var data: ListData?
}

// MARK: - DataClass
struct DataCreteList: Codable {
    var products: [JSONAny]?
    var id, user, name, createdAt: String?
    var updatedAt: String?
    var v: Int?

    enum CodingKeys: String, CodingKey {
        case products = "_products"
        case id = "_id"
        case user = "_user"
        case name, createdAt, updatedAt
        case v = "__v"
    }
}



// MARK: - ShoppingList
struct ShoppingList: Codable {
    var status:Minimum?
    var message: String?
    var data: [ListData]?
}

// MARK: - Datum
struct ListData: Codable {
    var id, name,product,user : String?
    var max_price, wish_price : String?
    

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case product = "_product"
        case user = "_user"
        case max_price
        case wish_price
        case name
    }
}




// MARK: - WishList
struct WishList: Codable {
    var status, message: String?
    var data: [WishListData]?
}

// MARK: - Datum
struct WishListData: Codable {
    var id,product,user : String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case product = "_product"
        case user = "_user"
    }
}
