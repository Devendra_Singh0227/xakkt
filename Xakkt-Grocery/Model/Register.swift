//
//  Login.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 5/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let login = try? newJSONDecoder().decode(Login.self, from: jsonData)

import Foundation

// MARK: - Login
struct Register: Codable {
    let status: Minimum?
    let message: String?
    let data: User?
}


// MARK: - RemoveProduct
struct RemoveProduct: Codable {
    let status: Minimum?
    let message: String?
}

// MARK: - RemoveProduct
struct RemoveList: Codable {
    let status: String?
    let message: String?
}


struct AddAddress: Codable {
    let status: Minimum?
    let message: String?
    //let errors:[[String:Any]]?
}
