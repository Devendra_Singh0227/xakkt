// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let home = try? newJSONDecoder().decode(Home.self, from: jsonData)

import Foundation

struct CartCount: Codable {
    var status: Minimum?
    let message:String?
    var data: CartCountData?
}
struct CartCountData: Codable {
    var total_products:Minimum?
}


// MARK: - Home
struct Home: Codable {
    var status: Minimum?
    var data: [AllData]?
}

// MARK: - Datum
struct AllData: Codable {
    let message:String?
    var path: String?
    var type: type?
    var banner: [Banner]?
    var subType: String?
    var product: [Product]?

    enum CodingKeys: String, CodingKey {
        case message, path, type, banner
        case subType = "sub_type"
        case product
    }
}


// MARK: - Banner
struct Banner: Codable {
    let deal:Deal?
    let id, bannerDescription, url, name: String?
    let image, type: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case deal = "_deal"
        case bannerDescription = "description"
        case url, name, image, type
        case v = "__v"
    }
}

// MARK: - Product
struct Deal: Codable {
    let id,name:String?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name = "name"
    }
}
struct Product: Codable {
    var id, unit: String?
    var weight, quantity, in_cart:ConfigDatumElement?
    var name: Name?
    var sku, title, product: String?
    var price, deal_price: Minimum?
    var unit_cost: Minimum?
    var image, type, currency: String?
    var in_shoppinglist, is_favourite: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case price = "regular_price"
        case deal_price
        case product = "_product"
        case unit, weight, quantity, name, sku, image, type, in_cart, title, currency
        case in_shoppinglist,is_favourite, unit_cost
    }
}


enum type: String, Codable {
    case banner = "banner"
    case product = "product"
}

// MARK: - Name
struct Name: Codable {
    var arabic, english, spanish: String?
}
