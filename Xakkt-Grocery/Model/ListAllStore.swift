//
//  ListAllStore.swift
//  Xakkt-Grocery
//
//  Created by Ankur Nautiyal on 29/08/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation

// MARK: - ListAllStore
struct ListAllStore: Codable {
    let status:Minimum?
    let message: String?
    let data: [Store]?
}

// MARK: - Datum
struct Store: Codable {
    let currency: Currency?
    let location: Location?
    let status, isdeleted: Bool?
    let userID: String?
    let icon: Int?
    let id, name, address, city, state, contactNo: String?
    let zipcode: ConfigDatumElement?
    let createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case location, status, isdeleted
        case currency = "_currency"
        case userID = "user_id"
        case icon
        case id = "_id"
        case name, address, city, state
        case contactNo = "contact_no"
        case zipcode, createdAt, updatedAt
        case v = "__v"
    }
}

struct Currency: Codable {
    let id,name:String?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name = "name"
    }
}
// MARK: - Location
struct Location: Codable {
    let type: String?
    let coordinates: [Minimum]?
}



// MARK: - ProductAddList
struct ProductAddList: Codable {
    var status:Minimum?
    var message: String?
    var total_products: Minimum?
}


extension Minimum {
    var integerValue: Int? {
        switch self {
        case .integer(let value): return value
        case .string(let value): return Int(value)
        case .double(let value): return Int(value)
        }
    }
    var doubleValue: Double? {
        switch self {
        case .integer(let value): return Double(value)
        case .string(let value): return Double(value)
        case .double(let value): return value
        }
    }
    var stringValue: String? {
        switch self {
        case .integer(let value): return "\(value)"
        case .string(let value): return "\(value)"
        case .double(let value): return "\(value)"
        }
    }
}
enum Minimum: Codable {
    case integer(Int)
    case string(String)
    case double(Double)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(Double.self) {
            self = .double(x)
            return
        }
        throw DecodingError.typeMismatch(Minimum.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Minimum"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        case .double(let x):
            try container.encode(x)
        }
    }
}





