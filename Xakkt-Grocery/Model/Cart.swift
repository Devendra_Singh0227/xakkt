// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let cart = try? newJSONDecoder().decode(Cart.self, from: jsonData)

import Foundation

// MARK: - Cart
struct Cart: Codable {
    let status:Minimum
    let message: String?
    let data: CartData?
    let subtotal: Subtotal?
}

// MARK: - DataClass
struct CartData: Codable {
    let id, user, store: String?
    let cart: [CartElement]?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case user = "_user"
        case store = "_store"
        case cart, createdAt, updatedAt
    }
}

// MARK: - CartElement
struct CartElement: Codable {
    let id: String?
    let product: Product?
    let quantity: ConfigDatumElement?
    let totalPrice: ConfigDatumElement?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case product = "_product"
        case quantity = "in_cart"
        case totalPrice = "total_price"
    }
}

// MARK: - Subtotal
struct Subtotal: Codable {
    let quantity: Minimum?
    let price: Minimum?
    let shippingCost: Minimum?
    let coupon: Coupon?
    let subTotal: Minimum?

    enum CodingKeys: String, CodingKey {
        case quantity, price
        case shippingCost = "shipping_cost"
        case coupon
        case subTotal = "sub_total"
    }
}

// MARK: - Coupon
struct Coupon: Codable {
    let code, discount: String?
}


extension ConfigDatumElement {
    var integerValue: Int? {
        switch self {
        case .integer(let value): return value
        case .string(let value): return Int(value)
        case .double(let value): return Int(value)
        }
    }
    var doubleValue: Double? {
        switch self {
        case .integer(let value): return Double(value)
        case .string(let value): return Double(value)
        case .double(let value): return value
        }
    }
    var stringValue: String? {
        switch self {
        case .integer(let value): return "\(value)"
        case .string(let value): return "\(value)"
        case .double(let value): return "\(value)"
        }
    }
}

enum ConfigDatumElement: Codable {
    case double(Double)
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(Double.self) {
            self = .double(x)
            return
        }
        throw DecodingError.typeMismatch(ConfigDatumElement.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ConfigDatumElement"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .double(let x):
            try container.encode(x)
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}








// MARK: - Cart
struct RemoveCart: Codable {
    let status, message: String?
    let data: RemoveCartData?
}

// MARK: - DataClass
struct RemoveCartData: Codable {
    let id, user, store: String?
    let cart: [RemoveCartElement]?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case user = "_user"
        case store = "_store"
        case cart, createdAt, updatedAt
    }
}

// MARK: - CartElement
struct RemoveCartElement: Codable {
    let id: String?
    let product: String?
    let quantity: ConfigDatumElement?
    let totalPrice: ConfigDatumElement?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case product = "_product"
        case quantity
        case totalPrice = "total_price"
    }
}
