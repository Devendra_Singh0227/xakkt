//
//  Categories.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 31/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import Foundation

struct Categories: Codable {
    let status: String?
    let message: String?
    let data: [categories]?
}

struct categories: Codable {

    let id: String?
    let logo: String?
    let createdAt, updatedAt: String?
    let v, no_of_stores: Int?
    let name: String?
    let products : [products]?
    let coupon_code: String?
    let store: String?
    
    enum CodingKeys: String, CodingKey {
        
        case name
        case logo
        case id = "_id"
        case createdAt, updatedAt
        case v = "__v"
        case products
        case coupon_code
        case store = "_store"
        case no_of_stores
    }
}

struct products: Codable {

    let id: String?
    let logo: String?
    let createdAt, updatedAt: String?
    let v: Int?
    let name: String?

    enum CodingKeys: String, CodingKey {
        
        case name
        case logo
        case id = "_id"
        case createdAt, updatedAt
        case v = "__v"
    
    }
}
