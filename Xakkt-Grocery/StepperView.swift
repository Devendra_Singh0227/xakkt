//
//  StepperView.swift
//  ChefJoy
//
//  Created by Developer on 19/07/20.
//  Copyright © 2020 Chef Joy LLC. All rights reserved.
//

import UIKit


protocol StepperViewDelegate {
    func setValue(value:Int, tag:Int, isIncrease:Bool) -> Void
}

class StepperView: UIView {

    var content: UIView?
    var isIncrease:Bool = false
    
    var delegate:StepperViewDelegate?
    
    @IBOutlet weak var decreseBtn: UIButton!
    @IBOutlet weak var increaseBtn: UIButton!
    @IBOutlet weak var txtvalue: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
      content = loadViewFromNib()
      guard let content = content else { return }
      content.frame = bounds
      content.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      addSubview(content)
    }

    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: String(describing:StepperView.self), bundle: nil)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            
         return UIView()
        }
      return view
    }
    
    
    var maxValue = 10
    var value:Int = 1{
        didSet{
            self.txtvalue.text = "\(value)"
        }
    }
    
    @IBAction func increaseValue(sender: UIButton){
        if maxValue >  value{
            isIncrease = true
            value = value + 1
            self.delegate?.setValue(value: value, tag: self.tag, isIncrease: isIncrease)
        }
    }
    @IBAction func decreaseValue(sender: UIButton){
        if value > 1 {
            isIncrease = false
            value = value - 1
            self.delegate?.setValue(value: value, tag: self.tag, isIncrease: isIncrease)
        }
    }

}
