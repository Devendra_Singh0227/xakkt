//
//  UIViewController+Alert.swift
//  QRScanner
//
//  Created by KM, Abhilash a on 11/03/19.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showToast(message : String, seconds: Double = 2.0) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        self.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    
    func Aler_WithAction(message: String,completion: @escaping (Bool) -> ()) {
        let alertVC = UIAlertController(title: "Message!", message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
            completion(true)
        }))
        self.present(alertVC, animated: true)
    }
    
    func showAlert(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func removeKeyBoard(){
          view.endEditing(true)
      }
    
    func isValidEmail(testStr:String) -> Bool {
           let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
           
           let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
           return emailTest.evaluate(with: testStr)
       }
      
    
    func formattedNumber(number: String) -> String {
        var cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        var mask = "(XXX)XXX-XXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask.characters {
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    func addline(text: String) -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
}



//extension String {
//    
//    fileprivate static let ANYONE_CHAR_UPPER = "X"
//    fileprivate static let ONLY_CHAR_UPPER = "C"
//    fileprivate static let ONLY_NUMBER_UPPER = "N"
//    fileprivate static let ANYONE_CHAR = "x"
//    fileprivate static let ONLY_CHAR = "c"
//    fileprivate static let ONLY_NUMBER = "n"
//    
//    var containsSpecialCharacter: Bool {
//       let regex = ".*[^A-Za-z0-9].*"
//       let testString = NSPredicate(format:"SELF MATCHES %@", regex)
//       return testString.evaluate(with: self)
//    }
//    
//    func formattedUSNumber() -> String {
//        let cleanPhoneNumber = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
//        let mask = "(XXX) XXX-XXXX"
//
//        var result = ""
//        var index = cleanPhoneNumber.startIndex
//        for ch in mask where index < cleanPhoneNumber.endIndex {
//            if ch == "X" {
//                result.append(cleanPhoneNumber[index])
//                index = cleanPhoneNumber.index(after: index)
//            } else {
//                result.append(ch)
//            }
//        }
//        return result
//    }
//    
//    func format(_ format: String, oldString: String) -> String {
//        let stringUnformated = self.unformat(format, oldString: oldString)
//        var newString = String()
//        var counter = 0
//        if stringUnformated.count == counter {
//            return newString
//        }
//        for i in 0..<format.count {
//            var stringToAdd = ""
//            let unicharFormatString = format[i]
//            let charFormatString = unicharFormatString
//            let charFormatStringUpper = unicharFormatString.uppercased()
//            let unicharString = stringUnformated[counter]
//            let charString = unicharString
//            let charStringUpper = unicharString.uppercased()
//            if charFormatString == String.ANYONE_CHAR {
//                stringToAdd = charString
//                counter += 1
//            } else if charFormatString == String.ANYONE_CHAR_UPPER {
//                stringToAdd = charStringUpper
//                counter += 1
//            } else if charFormatString == String.ONLY_CHAR_UPPER {
//                counter += 1
//                if charStringUpper.isChar() {
//                    stringToAdd = charStringUpper
//                }
//            } else if charFormatString == String.ONLY_CHAR {
//                counter += 1
//                if charString.isChar() {
//                    stringToAdd = charString
//                }
//            } else if charFormatStringUpper == String.ONLY_NUMBER_UPPER {
//                counter += 1
//                if charString.is_Number() {
//                    stringToAdd = charString
//                }
//            } else {
//                stringToAdd = charFormatString
//            }
//            
//            newString += stringToAdd
//            if counter == stringUnformated.count {
//                if i == format.count - 2 {
//                    let lastUnicharFormatString = format[i + 1]
//                    let lastCharFormatStringUpper = lastUnicharFormatString.uppercased()
//                    let lasrCharControl = (!(lastCharFormatStringUpper == String.ONLY_CHAR_UPPER) &&
//                        !(lastCharFormatStringUpper == String.ONLY_NUMBER_UPPER) &&
//                        !(lastCharFormatStringUpper == String.ANYONE_CHAR_UPPER))
//                    if lasrCharControl {
//                        newString += lastUnicharFormatString
//                    }
//                }
//                break
//            }
//        }
//        return newString
//    }
//    
//    func unformat(_ format: String, oldString: String) -> String {
//        var string: String = self
//        var undefineChars = [String]()
//        for i in 0..<format.count {
//            let unicharFormatString = format[i]
//            let charFormatString = unicharFormatString
//            let charFormatStringUpper = unicharFormatString.uppercased()
//            if !(charFormatStringUpper == String.ANYONE_CHAR_UPPER) &&
//                !(charFormatStringUpper == String.ONLY_CHAR_UPPER) &&
//                !(charFormatStringUpper == String.ONLY_NUMBER_UPPER) {
//                var control = false
//                for i in 0..<undefineChars.count {
//                    if undefineChars[i] == charFormatString {
//                        control = true
//                    }
//                }
//                if !control {
//                    undefineChars.append(charFormatString)
//                }
//            }
//        }
//        if oldString.count - 1 == string.count {
//            var changeCharIndex = 0
//            for i in 0..<string.count {
//                let unicharString = string[i]
//                let charString = unicharString
//                let unicharString2 = oldString[i]
//                let charString2 = unicharString2
//                if charString != charString2 {
//                    changeCharIndex = i
//                    break
//                }
//            }
//            let changedUnicharString = oldString[changeCharIndex]
//            let changedCharString = changedUnicharString
//            var control = false
//            for i in 0..<undefineChars.count {
//                if changedCharString == undefineChars[i] {
//                    control = true
//                    break
//                }
//            }
//            if control {
//                var i = changeCharIndex - 1
//                while i >= 0 {
//                    let findUnicharString = oldString[i]
//                    let findCharString = findUnicharString
//                    var control2 = false
//                    for j in 0..<undefineChars.count {
//                        if findCharString == undefineChars[j] {
//                            control2 = true
//                            break
//                        }
//                    }
//                    if !control2 {
//                        string = (oldString as NSString).replacingCharacters(in: NSRange(location: i, length: 1), with: "")
//                        break
//                    }
//                    i -= 1
//                }
//            }
//        }
//        for i in 0..<undefineChars.count {
//            string = string.replacingOccurrences(of: undefineChars[i], with: "")
//        }
//        return string
//    }
//    
//    fileprivate func isChar() -> Bool {
//        return regexControlString(pattern: "[a-zA-ZğüşöçıİĞÜŞÖÇ]")
//    }
//    
//    fileprivate func is_Number() -> Bool {
//        return regexControlString(pattern: "^[0-9]*$")
//    }
//    
//    fileprivate func regexControlString(pattern: String) -> Bool {
//        do {
//            let regex = try NSRegularExpression(pattern: pattern, options: [])
//            let numberOfMatches = regex.numberOfMatches(in: self, options: [], range: NSRange(location: 0, length: self.count))
//            return numberOfMatches == self.count
//        } catch {
//            return false
//        }
//    }
//}

extension UIDevice {
    
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
   
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    
   
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX_Max        = "iPhone X, iPhone Max"
        case iPhone11_XS       = "iPhone 11, iPhone XS"
        case iPhone11MaxPro_XMax = "iPhone Pro, iPhone X Max"
        case unknown // iphone 1792
    }
   
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX_Max
        case 1792:
            return .iPhone11_XS
        case 2688:
            return .iPhone11MaxPro_XMax

        default:
            return .unknown
        }
    }
}

extension UIView {
    
    func shadow () {
        self.layer.cornerRadius = 10.0
        self.layer.borderWidth = 0.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 5
        
    }
    
}
