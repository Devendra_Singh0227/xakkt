//
//  ShadowView.swift
//  Shiprocket
//
//  Created by Kraftly03 on 9/20/18.
//  Copyright © 2018 Shiprocket. All rights reserved.
//

import UIKit

@IBDesignable
class ShadowView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.3
    }
    
    
//    @IBInspectable var cornerRadius: CGFloat = 0.0 {
//        didSet {
//            self.layer.cornerRadius = cornerRadius
//            //self.clipsToBounds = true
//        }
//    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
