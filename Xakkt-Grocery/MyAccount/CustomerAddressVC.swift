//
//  CustomerAddressVC.swift
//  Xakkt-Grocery
//
//  Created by Developer on 19/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit
import Alamofire


protocol CustomerAddressVCDelegate {
    func setAddress(address:AddAddress)
}


class CustomerAddressTableCell: UITableViewCell {
    @IBOutlet weak var addressType: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var edit_Btn: UIButton!
    @IBOutlet weak var delete_Btn: UIButton!
    @IBOutlet weak var setDefault_Btn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class CustomerAddressVC: UIViewController {
    var delegate:CustomerAddressVCDelegate?
    var addresses: [Address] = []{
        didSet{
            DispatchQueue.main.async {
                self.addressTableView.reloadData()
            }
        }
    }
    
    @IBOutlet weak var addressTableView: UITableView!
    
    var Name = ""
    var contact = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchCustomerAddressList(isRefresh: false)
    }
    
    @IBAction func ClickonBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewCustomerAddress(_ sender: UIButton) {
        let dashboardVC:AddAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        dashboardVC.delegate =  self
        dashboardVC.isAddNewAddress = true
        dashboardVC.isComingFromMyAccount = true
        dashboardVC.phoneNo = self.contact
        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
    
    @IBAction func menu_BtnAction(_ sender: UIButton) {
          slideMenuController()?.toggleLeft()
    }
    
    @objc func setDefaultAddress(_ sender: UIButton) {
        guard let addressId = self.addresses[sender.tag].addressID else {
            return
        }
        let param = ["_address": addressId] as [String : Any]
        Webservices.instance.postMethod(API.setDefaultAddress, param: param, header: true, completion: {(error :String?, data: CustomerAddress?) -> Void in
            if data?.state?.integerValue == 1 {
                self.fetchCustomerAddressList(isRefresh: false)
            }
        })
    }
}

extension CustomerAddressVC: UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"CustomerAddressTableCell") as! CustomerAddressTableCell
        cell.setDefault_Btn.tag = indexPath.row
        cell.setDefault_Btn.addTarget(self, action: #selector(self.setDefaultAddress), for: .touchUpInside)
        
        if self.addresses[indexPath.row].is_default == true {
            cell.setDefault_Btn.isHidden = true
        }else{
            cell.setDefault_Btn.isHidden = false
        }
        cell.addressType.text = self.addresses[indexPath.row].addressType
        cell.mobileNo.text = self.contact
        cell.customerName.text = self.addresses[indexPath.row].name
        cell.address.text = self.addresses[indexPath.row].address1
        cell.delete_Btn.tag = indexPath.row
        cell.edit_Btn.addTarget(self, action: #selector(edit_Address_Btn(sender:)), for: .touchUpInside)
        cell.delete_Btn.addTarget(self, action: #selector(delete_Address_Btn), for: .touchUpInside)
        return cell
        
    }
    
    @objc func delete_Address_Btn(sender: UIButton) {
        let Id = self.addresses[sender.tag].addressID
        self.addresses.removeAll()
        print(self.addresses.count)
        deleteAddressList(isRefresh: true, Id: Id!)
    }
    
    @objc func edit_Address_Btn(sender: UIButton) {
        let dashboardVC:AddAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        dashboardVC.isAddNewAddress = false
        dashboardVC.phoneNo = self.contact
        dashboardVC.addresses = self.addresses[sender.tag]
        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
}

extension CustomerAddressVC:AddAddressVCDelegate{
    func fetchCustomerAddressList(isRefresh:Bool){
        
        Webservices.instance.getMethod(API.getAddressList) { (data : CustomerAddress?, error) in
            if data?.state?.integerValue == 1, let array = data?.data?.address, array.count > 0 {
                self.addresses = array
            } else if data?.state?.integerValue == 1, let array = data?.data?.address, array.count == 0, self.addresses.count == 0{
                DispatchQueue.main.async {
                    let noOrderView = self.view.viewWithTag(100)
                    noOrderView?.isHidden = false
                }
            } else if data?.state?.integerValue == 1, let array = data?.data?.address, array.count == 0{
            }
        }
    }
    
    func deleteAddressList(isRefresh:Bool, Id: String){
        
        Webservices.instance.getMethod("\(API.deleteNewUser)\(Id)") { (data : CustomerAddress?, error) in
            
            if data?.state?.integerValue == 1 {
                self.fetchCustomerAddressList(isRefresh: true)
            }else if data?.state?.integerValue == 1, let array = data?.data?.address, array.count == 0, self.addresses.count == 0{
                let noOrderView = self.view.viewWithTag(100)
                noOrderView?.isHidden = false
            }else if data?.state?.integerValue == 1, let array = data?.data?.address, array.count == 0{
            }
        }
    }
}

