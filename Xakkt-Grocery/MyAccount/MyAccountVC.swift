//
//  MyAccountVC.swift
//  MyAccount
//
//  Created by Shobha Kumari on 27/09/1941 Saka.
//  Copyright © 1941 Shobha Kumari. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class MyAccountCell : UITableViewCell {
    @IBOutlet weak var LeftArrow: UIButton!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var Label: UILabel!
}

class MyAccountVC: UIViewController {
    var isComingFromMenu:Bool = false

    var labelname = ["Personal Details", "Delivery Address", "Customer Service"]
    var icon = ["Personal-Details-icon", "Delivery-Add-icon", "Customer-Service-icon"]
    @IBOutlet weak var AccountTableView: UITableView!
    var isshowBottomView = true
    var isShowContactctCell = false
    var sellerNumber:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AccountTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tabBarController?.tabBar.isHidden = false
        // Do any additional setup after loading the view.
        
        
        //************************* Refresh Seller Number *****************
    }
    
    @IBAction func ClickonBackBtn(_ sender: Any) {
         slideMenuController()?.toggleLeft()
    }
    
    @objc func clickHereButton() {
       if self.isshowBottomView == true {
            self.isshowBottomView = false
            self.AccountTableView.reloadData()
        }
    }
    
    @IBAction func mailBtnTapped(_ sender: Any) {
        let email = "customercare@mykirana.com"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    @IBAction func callBtnTapped(_ sender: Any) {
        if let basicSetting = UserDefaults.standard.value(forKey: "BasicSetting") as? NSDictionary, let customer_care_number =  basicSetting.value(forKey: "customer_care_number") as? String {
            guard let number = URL(string: "tel://" + customer_care_number) else { return }
            UIApplication.shared.open(number)
        }else{
            guard let number = URL(string: "tel://" + "18001003070") else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @IBAction func extracallBtnTapped(_ sender: Any) {
        if let basicSetting = UserDefaults.standard.value(forKey: "BasicSetting") as? NSDictionary, let new_customer_care_number =  basicSetting.value(forKey: "new_customer_care_number") as? String {
            guard let number = URL(string: "tel://" + new_customer_care_number) else { return }
            UIApplication.shared.open(number)
        }else{
            guard let number = URL(string: "tel://" + "18001003070") else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @IBAction func menu_Btn(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func ContactStoreNoTapped(_ sender: Any) {
        guard let number = URL(string: "tel://" + "") else { return }
        UIApplication.shared.open(number)
        
    }
    
}

extension MyAccountVC:UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelname.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactStoreCell") as! ContactStoreCell
            
            cell.Label.text = labelname[indexPath.row]
            cell.iconImage?.image = UIImage(named: icon[indexPath.row])
//            cell.callback = {(value) in
//                cell.stackView.isHidden = true
//            }
            
            cell.clickHereButton.addTarget(self, action: #selector(self.clickHereButton), for: .touchUpInside)
            cell.bottomView.isHidden = self.isshowBottomView
            cell.emailButton.addTarget(self, action: #selector(self.mailBtnTapped(_:)), for: .touchUpInside)
            cell.callBtn.addTarget(self, action: #selector(self.callBtnTapped(_:)), for: .touchUpInside)
            cell.extraCallBtn.addTarget(self, action: #selector(self.extracallBtnTapped(_:)), for: .touchUpInside)
            cell.contactStoreNo.addTarget(self, action: #selector(self.ContactStoreNoTapped(_:)), for: .touchUpInside)
                
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountCell") as! MyAccountCell
            cell.Label.text = labelname[indexPath.row]
            cell.iconImage?.image = UIImage(named: icon[indexPath.row])
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
           let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            let dashboardVC = storyboard.instantiateViewController(withIdentifier: "PersonalDetail_VC") as? PersonalDetail_VC
            self.navigationController?.pushViewController(dashboardVC!, animated: true)
        } else if indexPath.row == 1 {
            let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            let dashboardVC = storyboard.instantiateViewController(withIdentifier: "CustomerAddressVC") as? CustomerAddressVC
            self.navigationController?.pushViewController(dashboardVC!, animated: true)
        }else if indexPath.row == 2 {
            let cell = tableView.cellForRow(at: indexPath) as! ContactStoreCell
            cell.stackView.isHidden = !cell.stackView.isHidden
        }
    }
}
