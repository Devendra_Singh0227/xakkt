//
//  AddNewAddressVC.swift
//  MyKirana
//
//  Created by Manoj Gupta on 11/12/19.
//  Copyright © 2019 BIGFOOT. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Toast_Swift

protocol AddNewAddressVCDelegate {
    func fetchCustomerAddressList()
}
extension AddNewAddressVCDelegate{
    func fetchCustomerAddressList() {
        
    }
}

class AddNewAddressVC: UIViewController {

    var delegate:AddNewAddressVCDelegate?
    var isAddNewAddress:Bool = false
    var isFromMap:Bool = false
    var addresses: Address?
    
    var zone_id:String = ""
    var pickupIndex:Int = 0
    var localityArray:[String] = []
    
    
    @IBOutlet weak var titleNameLbl: UILabel!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var flatTxt: UITextField!
    @IBOutlet weak var landmarkTxt: UITextField!
    @IBOutlet weak var areaTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var pincodeTxt: UITextField!
    @IBOutlet weak var mobileTxt: UITextField!
    
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var officeBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    let thePicker = UIPickerView()
    
    var addressType:String = "Home"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TrackManager.sharedInstance.setScreenName(screenName: "Add Edit Address Page", screenClass: "AddNewAddressVC")
        // Do any additional setup after loading the view.
        
        thePicker.delegate = self
        thePicker.dataSource = self
        self.areaTxt.inputView = thePicker
        self.areaTxt.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        if let address =  addresses{
            self.titleNameLbl.text = "Edit Address"
            self.nameTxt.text = address.name
            self.flatTxt.text = address.address1
            self.landmarkTxt.text = address.landmark
            self.areaTxt.text = address.area
            self.cityTxt.text = address.city
            self.stateTxt.text = address.state
            self.pincodeTxt.text = address.postcode
            self.getLocality(value: address.postcode!)
            
            if let type = address.addressType, type.capitalized == "Home" {
                self.homeBtn.isSelected = true
            }else if let type = address.addressType, type.capitalized == "Office" {
                self.officeBtn.isSelected = true
            }else {
                self.otherBtn.isSelected = true
            }
        }else{
            self.titleNameLbl.text = "New Address"
            self.homeBtn.isSelected = true
            guard let userPincode = UserDefaults.standard.object(forKey: "UserPinCode") as? String else {
                    return
            }
            
            
//            guard let savedShop = UserDefaults.standard.object(forKey: "SavedShop") as? Data else {
//                    return
//            }
//            let decoder = JSONDecoder()
//            guard let shop = try? decoder.decode(RecommendShop.self, from: savedShop) else {
//                    return
//            }
            self.pincodeTxt.text = userPincode
            self.getLocality(value: userPincode)
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func clickOnBackButton(_ sender: Any) {
        if isAddNewAddress == true {
            self.navigationController?.popViewController(animated: true)
        }else{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: CartVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
    @IBAction func selectAddressSomeWhereYouWant(_ sender: UIButton) {
        switch sender {
        case homeBtn:
            // Change Address to Home
            homeBtn.isSelected = true
            officeBtn.isSelected = false
            otherBtn.isSelected = false
            self.addressType = "Home"
        case officeBtn:
            // or Office
            homeBtn.isSelected = false
            officeBtn.isSelected = true
            otherBtn.isSelected = false
            self.addressType = "Office"
        case otherBtn:
            // or Other
            self.addressType = "Other"
            homeBtn.isSelected = false
            officeBtn.isSelected = false
            otherBtn.isSelected = true
        default:
            break

        }

    }
    @IBAction func clickOnSubmit(_ sender: UIButton) {
        sender.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            sender.isEnabled = true
        }
        guard let name = self.nameTxt.text, !name.isEmpty else {
            self.showAlert("", message: "Please enter name.")
            return
        }
        
        
        guard let flatNo = self.flatTxt.text, !flatNo.isEmpty else {
            self.showAlert("", message: "Please enter Flat / House No. / Building Name.")
            return
        }
        
        guard let landmark = self.landmarkTxt.text, !landmark.isEmpty else {
            self.showAlert("", message: "Please enter landmark.")
            return
        }
        
        guard let city = self.cityTxt.text, !city.isEmpty else {
            self.showAlert("", message: "Please enter city.")
            return
        }
        
        guard let state = self.stateTxt.text, !state.isEmpty else {
            self.showAlert("", message: "Please enter state.")
            return
        }
        
        guard let pincode = self.pincodeTxt.text, !pincode.isEmpty else {
            self.showAlert("", message: "Please enter pincode.")
            return
        }
        guard let area = self.areaTxt.text, !area.isEmpty else {
            self.showAlert("", message: "Please enter area.")
            return
        }
        
        if let address = self.addresses {
            let parameters: Parameters=[
                "address_id":address.addressID!,
                "address_type": self.addressType,
                "address_1": self.flatTxt.text ?? "",
                "address_2": area,
                "city": city,
                "country": "India",
                "country_id": "91",
                "name": name,
                "landmark": self.landmarkTxt.text ?? "",
                "mobile": UserModel.shared.fetchUser()?.customer?.telephone ?? "",
                "postcode": pincode,
                "state": state,
                "zone_id": self.zone_id
            ]
            self.editAddress(parameters: parameters)
        }else{
            let parameters: Parameters=[
                "address_type": self.addressType,
                "address_1": self.flatTxt.text ?? "",
                "address_2": area,
                "city": city,
                "country": "India",
                "country_id": "91",
                "name": name,
                "landmark": self.landmarkTxt.text ?? "",
                "mobile": UserModel.shared.fetchUser()?.customer?.telephone ?? "",
                "postcode": pincode,
                "state": state,
                "zone_id": self.zone_id
            ]
            checkServiceablity(postcode: pincode, area: area, city: city, state: state, parameters: parameters, isEdit: false)
        }
    }
    
    func checkServiceablity(postcode:String, area:String, city:String, state:String, parameters:Parameters, isEdit:Bool) {
        
        let serviceParameters: Parameters=[
            "postcode": postcode,
            "vendor_id": AppDelegate.sharedInstance().store_Id!,
            "area":area,
            "city":city,
            "state":state
        ]
        Webservices.instance.CheckServiceability(parameters: serviceParameters) { (success, response) in
            if success == true
            {
                guard let responseData = response as? NSDictionary else { return }
                if let success = responseData.value(forKey: "success") as? Int, success == 1 {
                    if isEdit == true
                    {
                        self.editAddress(parameters: parameters)
                    }else
                    {
                        self.addNewAddress(parameters: parameters)
                    }
                }else if let error = responseData.value(forKey: "errors") as? [String], let message = error.first
                {
                    self.showAlert("Mykirana", message: message)
                }else if let message = responseData.value(forKey: "message") as? String
                {
                    // Create the actions
                    let editAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    }
                    let continueAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.default) {
                        UIAlertAction in

                        guard let area = self.areaTxt.text, !area.isEmpty else {
                            self.showAlert("", message: "Please enter area.")
                            return
                        }
                        
                        guard let pincode = self.pincodeTxt.text, !pincode.isEmpty else {
                            self.showAlert("", message: "Please enter pincode.")
                            return
                        }
                        
                        let locationDict:[String:Any] = ["isManually":true,"postalcode":pincode, "area":area]
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let objVC:RecommendedShopVC = storyboard.instantiateViewController(withIdentifier: "RecommendedShopVC") as! RecommendedShopVC
                        objVC.locationDict = locationDict
                        objVC.isChangeStore = true
                        self.navigationController?.pushViewController(objVC, animated: true)
                        
                        
                        
                        NSLog("Cancel Pressed")
                    }
                    self.showAlert("", message: message, alertActions: [editAction,continueAction])
                }
            }
        }
    }
}


// MARK: API Function
extension AddNewAddressVC{
    func editAddress(parameters:Parameters) {
        SVProgressHUD.show(withStatus: "Editing...")
        let request = Webservices.instance.createPostRequest(urlString: APPURL.EditNewAddress, parameters: parameters)
        Alamofire.request(request as URLRequestConvertible).responseJSON { (response) in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success:
                if let result = response.result.value {
                        guard let responseData = result as? NSDictionary else { return }
                        if let success = responseData.value(forKey: "success") as? Int, success == 1 {
                            self.delegate?.fetchCustomerAddressList()
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: DeliveryAddressVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                            self.navigationController?.popViewController(animated: true)
                        } else if let error = responseData.value(forKey: "errors") as? NSArray, let errorMSg = error.firstObject as? String{
                            self.showAlert("Mykirana", message: errorMSg)
                        }else if let message = responseData.value(forKey: "message") as? String
                        {
                            self.showAlert("Mykirana", message: message)
                        }
                    }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    func addNewAddress(parameters:Parameters) {
        SVProgressHUD.show(withStatus: "Adding...")
        let request = Webservices.instance.createPostRequest(urlString: APPURL.AddNewAddress, parameters: parameters)
        Alamofire.request(request as URLRequestConvertible).responseJSON { (response) in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success:
                if let result = response.result.value {
                    guard let responseData = result as? NSDictionary else { return }
                    if let success = responseData.value(forKey: "success") as? Int, success == 1 {
                        self.delegate?.fetchCustomerAddressList()
                        
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: DeliveryAddressVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                return
                            }else if controller.isKind(of: PlaceOrderVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                return
                            }
                        }
                        self.navigationController?.popViewController(animated: true)
                    } else if let error = responseData.value(forKey: "errors") as? NSArray, let errorMSg = error.firstObject as? String{
                        self.showAlert("Mykirana", message: errorMSg)
                    }else if let message = responseData.value(forKey: "message") as? String
                    {
                        self.showAlert("Mykirana", message: message)
                    }
                    
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getLocality(value:String) -> Void {
        let parameters: Parameters = ["postcode":value]
        SVProgressHUD.show(withStatus: "Fetching...")
        let request = Webservices.instance.createPostRequest(urlString: APPURL.GetLocality, parameters: parameters)
        Alamofire.request(request as URLRequestConvertible).responseJSON { (response) in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success:
                if let result = response.result.value {
                    guard let responseData = result as? NSDictionary else { return }
                    
                    guard let data = responseData.value(forKey: "data") as? NSDictionary else { return }
                    
                    guard let postcode_details = data.value(forKey: "postcode_details") as? NSDictionary else { return }
                    
                    if let city = postcode_details.value(forKey: "city") as? String
                    {
                        self.cityTxt.text = city
                    }
                    if let locality = postcode_details.value(forKey: "locality") as? [String], locality.count > 0
                    {
                        self.localityArray = locality
                        self.thePicker.reloadAllComponents()
                    }
                    if let state = postcode_details.value(forKey: "state") as? String
                    {
                        self.stateTxt.text = state
                    }
                    if let zone_id = postcode_details.value(forKey: "zone_id") as? String
                    {
                        self.zone_id = zone_id
                    }
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
}




extension AddNewAddressVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                      replacementString string: String) -> Bool
       {
        if textField == self.pincodeTxt
        {
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString

            if newString.length == 6
            {
                self.getLocality(value: newString as String)
            }else if newString.length < 6{
                self.cityTxt.text = ""
                self.stateTxt.text = ""
                self.areaTxt.text = ""
            }
            return newString.length <= maxLength
        }else
        {
            return true
        }
      }
}




// MARK: UIPickerView Delegation
extension AddNewAddressVC: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.localityArray.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.localityArray[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickupIndex = row
        self.areaTxt.text = self.localityArray[row]
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
        self.areaTxt.text = self.localityArray[self.pickupIndex]
    }
}
