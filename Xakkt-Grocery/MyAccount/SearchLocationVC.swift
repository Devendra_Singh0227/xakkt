//
//  SearchLocationVC.swift
//  MyKirana
//
//  Created by Manoj Gupta on 28/01/20.
//  Copyright © 2020 BIGFOOT. All rights reserved.
//

import UIKit


protocol SearchLocationVCDelegate {
    //func setCustomLocation(lat:Double, long:Double, address:String) -> Void
    func setCustomLocation(lat: Double, long: Double, address: String, completion: (Bool) -> Void)
}

class LocationTableCell: UITableViewCell {
    @IBOutlet weak var txtLbl: UILabel!
}

class SearchLocationVC: UIViewController {

    var delegate:SearchLocationVCDelegate?
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    var timer = Timer()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var noResultLabel: UILabel!
    @IBOutlet var locationTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickOnBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension SearchLocationVC: UISearchBarDelegate {
    func showResults(string:String){
        
        var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                    self.noResultLabel.isHidden = true
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                    self.locationTable.reloadData()
                }
            } else {
                print(response.error ?? "ERROR")
            }
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isNumber {
            if (searchText.count >= 6)
            {
                showResults(string:searchText)
            }
        }else if searchText.count > 2
        {
            if self.timer.isValid {
                self.timer.invalidate()
            }
            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.delayedAction), userInfo: searchText, repeats: false)
            //showResults(string:searchText)
        }else {
            if self.timer.isValid {
                self.timer.invalidate()
            }
            self.autocompleteResults.removeAll()
            self.locationTable.reloadData()
        }
    }
    @objc func delayedAction(timer: Timer!) {
        let info = timer.userInfo as! String
        showResults(string:info)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.endEditing(true)
    }
}



extension SearchLocationVC:UITableViewDelegate,UITableViewDataSource
{
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfSections section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if autocompleteResults.count > 0 {
            return autocompleteResults.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = self.locationTable.dequeueReusableCell(withIdentifier: "LocationTableCell", for: indexPath) as! LocationTableCell
        if autocompleteResults.count > 0 {
            cell.txtLbl.text = autocompleteResults[indexPath.row].formattedAddress
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = autocompleteResults[indexPath.row].formattedAddress
        var input = GInput()
        input.keyword = autocompleteResults[indexPath.row].placeId
        GoogleApi.shared.callApi(.placeInformation,input: input) { (response) in
            if let place =  response.data as? GApiResponse.PlaceInfo, response.isValidFor(.placeInformation) {
                DispatchQueue.main.async {
                    if let lat = place.latitude, let long = place.longitude{
                        self.delegate?.setCustomLocation(lat: lat, long: long, address: address, completion: { (success) in
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
}




















extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}
