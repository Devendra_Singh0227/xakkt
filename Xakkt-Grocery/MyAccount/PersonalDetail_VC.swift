//
//  PersonalDetail_VC.swift
//  Xakkt-Grocery
//
//  Created by Dev's Mac on 30/12/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

class PersonalDetail_VC: UIViewController {

    @IBOutlet weak var firstName_TxtField: UITextField!
    @IBOutlet weak var lastName_TxtField: UITextField!
    @IBOutlet weak var email_TxtField: UITextField!
    @IBOutlet weak var mobile_TxtField: UITextField!
    @IBOutlet weak var password_TxtField: UITextField!
    var user_Id = ""
    @IBOutlet weak var save_BtnStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         if let userDAta : Login = Utilities.getModelKeyChange(keys: keys.userData) as Login? {
            print(userDAta)
            firstName_TxtField.text = userDAta.data?.user?.firstName ?? ""
            lastName_TxtField.text = userDAta.data?.user?.lastName ?? ""
            email_TxtField.text = userDAta.data?.user?.email ?? ""
            mobile_TxtField.text = userDAta.data?.user?.contactNo ?? ""
            user_Id = userDAta.data?.user?.id ?? ""
        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancel_Btn(_ sender: DesignableButton) {
        save_BtnStack.isHidden = true
        setUserInteraction(value: false)
    }
    
    @IBAction func save_Btn(_ sender: DesignableButton) {
        updateProfile()
    }
    
    func setUserInteraction(value: Bool){
        firstName_TxtField.isUserInteractionEnabled = value
        lastName_TxtField.isUserInteractionEnabled = value
        email_TxtField.isUserInteractionEnabled = value
        mobile_TxtField.isUserInteractionEnabled = value
    }
    
    @IBAction func edit_Btn(_ sender: UIButton) {
        save_BtnStack.isHidden = false
        setUserInteraction(value: true)
    }
    
    func updateProfile() {
        let param:[String : Any] = ["first_name": firstName_TxtField.text!,
                                    "last_name": lastName_TxtField.text!,
                                    "email": email_TxtField.text!,
                                    "contact_no" :mobile_TxtField.text!]
        
        Webservices.instance.putMethod("\(API.updateProfile)\(user_Id)", param: param, header: true, completion: {(error :String?, data: Login?) -> Void in
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    Utilities.saveModelInKeychan(ModelObj: data, key: keys.userData)
                    self.navigationController?.popViewController(animated: true)
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}
