// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let customerAddress = try? newJSONDecoder().decode(CustomerAddress.self, from: jsonData)

import Foundation

// MARK: - CustomerAddress
struct CustomerAddress: Codable {
    let state: Minimum?
    let message: String?
    let errors: [JSONAny]?
    let data: CustomerDataClass?
}

// MARK: - DataClass

struct CustomerDataClass: Codable {
    let address: [Address]?
    let first_name : String?
    let last_name : String?
    let contact_no: String?
}

// MARK: - Address
struct Address: Codable {
    let name, addressID: String?
    let address1,address2:String?
    let addressType :String?
    let city, emirate, country: String?
    let countrycode:String?
    let is_default: Bool?

    enum CodingKeys: String, CodingKey {
        case name
        case addressID = "_id"
        case address1
        case address2
        case countrycode
        case addressType = "address_type"
        case city, emirate, country
        case is_default = "is_default"
    }
}

struct location: Codable {
    let coordinates:Coordinates?
}

struct Coordinates: Codable {
    let latlongArray:[String]?
}
