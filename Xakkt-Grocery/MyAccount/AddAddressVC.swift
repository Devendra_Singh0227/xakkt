//
//  AddAddressVC.swift
//  Feed Me
//
/// Copyright (c) 2017 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import GoogleMaps
import SlideMenuControllerSwift
import Alamofire
import SVProgressHUD


protocol AddAddressVCDelegate {
    func fetchCustomerAddressList(isRefresh: Bool)
}

extension AddAddressVCDelegate{
    func fetchCustomerAddressList(isRefresh: Bool) {
        
    }
}

class AddAddressVC: UIViewController {
    
    var addresses: Address?
    var isAddNewAddress:Bool = false
    var isComingFromMyAccount:Bool = false
    var isComingFromDeliveryAddress:Bool = false

    var delegate:AddAddressVCDelegate?
    var isDropLocation = false
    var locationDict:[String:Any]?

    var postalCode:String?
    var areaCode:String?
    var lat:Double?
    var long:Double?

    var addressType:String = "Home"
    var zone_id:String?
    var city:String?
    var state:String?
    var Country: String?
    var phoneNo : String?
    
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var officeBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var titleNameLbl: UILabel!

    @IBOutlet weak var manualBtn: UIButton!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet private weak var mapCenterPinImage: UIImageView!
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!

    @IBOutlet weak var nameTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var flatTxt: SkyFloatingLabelTextField!
    @IBOutlet weak var landmarkTxt: SkyFloatingLabelTextField!
    
    private let locationManager = CLLocationManager()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.manualBtn.layer.borderColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1).cgColor
        self.manualBtn.layer.borderWidth = 1.0
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
           locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.isAddNewAddress == false {
            self.addressLabel.text = addresses?.address1
            self.nameTxt.text = addresses?.name
        }
    }
  
  private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
    let geocoder = GMSGeocoder()
    
    geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        self.addressLabel.unlock()

        //, let lines = address.lines
        guard var address = response?.firstResult() else {
        return
        }
        
        if address.postalCode == nil{
            for addressValue in (response?.results())! {
                if addressValue.postalCode != nil {
                    address = addressValue
                    break
                }
            }
        }
        print(address)
        if let lines = address.lines{
            self.addressLabel.text = lines.joined(separator: "\n")
        }else if let locality = address.locality, let subLocality = address.subLocality, let postalCode = address.postalCode, let country = address.country
        {
            self.addressLabel.text = subLocality + ", " + locality + ", " + postalCode + ", " + country
        }else if let locality = address.locality, let postalCode = address.postalCode, let country = address.country
        {
            self.addressLabel.text = locality + ", " + postalCode + ", " + country
        }else if let locality = address.locality, let country = address.country
        {
            self.addressLabel.text = locality + ", " + country
        }
        if let postalCode = address.postalCode
        {
            self.city = address.locality
            self.state = address.administrativeArea
            self.postalCode = postalCode
            self.Country = address.country
        }else
        {
            self.postalCode = ""
        }
        if address.subLocality != nil
        {
            self.areaLabel.text = address.subLocality
            self.areaCode = address.subLocality
        }else{
            self.areaLabel.text = address.locality
            self.areaCode = address.locality
        }
        
        self.lat = address.coordinate.latitude
        self.long = address.coordinate.longitude
       
      let labelHeight = self.addressLabel.intrinsicContentSize.height
      self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                          bottom: labelHeight, right: 0)
       
      UIView.animate(withDuration: 0.25) {
        self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
        self.view.layoutIfNeeded()
      }
    }
  }
  
    @IBAction func menu_BtnAction(_ sender: UIButton) {
          slideMenuController()?.toggleLeft()
    }
    
    
    
  // MARK: - Button Action
    @IBAction func setStoreLocation(_ sender: Any) {
        
    }
    @IBAction func addManuallAddressButton(_ sender: Any) {
        let dashboardVC:SearchLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchLocationVC") as! SearchLocationVC
        dashboardVC.delegate = self
        self.navigationController?.present(dashboardVC, animated: true, completion: nil)
    }
    @IBAction func clickOnBackButton(_ sender: Any) {
       
    }
  @IBAction func selectAddressSomeWhereYouWant(_ sender: UIButton) {
      switch sender {
      case homeBtn:
          // Change Address to Home
          homeBtn.isSelected = true
          officeBtn.isSelected = false
          otherBtn.isSelected = false
          self.addressType = "home"
      case officeBtn:
          // or Office
          homeBtn.isSelected = false
          officeBtn.isSelected = true
          otherBtn.isSelected = false
          self.addressType = "office"
      case otherBtn:
          // or Other
          self.addressType = "other"
          homeBtn.isSelected = false
          officeBtn.isSelected = false
          otherBtn.isSelected = true
      default:
          break

      }

  }
    

    @IBAction func openHome(_ sender: Any) {

    }

    @IBAction func clickOnSubmit(_ sender: UIButton) {
        
        guard let name = self.nameTxt.text, !name.isEmpty, name.count >= 3 else {
            self.showAlert(title: "", msg: "Please enter name.")
            return
        }
        
        if isCheckSpecialCharecter(testStr: name) == false{
            self.showAlert(title: "", msg: "Please enter valid name.")
        }
        
        guard let flatNo = self.flatTxt.text, !flatNo.isEmpty, flatNo.count >= 3 else {
            self.showAlert(title: "", msg: "Please enter Flat / House No. / Building Name.")
            return
        }
        
        guard let landmark = self.landmarkTxt.text, !landmark.isEmpty, landmark.count >= 3 else {
            self.showAlert(title: "", msg: "Please enter landmark.")
            return
        }
        
        guard let city = self.city, !city.isEmpty else {
            self.showAlert(title: "", msg: "Please pick location from map.")
            return
        }
        
        guard let state = self.state, !state.isEmpty else {
            self.showAlert(title: "", msg: "Please pick location from map.")
            return
        }
        
        guard let pincode = self.postalCode, !pincode.isEmpty else {
            self.showAlert(title: "", msg: "Please pick location from map.")
            return
        }
        let pin = Int(pincode)
        let contact = phoneNo?.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: "")//Int(phoneNo ?? "0") as? Int
        let contactNo = Int(contact ?? "0")
        guard let area = self.areaCode, !area.isEmpty else {
            self.showAlert(title: "", msg: "Please pick location from map.")
            return
        }
//        guard let zone_id = self.zone_id, !zone_id.isEmpty else {
//            return
//        }
        
        guard let lat = self.lat else {
            self.showAlert(title: "", msg: "Please pick location from map.")
            return
        }
        guard let long = self.long else {
            self.showAlert(title: "", msg: "Please pick location from map.")
            return
        }
        
        
//        {
//            "address1":"abcd",
//            "address2":"efg",
//            "address_type":"home",
//            "emirate":"Dubai",
//            "country":"UAE",
//            "zipcode":456789,
//            "address":"punjab",
//            "countrycode":"+87",
//            "contactno":9872100824,
//            "lat":343.89089,
//            "long":28.48745
//        }
        
        
        
        var parameters:Parameters = [:]
        parameters["name"] = name
        parameters["address1"] = addressLabel.text
        parameters["address2"] = landmark
        parameters["address_type"] = self.addressType
        parameters["emirate"] = state
        parameters["country"] = Country
        parameters["zipcode"] = pin
        parameters["address"] = addressLabel.text
        parameters["countrycode"] = "+91"
        parameters["contactno"] = contactNo
        parameters["lat"] = lat
        parameters["long"] = long
        if self.isAddNewAddress == false {
            parameters["address_id"] = addresses?.addressID
            self.edit_Address(parameters: parameters)
        } else{
            self.addNewAddress(parameters: parameters)
        }
    }
    
    func addNewAddress(parameters:Parameters) {
        Webservices.instance.postMethod(API.addNewUser, param: parameters, header: false, completion: {(error :String?, data: AddAddress?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    self.navigationController?.popViewController(animated: true)
//                    self.showAlert(title: "", msg: data?.message ?? "")
                }else if let message = data?.message{
                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
    
    func edit_Address(parameters:Parameters) {
        Webservices.instance.postMethod("\(API.EditAddress)\(addresses?.addressID ?? "")", param: parameters, header: false, completion: {(error :String?, data: AddAddress?) -> Void in
            
            print(data as Any)
            DispatchQueue.main.async {
                if data?.status?.integerValue == 1 {
                    self.navigationController?.popViewController(animated: true)
                    //                    self.showAlert(title: "", msg: data?.message ?? "")
                }else if let message = data?.message{
                    //                    self.showAlert(title: "", msg: message)
                }else if let message = error{
                    self.showAlert(title: "", msg: message)
                }
            }
        })
    }
}


// MARK: - Custom Location
extension AddAddressVC
{
    
}

// MARK: - CLLocationManagerDelegate
extension AddAddressVC: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    guard status == .authorizedWhenInUse else {
        mapView.camera = GMSCameraPosition.camera(withLatitude: 19.115884, longitude: 72.854202, zoom: 19.0)
      return
    }
    
    locationManager.startUpdatingLocation()
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
    
    mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 19.0, bearing: 0, viewingAngle: 0)
    locationManager.stopUpdatingLocation()
  }
    
    func checkUsersLocationServicesAuthorization(){
        /// Check if user has authorized Total Plus to use Location Services
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                
            case .notDetermined:
                // Request when-in-use authorization initially
                // This is the first and the ONLY time you will be able to ask the user for permission
                self.locationManager.delegate = self
                locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
                break
                
            case .restricted, .denied:
                // Disable location features
                
                let alert = UIAlertController(title: "Allow Location Access", message: "MyApp needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                // Button to Open Settings
                alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    }
                }))
                break
                
            case .authorizedWhenInUse, .authorizedAlways:
                if let lat = self.lat, let long = self.long, let postalcode = self.postalCode, let area = self.areaCode
                {
                    
                }
                break
            @unknown default:
                print("Full Access")
                break
            }
        }
    }
}

// MARK: - GMSMapViewDelegate
extension AddAddressVC: GMSMapViewDelegate {
  func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    reverseGeocodeCoordinate(position.target)
  }
  
  func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    addressLabel.lock()
    
    if (gesture) {
      mapCenterPinImage.fadeIn(0.25)
      mapView.selectedMarker = nil
    }
  }
  
  func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
    guard let placeMarker = marker as? PlaceMarker else {
      return nil
    }
    guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
      return nil
    }
    
    infoView.nameLabel.text = placeMarker.place.name
    if let photo = placeMarker.place.photo {
      infoView.placePhoto.image = photo
    } else {
      infoView.placePhoto.image = UIImage(named: "generic")
    }
    
    return infoView
  }
  
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    mapCenterPinImage.fadeOut(0.25)
    return false
  }
  
  func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
    mapCenterPinImage.fadeIn(0.25)
    mapView.selectedMarker = nil
    return false
  }
}


extension AddAddressVC:SearchLocationVCDelegate{
    func setCustomLocation(lat: Double, long: Double, address: String, completion: (Bool) -> Void) {
        mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 19.0)
        completion(true)
    }
}
