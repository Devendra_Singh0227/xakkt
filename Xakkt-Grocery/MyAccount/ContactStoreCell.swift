//
//  ContactStoreCell.swift
//  MyKirana
//
//  Created by Ankur Nautiyal on 04/11/20.
//  Copyright © 2020 BIGFOOT. All rights reserved.
//

import UIKit

class ContactStoreCell: UITableViewCell {
    
    var callback:(Bool) -> () = { _ in}
    
    @IBOutlet weak var LeftArrow: UIButton!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var stackView:UIStackView!
    
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var extraCallBtn: UIButton!
    @IBOutlet weak var contactStoreNo: UIButton!
    @IBOutlet weak var clickHereButton: UIButton!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
