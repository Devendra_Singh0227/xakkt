//
//  BannerTableCell.swift
//  Xakkt-Grocery
//
//  Created by Developer on 03/09/20.
//  Copyright © 2020 Xakkt. All rights reserved.
//

import UIKit

protocol BannerTableCellDelegate {
    func didselectBannerCollection(indexPath: IndexPath, collectionview: UICollectionView)
}

class BannerTableCell: UITableViewCell {
    
    @IBOutlet weak var paging: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageLbl: UILabel!
    var delegate : BannerTableCellDelegate?
    fileprivate var banner : [Banner]?
    fileprivate var type : type?
    fileprivate var path : String?

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configureCell(banner: [Banner]?, path: String?) -> Void {
        self.paging.numberOfPages = banner?.count ?? 0
        self.banner = banner
        self.path = path
        self.collectionView.reloadData()
    }
}

extension BannerTableCell:UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.banner?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as! BannerCell

        if let image_url =  self.banner?[indexPath.item].image, let path = self.path{
            let url = path + image_url
            cell.image?.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "mainicon"), options: .transformAnimatedImage, completed: nil)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didselectBannerCollection(indexPath: indexPath, collectionview: collectionView)
    }
    
}

extension BannerTableCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
}
